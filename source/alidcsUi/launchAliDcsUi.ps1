﻿# ****       ALICE DCS UI == launchAliDcsUi.ps1     ****
#      ver.1.0  24/06/2020  A.Franco, INFN BARI ITALY
# 
#     Batch script to start the Ui and save in a file the 
#     PID of the process. This is used to run the kill script. 
# 
#     use :  ./launchAliDcsUi.ps1 <name of project>
# **********

echo "*** ALICE DCS UI : starter script ***"
echo " v.2  10/10/2020 - Auth. A.Franco "
echo "     INFN Sez. Bari - ITALY"
echo "line parameter aka project : $args[0]"

$p = $args[0] #"dcs_ui_remote_Run3"
echo "Starts UI for the project : $p ..."
cd C:\Programs\WinCC\bin
$process = Start-Process -FilePath "C:\Programs\WinCC\bin\WCCOAui.exe" -ArgumentList " -style fusion -proj $p -p aliDcsUi\bootUi.pnl" -PassThru
# get the Pid and store it in a file
$id = $process.Id
echo $id | Out-File -FilePath D:\WinCC_Projects\$p\data\myself.pid -Encoding ascii
echo "Terminate start of the UI [PID=$id]"
# -------   EOF  --------