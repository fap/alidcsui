﻿# ----------- ALICE DCS UI  CreateShortcut.ps1
#  Create a Windows ShortCut that starts the UI
#
#  ver 0.1    24/06/2021  Auth: A.Franco
#
echo "CreateShortcut.ps1 :: Start to create the LNK"
$Project = $args[0]
$Target = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
$Arg = " D:\WinCC_Projects\launchAliDcsUi.ps1 " + $Project
$ShortFile = "D:\WinCC_Projects\ALIDCSUI.lnk"

$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut($ShortFile)
$Shortcut.WorkingDirectory = "D:\WinCC_Projects"

$Shortcut.TargetPath = $Target
$Shortcut.Arguments = $Arg
$Shortcut.Save()
$a = "CreateShortcut.ps1 :: The "+$ShortFile+" was created !"
echo $a
$a = "   target : "+$Target
echo $a
$a = "   argumemts : "+$Arg
echo $a
echo " ----------------------------------"


