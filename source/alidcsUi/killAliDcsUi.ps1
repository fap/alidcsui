﻿# ****       ALICE DCS UI == killAliDcsUi.ps      ****
#      ver.1.0  24/06/2020  A.Franco, INFN BARI ITALY
# 
#     Batch script to kill WinCC_UI processes 
#     that remains as zombies after the closure of the UI
# 
#     use :  ./killAliDcsUi.ps1 <PID to kill>
# **********

echo "ALIDCSUI > *** executes the kill of zombies WinCC_Ui *** " 2>&1 | Out-File -FilePath D:\WinCC_Projects\kill.log
echo "The Child PID is $PID"  2>&1 | Out-File -FilePath D:\WinCC_Projects\kill.log  -Append
$pareId = $args[0]
echo "The Parent PID $pareId" 2>&1  | Out-File -FilePath  D:\WinCC_Projects\kill.log  -Append
Start-Sleep -s 10 
echo "Stopping the execution of process $pareId ..."  2>&1 | Out-File -FilePath D:\WinCC_Projects\kill.log -Append
Stop-Process -Id $pareId  -Force  -Verbose 2>&1 | Out-File -FilePath D:\WinCC_Projects\kill.log -Append
echo "Process $pareId stopped !" 2>&1 | Out-File -FilePath D:\WinCC_Projects\kill.log -Append

# -------- EOF ----------
