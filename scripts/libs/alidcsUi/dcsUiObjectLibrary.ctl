// $License: NOLICENSE
/* ------------------------------------------------------------

    	ALICE DCS - UI BASE LIBRARY

  This library collect all functions linked to the ALICE UI
  Ver. 3.0  8/07/2019

  Auth: A.Franco
  Inst. : INFN Seza. BARI - ITALY

       * HISTORY *
05/02/20   Adapt to Dynamic Hierarchy
13/05/20   Intercept the PanelOff() function in the scripts of the
           loaded panel.
21/05/20   Add the manage of PID (ro resolve zombies
03/12/20   Add the User Getter
17/02/21   Fix a bug in Panel name
22/02/21   Fix the bug into the Dialog facility
05/04/21   Remove SELECTION LIST form hookable
08/09/21   Fix the Button word wrap
20/10/21   Fix the Parameters in the Left panel & Multi Widjet
18/01/22   Export the readPID in order to reduce the critical run
22/03/22   Allows the run without an FSM defined
29/03/22   Adopt the UIwith UIheigh  GetUIDimension(int &Width, int &Height)
04/05/22   Fix the "left_click" RADIO_BOX warning & add  LaunchActualPanel()
23/05/22   Hook the PanelOff()
17/06/22   Fix the Hook function
21/06/22   Exclude the Scope Grafic Controls hooking !
07/11/22   Check the presence of an already hooked script, avoids the repetition for "keep in memory" panels.
07/11/22   Sets the button label on double rows stepping to space char.

---------------------------------------------------------------*/
#uses "fwAccessControl/fwAccessControl"
#uses "CtrlXml"
#uses "alidcsUi/dcsUiLibrary.ctl"
#uses "alidcsUi/dcsUiConfigLibrary.ctl"
#uses "alidcsUi/dcsUiFSMLibrary.ctl"


// ==============  Global const definitions ==================
const string DCSUI_OBJ_VERSION = "1.0.29"; // Library Version

const string GUIPANELFILENAME = "alidcsUi/uiver1.xml";
const string GUIPANELNAME = "dcsUiUserPanel";
const string GUIMODULNAME = "MainDCSUi";

const string DIALOGPANELFILENAME = "alidcsUi/dcsUiDialog.pnl";
const string DIALOGPANELNAME = "dcsUiDialog";

const int SCREENBAROFFSET = 75;


// ==============  Global var definitions ==================


// ==============  Data type/structures definition ==============

struct PanelButton
{
  string Label;
  string Panel;
  bool Enabled;
  bool Selected;
  dyn_string Parameters;
  bool isHookable;
};

// ==============  Class Definition =================

class Menu
{
  const string DEF_CONTAINERNAME = "modContainer";
  const string DEF_LEFTPANELNAME = "modLeftPanel";
  const string DEF_MODHEADERNAME = "rtHeader";
  const string DEF_HEADERPANELNAME = "alidcsUi/HeaderPanel.pnl";
  const string DEF_MODFOOTERNAME = "rtFooter";
  const string DEF_FOOTERPANELNAME = "alidcsUi/FooterPanel.pnl";
  const string DEF_FRAMEBACKGND = "retBackground";
  const string DEF_FRAMETABBAR = "retTabbar";
  const string DEF_FRAMEBUTCOL = "retFrameLeft";
  const string DEF_TABBUTPREFIX = "TB";
  const string DEF_PANBUTPREFIX = "COL";

  const int NUMOFCOLUMNS = 1;
  const int TABHEIGTHRATIO = 12;
  const int WINFRAMESPACE = 3;
  const float FRAMETABRATIO = 1.1;

  const int BUTTONSHADOWDIM = 0;//2;
  const int BUTTONSHADOWRAD = 6;
  const int BUTTONSHADOWMULTIP = 5;
  const string BUTTONSHADOWCOLOR = "aliUiZoneFore";

  int windowWidth;
  int windowHeigth;
  int tabSlotHeigth;
  int tabSlotWidth;
  int tabZoneXleft;
  int tabZoneXright;
  int tabZoneYtop;
  int tabZoneYbottom;
  int frameSlotWidth;
  int frameSlotHeigth;
  int frameZoneXleft;
  int frameZoneXright;
  int frameZoneYtop;
  int frameZoneYbottom;

  float zoomFactor;

  // Members
  int defaultTabButtonPosition = 0;

  dyn_anytype frameDefArray;

  string actualShowPanelName = "";
  string actualShowPanelRef = "";
  dyn_string actualShowPanelParameters;

  string leftPanelName = "";
  string leftPanelRef = "";

  string guiModuleName;
  string guiPanelName;
  string guiContainerModuleName;

  string modContainerName = DEF_CONTAINERNAME;
  string modLeftPanelName = DEF_LEFTPANELNAME;
  public AssignContainersName(string panelmod, string leftpanelmod)
  {
    modContainerName = panelmod;
    modLeftPanelName = leftpanelmod;
    return;
  }

  string modHeaderName = DEF_MODHEADERNAME;
  string headerPanelName = DEF_HEADERPANELNAME;
  public AssignHeaderName(string panelmod, string panelname)
  {
    modHeaderName = panelmod;
    headerPanelName = panelname;
    return;
  }

  string modFooterName = DEF_MODFOOTERNAME;
  string footerPanelName = DEF_FOOTERPANELNAME;
  public AssignFooterName(string panelmod, string panelname)
  {
    modFooterName = panelmod;
    footerPanelName = panelname;
    return;
  }

  string shBackGnd = DEF_FRAMEBACKGND;
  string shTabBar = DEF_FRAMETABBAR;
  string shButCol = DEF_FRAMEBUTCOL;
  string shRightCol = "";
  public AssignFrames(string backgnd, string tabbar, string butcol, string rightcol="")
  {
    shBackGnd = backgnd;
    shTabBar = tabbar;
    shButCol = butcol;
    shRightCol = rightcol;
    return;
  }

  string shTabButPrefix = DEF_TABBUTPREFIX;
  string shColButPrefix = DEF_PANBUTPREFIX;
  public AssignButPrexif(string tabbut, string colbut)
  {
    shTabButPrefix = tabbut;
    shColButPrefix = colbut;
    return;
  }

  UILayout theLayout;
  public Configuration Conf;

  // Initialize the class reading the XML configuration file
  public Init(string configurationFileName,
              string Module,
              string Panel,
              string ID="MAIN")
  {
    Conf = new Configuration();
    if(!Conf.Init(configurationFileName, true)) return;
    if(!Conf.ReadUILayout(theLayout)) {
      alidcsUi_log("Assumed the default UI layout !",LOGWARN, "Menu::Init()");
    }
    dynClear(frameDefArray);
    guiModuleName = Module;
    guiPanelName = Panel;
    guiContainerModuleName = "Lello" + rand();
    return;
  }

  // Sets the windows properties
  public int SetupTheWindow(shape win)
  {
    if(theLayout.windowFullScreen) {
      setScaleStyle(SCALE_FIT_TO_MODULE, guiModuleName);
      windowStyle(guiModuleName, guiPanelName, false, false);
      titleBar(false, guiModuleName);
      win.windowFlags("FramelessWindowHint");
      win.windowState("WindowFullScreen");
      return(2);
    }
    if(theLayout.windowResizable) {
      setScaleStyle(SCALE_FIT_TO_MODULE, guiModuleName);
      return(1);
    }
    windowStyle(guiModuleName,guiPanelName,true,true);
    return(0);
  }

  // Sets Up all the size/pos for all the widjet in the window ...
  public PopulateTheWindow(shape win, int width=-1, int heigth=-1)
  {
    // Gets the Geometry
    int x,y,h,w;
    getValue( win, "windowGeometry", x, y, w, h);
    windowWidth = (width == -1)? w : width;
    windowHeigth = (heigth == -1) ? h: heigth;
    // Calculate ande sets the Geometry
    tailorGeometry(win);
    arrangeTheWindow(win);
    arrangeTheHeader();
    arrangeTheFooter();
    arrangeTheModuleContainer();
    arrangeTheTabBar();
    arangeTheFrameButtons();
    // If a Default Tab is defined
    if(defaultTabButtonPosition > 0) {
      SelectTab(shTabButPrefix+defaultTabButtonPosition);
    }
    return;
  }

  // Calculate all the geometry parameters of the window
  private void tailorGeometry(shape win)
  {
    tabSlotHeigth = (windowHeigth - (theLayout.headerHeigth+theLayout.footerHeigth)) / TABHEIGTHRATIO;
    tabSlotWidth = (windowWidth-WINFRAMESPACE*2) / theLayout.numberOfTabs;
    tabZoneXleft = WINFRAMESPACE;
    tabZoneXright = windowWidth-WINFRAMESPACE;
    tabZoneYtop = theLayout.headerHeigth + WINFRAMESPACE;
    tabZoneYbottom = tabZoneYtop + tabSlotHeigth;

    frameSlotWidth = (windowWidth-WINFRAMESPACE*2) / theLayout.numberOfTabs * FRAMETABRATIO;
    frameZoneXleft = WINFRAMESPACE;
    frameZoneXright = windowWidth - WINFRAMESPACE;
    frameZoneYbottom = windowHeigth - theLayout.footerHeigth - WINFRAMESPACE;
    frameZoneYtop = tabZoneYbottom + WINFRAMESPACE * 4;
    frameSlotHeigth = (frameZoneYbottom - frameZoneYtop) / theLayout.numPanButRows;
    return;
  }

  // Try to extablish a font dimension from a width in pixel : empiric !
  private int calculateFontSize(int w)
  {
    int f = w / 16 + 8;
    return(f);
  }

  // Set up the Text property of buttons with the label one/two rows if...
  private void setupButtonLabel(string shapeName, string label, int width)
  {
    int n = width / 11;
    int k = strlen(label);
    if(k > n) {
      int j = strpos(label, " ");
      if(j < 0) {
        label = substr(label,0,k/2) + '\n' + substr(label,k/2);
      } else {
        label = substr(label,0,j) + '\n' + substr(label,j+1);
      }
    }
    setValue(shapeName,"text",label);
    return;
  }

  private void getTabButtonGeometry(int &width, int&heigth, int &font)
  {
    heigth = tabSlotHeigth - 2;
    width = tabSlotWidth - 9;
    font = calculateFontSize(width);
    return;
  }

  private void getFrameButtonGeometry(int &width, int &heigth, int &font)
  {
    heigth = frameSlotHeigth - 11;
    width = frameSlotWidth - 20;// + 50;
    font = calculateFontSize(width);
    return;
  }

  // Sets all the Button Porperties ...
  private void setupButtonShape(string shapeName,int w,int h,int x, int y, int f)
  {
    setMultiValue(shapeName, "position", x, y, shapeName, "size", w, h);
    setMultiValue(shapeName, "foreCol",theLayout.buttonForegColor,
                  shapeName, "backCol", theLayout.buttonBackgColor);
    setMultiValue(shapeName, "enabled",true, shapeName, "visible", true);
    setValue(shapeName, "font", theLayout.buttonFont + ",-1,"+f+",5,25,0,0,0,0,0");
    setValue(shapeName, "toggleState", !theLayout.buttonTypeIsRise);
    return;
  }

  // Sets the shadow of toggled buttons
  public void setShadow(string shapeName, int position, bool state)
  {
    int dx,dy,rad;
    switch(position) {
      case 0: // top
        dx = BUTTONSHADOWDIM;
        dy = BUTTONSHADOWDIM;
        break;
      case 1: // left
        dx = BUTTONSHADOWDIM;
        dy = -1*BUTTONSHADOWDIM;
        break;
      case 2: // right
        dx = -1*BUTTONSHADOWDIM;
        dy = -1*BUTTONSHADOWDIM;
        break;
    }
    if (state) { // == theLayout.buttonTypeIsRise) {
      dx = dx * BUTTONSHADOWMULTIP;
      dy = dy * BUTTONSHADOWMULTIP;
      rad = BUTTONSHADOWRAD * BUTTONSHADOWMULTIP;
    }
    setMultiValue(shapeName,"shadowOffset",dx,dy, shapeName,"shadowColor",BUTTONSHADOWCOLOR,
                  shapeName,"shadowRadius",rad, shapeName,"shadowEnabled",true);
    return;
  }

  private void resetPanelButton(int ind)
  {
    string sn;
    int i,side;
    sn = shColButPrefix+ind;
    i = detectIndexSide(sn,side);
    setMultiValue(sn,"visible",false ,sn,"toggleState",!theLayout.buttonTypeIsRise);
    setShadow(sn,side,!theLayout.buttonTypeIsRise);
    return;
  }

  private void arrangeTheWindow(shape win)
  {
    setMultiValue(shBackGnd,"position",0,0,shBackGnd,"size",windowWidth,windowHeigth);
    setValue(shBackGnd,"backCol",theLayout.windowBackColor);
    return;
  }

  // Set up the Header ...
  private void arrangeTheHeader()
  {
    int w = windowWidth;
    int h = theLayout.headerHeigth;
    int x = 0;
    int y = 0;
    setMultiValue(modHeaderName, "size", w,h, modHeaderName, "position", x,y);
    dyn_int di = getPanelSize(headerPanelName);
    x = windowWidth - di[1];
    y = (theLayout.headerHeigth - di[2]) /2;
    addSymbol(myModuleName(),myPanelName(),headerPanelName,"head123",makeDynString(),x,y);
    return;
  }

  // Set up the Footer ...
  private void arrangeTheFooter()
  {
    int w = windowWidth;
    int h = theLayout.footerHeigth;
    int x = 0;
    int y = windowHeigth-theLayout.footerHeigth;
    setMultiValue(modFooterName, "size", w,h, modFooterName, "position", x,y);
    addSymbol(myModuleName(),myPanelName(),footerPanelName,"foot123",makeDynString("$w:"+(w-4),"$h:"+(h-3)),x+2,y+2);
    return;
  }

  // Set up the Panel Module
  private void arrangeTheModuleContainer()
  {
    int w = frameZoneXright - (frameZoneXleft + frameSlotWidth * NUMOFCOLUMNS);
    int h = frameZoneYbottom - frameZoneYtop;
    int x = frameZoneXleft + frameSlotWidth;
    int y = frameZoneYtop;
    setMultiValue(modContainerName, "size", w,h, modContainerName, "position", x,y);
    return;
  }

  // Set up the Tab Bar ...
  private void arrangeTheTabBar()
  {
    string shapeName;
    string value,Label;
    int Index,Position;
    bool Enabled;
    int buttonWidth,buttonHeigth,fontSize;

    getTabButtonGeometry(buttonWidth,buttonHeigth,fontSize);
    int posX = tabZoneXleft;
    int posY = tabZoneYtop;
    setMultiValue(shTabBar,"position",tabZoneXleft,tabZoneYtop, shTabBar,"size",tabZoneXright-tabZoneXleft,tabZoneYbottom-tabZoneYtop);
    setValue(shTabBar,"backCol",theLayout.zoneBackColor);

    // switch off oll the widjets
    for(int i=1; i<=theLayout.maxOfTabs; i++) {
      shapeName = shTabButPrefix+i;
      setValue(shapeName, "visible", false);
    }

    // Now read the XML file in order to aquire tabbar def
    dyn_dyn_string ddsTabs = Conf.ReadTabsDefinition();

    for(int i=1;i<=dynlen(ddsTabs);i++) {
      Enabled = ddsTabs[i][4];
      if(Enabled) {
        Position = ddsTabs[i][3]; shapeName = shTabButPrefix+Position;
        setupButtonShape(shapeName, buttonWidth, buttonHeigth, posX+(Position-1)*tabSlotWidth, posY+1, fontSize);
        setupButtonLabel(shapeName, ddsTabs[i][1], buttonWidth);
        setValue(shapeName, "enabled", Enabled);
        Index = ddsTabs[i][2];
        if((theLayout.defaultTabButton > 0) && (theLayout.defaultTabButton == Index)) {
            defaultTabButtonPosition = Position;
        }
      }
    }
    return;
  }

  // This is called wen a Tab button is pressed or
  public void SelectTab(string shapeName)
  {
    string snApp;
    int iApp;
    for(int i=1; i<=theLayout.numberOfTabs; i++) {
      snApp = shTabButPrefix+i;
      if(snApp != shapeName) {
        setValue(snApp, "toggleState", !theLayout.buttonTypeIsRise);
        setShadow(snApp, 0, !theLayout.buttonTypeIsRise) ;
      } else {
        iApp = i;
        setValue(shapeName,"toggleState",theLayout.buttonTypeIsRise);
        setShadow(shapeName, 0, theLayout.buttonTypeIsRise);
      }
    }
    setupTheframeDefinition(iApp);
    return;
  }

  // detect the index and the side of the frame button
  // from the name (number of button) of the shape
  public int detectIndexSide(string name, int &side)
  {
    int index = substr(name,3);
    side = (index<=theLayout.numPanButRows) ? 1: 2;
    return(index);
  }

  // Arranges all the frame buttons
  private void arangeTheFrameButtons()
  {
    int buttonWidth, buttonHeigth, fontSize;
    getFrameButtonGeometry(buttonWidth, buttonHeigth, fontSize);

    setMultiValue(shButCol,"position",frameZoneXleft,frameZoneYtop, shButCol,"size",frameSlotWidth-10,frameZoneYbottom-frameZoneYtop);
    setValue(shButCol,"backCol",theLayout.zoneBackColor);
    if(shRightCol != "") {
      setMultiValue(shRightCol,"position",frameZoneXright-frameSlotWidth+10,frameZoneYtop, shRightCol,"size",frameSlotWidth-10,frameZoneYbottom-frameZoneYtop);
      setValue(shRightCol,"backCol",theLayout.zoneBackColor);
    }

    int posX, posY;
    posX = frameZoneXleft+2;
    posY = frameZoneYtop+2;

    string shapeName;

    for(int i=1; i<=theLayout.numPanButRows; i++) {
      shared_ptr<PanelButton> newButton = new PanelButton();
      newButton.Label = "Lab"+i;
      newButton.Panel = "";
      newButton.Enabled = false;
      newButton.Selected = false;
      dynAppend(frameDefArray, newButton);

      shapeName = shColButPrefix+i;
      setupButtonShape(shapeName, buttonWidth, buttonHeigth, posX, posY, fontSize);
      setupButtonLabel(shapeName, newButton.Label, buttonWidth);
      setValue(shapeName, "visible", false);
      posY = posY + frameSlotHeigth;
      if(i >= theLayout.numPanButRows) { // we complete the first column
        if( NUMOFCOLUMNS == 1 ) { // we have only one column, and reach the end
          break;
        }
        posX = frameZoneXright - buttonWidth - 2;
        posY = frameZoneYtop+2;
      }
    }
    setMultiValue(modLeftPanelName,"size",buttonWidth+10 , (frameZoneYbottom - frameZoneYtop),
                  modLeftPanelName,"position",frameZoneXleft , frameZoneYtop);
  }

  // ----- Manage the Left space for host a panel
  private void setupLeftPanel(string PanelFile, dyn_string Parameters)
  {
    leftPanelRef  = rand();
    leftPanelName = PanelFile;
    setMultiValue(modLeftPanelName, "visible", true, modLeftPanelName, "ModuleName", "leftPan");

    if(!isfile(getPath( PANELS_REL_PATH,leftPanelName))) {
      alidcsUi_log("Panel file : "+leftPanelName+" doesn't exists!",LOGERRO,"Menu::setupLeftPanel()");
      return;
    }
    RootPanelOnModule(PanelFile,leftPanelRef,"leftPan",Parameters);

    dyn_int di = getPanelSize(getPath( PANELS_REL_PATH,PanelFile));
    int w,h;// getValue("leftPan:modLeftPan", "size", w,h);

      getValue(guiModuleName+"."+guiPanelName+":"+modLeftPanelName, "size", w,h);

    float rx = (float)w/(float)di[1];
    float ry = (float)h/(float)di[2];
    ZoomModule("leftPan",((rx < ry)? rx : ry), 10, 10);

    alidcsUi_log("Panel file : "+leftPanelName+" loaded ("+dynStringToString(Parameters,",")+")",LOGINFO,"Menu::setupLeftPanel()");
    return;
  }

  private void clearLeftPanel()
  {
    setMultiValue(modLeftPanelName, "visible", false,modLeftPanelName, "ModuleName", "leftPan");
    PanelOffModule(leftPanelRef,"leftPan");
    leftPanelName = "";
    leftPanelRef = "";
    return;
  }

  // This is called every time a tab button is clicked
  private void setupTheframeDefinition(int index)
  {
    int buttonWidth, buttonHeigth, fontSize;
    getFrameButtonGeometry(buttonWidth, buttonHeigth, fontSize);

    string shapeName, theShapeToSelect ;
    int butPosition;

    // Reset all the buttons/panel
    ClearPanel();
    for(int i = 1; i<=theLayout.numPanButRows; i++) {
      resetPanelButton(i);
    }
    clearLeftPanel();

    // read the XML to find the new conf
    theShapeToSelect = "";
    dyn_dyn_anytype ddaDef = Conf.ReadFrameDefinition(index);
    if(dynlen(ddaDef) == 1 && dynlen(ddaDef[1]) == 2) { // this is a panel
      setupLeftPanel(ddaDef[1][1], stringToDynString(ddaDef[1][2]," | "));
      return;
    }
    for(int i=1;i<=dynlen(ddaDef);i++) {  // Set up the buttons
      butPosition = ddaDef[i][2];
      shapeName = shColButPrefix + ddaDef[i][2];
      setupButtonLabel(shapeName, ddaDef[i][3], buttonWidth);
      setMultiValue(shapeName, "enabled", ddaDef[i][6], shapeName, "visible", true);
      frameDefArray[butPosition].Label = ddaDef[i][3];
      frameDefArray[butPosition].Panel = ddaDef[i][4];
      frameDefArray[butPosition].Enabled = ddaDef[i][6];
      frameDefArray[butPosition].Parameters = stringToDynString(ddaDef[i][5],"|");
      frameDefArray[butPosition].Selected = false;
      frameDefArray[butPosition].isHookable = ddaDef[i][7];
      if(ddaDef[i][8]) theShapeToSelect = shapeName;
    }
    if(theShapeToSelect != "") {
      SelectPanel(theShapeToSelect);
    }
    return;
  }
  // A button is pressed !
  // deselct the other buttons and select and open the related panel
  //
  public void SelectPanel(string shapeName)
  {
    int side;
    int index = detectIndexSide(shapeName,side);
    for (int i=1; i<=MAXNUMPANBUT; i++) {
      if(i != index) {
       setValue(shColButPrefix+i, "toggleState", !theLayout.buttonTypeIsRise);
       setShadow(shColButPrefix+i, side, !theLayout.buttonTypeIsRise) ;
       frameDefArray[i].Selected = false;
      } else {
       setValue(shapeName, "toggleState", theLayout.buttonTypeIsRise);
       setShadow(shapeName, side, theLayout.buttonTypeIsRise);
       frameDefArray[i].Selected = true;

      }
    }
    DisplayPanel(index);
    return;
  }

  // -------- Display a panel that is defined into the structure
  public void LoadAndDisplayPanel(string panelname, dyn_string parameters, bool isHookable=true)
  {
    if(!isfile(getPath( PANELS_REL_PATH, panelname))) {
      alidcsUi_log("The panel file :"+  panelname + " doesn't exists !", LOGERRO, "Menu::DisplayPanel()");
      autoZoomLoadPanel("alidcsUi/404.pnl", parameters);
      return;
    }
    autoZoomLoadPanel(panelname, parameters, isHookable);
    return;
  }

  // -------- Display a panel that is defined into the structure
  private void DisplayPanel(int i)
  {
    if(i <= dynlen(frameDefArray)) {
      alidcsUi_log("Open the panel file :"+frameDefArray[i].Panel+" Param:"+dynStringToString(frameDefArray[i].Parameters)+" is Hok:"+(frameDefArray[i].isHookable == true ? "true":"false"), LOGDEBU,"Menu::DisplayPanel()" );
      LoadAndDisplayPanel(frameDefArray[i].Panel, frameDefArray[i].Parameters, frameDefArray[i].isHookable);
    }
    return;
  }

  private void ClearPanel()
  {
    if(actualShowPanelRef != "")
      PanelOffModule(actualShowPanelRef,guiContainerModuleName);
    actualShowPanelRef = "";
    actualShowPanelName = "";
    return;
  }

  // Loads the panel and zoom it at the maximum ratio
  private void autoZoomLoadPanel(string  panName, dyn_string panParameters, bool isHookable = true)
  {
    string refName = rand();
    setMultiValue(guiModuleName+"."+guiPanelName+":"+modContainerName, "visible", false,
                  guiModuleName+"."+guiPanelName+":"+modContainerName, "ModuleName", guiContainerModuleName);
    ClearPanel();
    RootPanelOnModule(panName,refName,guiContainerModuleName,panParameters);
    actualShowPanelName = panName;
    strreplace(actualShowPanelName, "\\", "/");
    actualShowPanelRef = refName;
    actualShowPanelParameters = panParameters;
    dyn_int di = getPanelSize(panName);
    ZoomFit(di[1], di[2]);
    setValue(guiModuleName+"."+guiPanelName+":"+modContainerName, "visible", true);
    delay(1,100);
    setValue(guiContainerModuleName+"."+refName+":", "keepInMemory", false);
    HookTheClickEvent(guiContainerModuleName, refName, isHookable) ;
    alidcsUi_log("The panel file :"+actualShowPanelName+" loaded as "+actualShowPanelRef, LOGINFO, "Menu::autoZoomLoadPanel()");
    return;
  }

  public void LaunchActualPanel(bool modal = false)
  {
    if(modal) {
      ChildPanelOnCentralModal(actualShowPanelName, actualShowPanelName, actualShowPanelParameters);
    } else {
      ChildPanelOnCentral( actualShowPanelName, actualShowPanelName, actualShowPanelParameters);
    }
    return;
  }

  public string GetActualPanelName()
  {
    return(actualShowPanelName);
  }

  // --- Zooming Functions
  private zoom(float factor)
  {
    if(factor >= 0.5 && factor <= 4.0) {
      zoomFactor = factor;
      ZoomModule(guiContainerModuleName, zoomFactor, 10, 10);
    } else {
      alidcsUi_log("Zoom factor out of range !", LOGDEBU, "Menu::zoom()");
    }
    return;
  }

  public void ZoomPlus()
  {
    zoom(zoomFactor + 0.1);
    return;
  }
  public void ZoomMinus()
  {
    zoom(zoomFactor - 0.1);
    return;
  }
  public void ZoomFit(int maxx, int maxy)
  {
    int w,h;
    getValue(guiModuleName+"."+guiPanelName+":"+modContainerName, "size", w,h);
    float rx,ry;
    rx = (float)w/(float)maxx;
    ry = (float)h/(float)maxy;
    if(rx < ry) {
      zoom(rx);
    } else {
      zoom(ry);
    }
    return;
  }
  public void ZoomOne()
  {
    zoom(1.0);
    return;
  }

  public void HookTheClickEvent(string ModuleName, string PanelReference, bool isHookable=true)
  {
/*
  "PRIMITIVE_TEXT","LINE","RECTANGLE","ELLIPSE","ARC","POLYGON","PUSH_BUTTON","TEXT_FIELD",
  "CLOCK","SELECTION_LIST","RADIO_BOX","CHECK_BOX","SPIN_BUTTON",,"COMBO_BOX","TREND","TABLE",
  "CASCADE_BUTTON","BAR_TREND","TAB","ACTIVE_X","FRAME","PIPE","DP_TREE","TEXT_EDIT","SLIDER",
  "THUMB_WHEEL","PROGRESS_BAR","TREE","DPTYPE","LCD","ZOOM_NAVIGATOR","EMBEDDED_MODULE","SCHEDULER_EWO"
*/
    dyn_string dsShType = makeDynString( "PUSH_BUTTON","RADIO_BOX","CHECK_BOX","SPIN_BUTTON",
                                       "CASCADE_BUTTON","RECTANGLE","ELLIPSE","ARC","POLYGON");

    string sScriptSource;
    int pt, flag;
	//DebugTN(">>>>------- Object Lybrary Version --->", DCSUI_OBJ_VERSION);
    dyn_string dsShapes, dsEvents;
    int ns,is,ms,js, iCount;
    string sShapRef;
    ms = dynlen(dsShType);
    for(js=1;js<=ms;js++) {
      dsShapes = getShapes(ModuleName,PanelReference,"shapeType",dsShType[js]);
      ns = dynlen(dsShapes);
      for(is=1;is<=ns;is++){
        sShapRef = ModuleName+"."+PanelReference+":"+dsShapes[is];
        if(!shapeExists(sShapRef) || strpos(dsShapes[is],"Scope")>0) continue;
        getValue(sShapRef, "scriptNames", dsEvents);
        iCount += catchTheHookableEvent(dsEvents,sShapRef,dsShapes[is],"Clicked",isHookable);
        iCount += catchTheHookableEvent(dsEvents,sShapRef,dsShapes[is],"RightMousePressed",isHookable);
        iCount += catchTheHookableEvent(dsEvents,sShapRef,dsShapes[is],"DoubleClicked",isHookable);
      }
    }
    alidcsUi_log("Is Hookable :"+isHookable+"  Hooked :"+iCount+" shapes/events into Panel:"+actualShowPanelName, LOGINFO, "Menu::HookTheClickEvent()");
    return;
  }

  private int catchTheHookableEvent(dyn_string dsEvents, string sShapRef, string sShape,
                                    string sEvent, bool isHookable)
  {
    string sScriptSource;
    int flag = 0;
    if(dynContains(dsEvents, sEvent) > 0) {
      getValue(sShapRef, "script", sEvent, sScriptSource);
      if(sScriptSource != "") {
        flag = strreplace(sScriptSource,"PanelOff(", ";//"); //"PanelOffPanel(myPanelName())");
        if(isHookable && strpos(sScriptSource, "alidcsUi_sendLogMsg") < 0) {
          sScriptSource = buildHookedScript(sScriptSource,sShape,sEvent);
        }
        setValue(sShapRef, "script", sEvent, sScriptSource);
        return(1);
      }
    }
    return(0);
  }

  private string buildHookedScript(string originalScript, string shapeName, string eventType)
  {
    string returnedScript = "";
    int pt = strpos(originalScript, "{", 0);
    if(pt>1) {
      returnedScript = substr(originalScript,0,pt+1) + "alidcsUi_sendLogMsg(LOGINFO,\"DCSUI::TrackEvents()\",\""+actualShowPanelName+"::"+shapeName+"->"+eventType+"() event issued!\");";
      returnedScript += substr(originalScript,pt+1);
    } else {
      returnedScript = originalScript;
    }
    return(returnedScript);
  }

  public dyn_dyn_anytype ReadToolsDefinition()
  {
    return(Conf.ReadToolsDefinition());
  }

  public dyn_string ReadFSMRootNodes()
  {
    return(Conf.ReadFSMRootNodes());
  }

  public dyn_dyn_anytype ReadCustomWidjet()
  {
    return(ReadCustomElement("WidjetPanel"));
  }

  public dyn_dyn_anytype ReadCustomElement(string sTag = "")
  {
    dyn_dyn_anytype ddaAppo = Conf.ReadCustomization();
    for(int i=dynlen(ddaAppo); i>0; i--) {
      if(ddaAppo[i][1] != sTag) {
        dynRemove(ddaAppo, i);
      }
    }
    return(ddaAppo);
  }


};


class DCSUI
{
  private static string User = "";
  private static string Password = "";
  private static int UserId = 0;

  public static string GetUser()
  {
    return(User);
  }

  private static dyn_anytype Menus;
  private static dyn_string MenuIDs;
  private static dyn_anytype FSMHierarchies;
  private static dyn_string RootNodes;

  public static void RecordMenu(shared_ptr<Menu> menuPtr, string ID)
  {
    dynAppend(Menus, menuPtr);
    dynAppend(MenuIDs, ID);
    return;
  }
  public static shared_ptr<Menu> GetMenuPtr(string ID)
  {
    int i = dynContains(MenuIDs, ID);
    if(i > 0) {
      return(Menus[i]);
    } else {
      return(nullptr);
    }
  }
  public static void RemoveMenu(string ID)
  {
    int i = dynContains(MenuIDs, ID);
    if(i > 0) {
      dynRemove(Menus,i);
      dynRemove(MenuIDs,i);
    }
    return;
  }

  public static dyn_string GetFsmNodes()
  {
    return( RootNodes );
  }
  public static void RecordFsm(shared_ptr<FSMHierarchy> fsmPtr, string Node)
  {
    dynAppend(FSMHierarchies, fsmPtr);
    dynAppend(RootNodes, Node);
    return;
  }
  public static shared_ptr<FSMHierarchy> GetFsmPtr(string Node)
  {
    int i = dynContains(RootNodes, Node);
    if(i > 0) {
      return(FSMHierarchies[i]);
    } else {
      return(nullptr);
    }
  }
  public static void RemoveFsm(string Node)
  {
    int i = dynContains(RootNodes, Node);
    if(i > 0) {
      dynRemove(FSMHierarchies,i);
      dynRemove(RootNodes,i);
    }
    return;
  }

  /*
  public static DCSUI()
  {
    DebugTN(">> DCSUI constructor");
  }
  */

  public static void TailorXMLpanel(string confFileName="")
  {
    if(confFileName != "") {
      Configuration Conf = new Configuration();
      Conf.Init(confFileName, false);
      Conf.ReadUIDim(ScreenWidth,ScreenHeight);
    }

    string panelName = GUIPANELFILENAME;
    string fileName = getPath(PANELS_REL_PATH,panelName);
    string errMsg;
    int errLine, errColumn;
    unsigned docId = xmlDocumentFromFile(fileName, errMsg, errLine, errColumn);
    int xmlNode = analizeXMLNode(xmlFirstChild(docId),docId);
    if(xmlNode >= 0) {
      alidcsUi_log("Modify the XML panel with the Screen dimension :"+fileName+":"+ScreenWidth+","+ScreenHeight+","+ScreenStartX+","+ScreenStartY, LOGINFO,"DCSUI::TailorXMLpanel()");
      xmlSetNodeValue(docId, xmlNode, ScreenWidth +" "+ScreenHeight);
      xmlDocumentToFile(docId,fileName);
      UIWidth = ScreenWidth;
      UIHeight = ScreenHeight;
    }
    xmlCloseDocument(docId);
    return;
  }

  private static int analizeXMLNode(int xmlNode, unsigned docId)
  {
    mapping attrib;
    delay(0,100);
    if(xmlNode >= 0 ){
      if(xmlNodeType(docId, xmlNode) == XML_ELEMENT_NODE ) {
        attrib = xmlElementAttributes(docId, xmlNode);
        if(mappinglen(attrib) > 0 && mappingHasKey(attrib,"name") && attrib["name"] == "Size") {
          xmlNode = xmlFirstChild(docId, xmlNode);
          return(xmlNode);
        }
      }
      unsigned n = xmlNode;
      xmlNode = analizeXMLNode(xmlFirstChild(docId, n),docId); if(xmlNode >= 0) return(xmlNode);
      xmlNode = analizeXMLNode(xmlNextSibling(docId, n),docId); if(xmlNode >= 0) return(xmlNode);
    }
    return(-1);
  }

  public static void RunMainPanel(string confFileName)
  {
    string guiModule = GUIMODULNAME + rand();
    string guiPanel = GUIPANELNAME + rand();
    ConfigFileName = confFileName;
    ModuleOnWithPanel(guiModule,
                    1,1,ScreenWidth,ScreenHeight,// giScreenStartX,giScreenStartY,giScreenWidth,giScreenHeight,
                    1, // withOut Icon Bar
                    1, // witOut the Menu Bar
                    "",//"None", // No resize
                    GUIPANELFILENAME,
                    guiPanel,
                    makeDynString("$config:"+confFileName,"$node:"+RootNode,"$obj:"+RootNode) );
    alidcsUi_log("Start panel "+guiModule+":"+guiPanel+","+GUIPANELFILENAME+", conf:"+confFileName, LOGINFO,"DCSUI::RunMainPanel()");
    return;
  }

  private static int ScreenWidth = 640;
  private static int ScreenHeight = 480;
  private static int ScreenStartX = 1;
  private static int ScreenStartY = 1;

  private static int UIWidth = 640;
  private static int UIHeight = 480;

  public static void ScreenDetect(bool isFullScreen)
  {
    // Get the Window resolution
    int giDcsUiScreenWidth, giDcsUiScreenHeight, giDcsUiScreenStartX, giDcsUiScreenStartY;
    getScreenSize(giDcsUiScreenWidth, giDcsUiScreenHeight, giDcsUiScreenStartX, giDcsUiScreenStartY);

    // Calculate dimensions for the Main Panel
    ScreenWidth = giDcsUiScreenWidth - ((isFullScreen==true) ? 2 : 15);
    ScreenHeight = giDcsUiScreenHeight - ((isFullScreen==true) ? 2 : SCREENBAROFFSET);
    ScreenStartX = 1;
    ScreenStartY = 1;
    alidcsUi_log("Detect screen size :"+ScreenWidth+"x"+ScreenHeight+",("+ScreenStartX+","+ScreenStartY+")", LOGINFO,"DCSUI::ScreenDetect()");
    return;
  }
  public static void GetScreenDimension(int &Width, int &Height)
  {
    Width = ScreenWidth;
    Height = ScreenHeight;
    return;
  }
  public static void GetUIDimension(int &Width, int &Height)
  {
    Width = UIWidth;
    Height = UIHeight;
    return;
  }

  public static string RootNode = "";
  public static string RootObj = "";
  public static dyn_string RootNodesConfigured;

  // Obsolete :private static string PrefixShape = "";

  public static bool Init(string confFileName)
  {
    alidcsUi_log("BOOT...", LOGINFO,"DCSUI::Init()");

    // Verify if the init is done
    if(RootNode != "")	{
      alidcsUi_log("ver."+ALIDCSUI_VERSION+" Intialization phase already done. Skipped ! ("+RootNode+")",LOGINFO,"DCSUI::Init()");
    		return(true);
    }

    // Get the PID number from e file
    readPid();

    int val;
    string fi = getPath(CONFIG_REL_PATH,"config");
    string sec = "NewAlice_DCSUI";
    string result = "";

    // Reads and manage FSM
    alidcsUi_log("Read DCSUI Config from :"+fi+" :"+sec, LOGINFO,"DCSUI::Init()");
    if(paCfgReadValue(fi,sec, "RootNode",result) != 0 ) {
      alidcsUi_log("Undefined item in config file ("+fi+":"+sec+":RootNode)",LOGWARN,"DCSUI::Init()");
  	} else {
      dynClear(RootNodesConfigured);
      RootNodesConfigured = strsplit(result,",");
      RootNode = dynlen(RootNodes) > 0 ? RootNodesConfigured[1] : "";
      if(paCfgReadValue(fi,sec, "RootObject",result) != 0 ) {
        alidcsUi_log("Undefined item in config file ("+fi+":"+sec+":RootObject)",LOGWARN,"DCSUI::Init()");
     	} else {
        RootObj = result;
      }
    }

    // reads and manage AC
    if(!manageAC(fi)) {
      alidcsUi_log("Failed to setup the Access Control. Abort !",LOGCRIT,"DCSUI::Init()");
      return(false);
    }

    alidcsUi_log("ver."+ALIDCSUI_VERSION+" Intialization phase...",LOGINFO,"DCSUI::Init()");
    UILayout theLayout;
    Configuration Conf;

    if(!Conf.Init(confFileName, true)) { // Verify the presence of Config file
      alidcsUi_log("Configuration file : "+confFileName+" doesn't exists. Init Aborted !",LOGCRIT,"DCSUI::Init()");
      return(false);
    }
    if(!Conf.ReadUILayout(theLayout)) {
      alidcsUi_log("Assumed the default UI layout !",LOGWARN, "Menu::Init()");
    }

    // Screen detect
    ScreenDetect(theLayout.windowFullScreen);

    // Now it ends -----
    alidcsUi_log("init phase DONE !",LOGINFO,"DCSUI::Init()");
    return(true);
  }

  // ----------------------------------------------------------------
  // Manage the PID
  // ----------------------------------------------------------------
  private static int myPid = -1;

  private static void readPid()
  {
    string filename = getPath(DATA_REL_PATH, "")+ "mySelf.pid";
    file fh = fopen(filename, "r");
    string buf;
    if(fh > 0) {
      fgets(buf, 1024, fh);
      fclose(fh);
      myPid = buf;
    } else {
      if(myPid == -1) { // File doesn't exists
        myPid = 0;
      } // else left the already aquired PID
    }
    alidcsUi_log("Acquire the PID="+myPid,LOGINFO,"DCSUI::readPid()");
    remove(filename);
    return;
  }

  public static int GetPid()
  {
    return(myPid);
  }

  public static int ReadPid()
  {
    readPid();
    return(myPid);
  }

  // ----------------------------------------------------------------
  // AC Subsection
  // ----------------------------------------------------------------
  public static bool useAC = false;
  public static string ACDomain = "";
  public static string ConfigFileName = "";
  public static string ACExcludePrivi = "Debug";
  public static string ACDisablePrivi = "Debug";

  private static bool manageAC(string conffile )
  {
    string sec = "NewAlice_DCSUI";

    int val = 0;
    string result = "";
    bool bLoginOk = false;
    dyn_string exceptionInfo;
    string osUser,osFullName,osDescription;
    dyn_string osGroups;

    val += paCfgReadValue(conffile, sec, "UseAC",result);
    useAC = (strtoupper(result) == "YES") ? true : false;
    if(useAC) { // test if the Framework Access Control is set and enabled
      val += paCfgReadValue(conffile, sec, "ACDomain",result);
      ACDomain = result;
      alidcsUi_log("Read the AC Domain :"+ACDomain,LOGINFO,"DCSUI::manageAC()");

      // Set the CB Function that Hook the change of Logged User
      fwAccessControl_setupPanel("alidcsUi_LoggedUserAC",exceptionInfo);
      if (dynlen(exceptionInfo)) {
        LoggedUserAC("",""); // global "reject"
      }
      // Get the actual user
      fwAccessControl_getUserName(User);
      // Keep the default user if he exists
      val += paCfgReadValue(conffile, sec, "DefaultUser",result);
      User = result;
      if(User == "" || strtoupper(User) == "NONE" || User == "root" || User == "NO USER") { // He don't exists ! Normal Login
        bLoginOk = false;
      } else {
        val += paCfgReadValue(conffile, sec, "DefaultUserPassword",result);
        Password = result;
        bLoginOk = _fwAccessControl_doLogin(User, Password, exceptionInfo);
        if (dynlen(exceptionInfo)) {
          fwAccessControl_displayException(exceptionInfo);
        }
      }
      alidcsUi_log("Read the AC User/Passwd  :"+User,LOGINFO,"DCSUI::manageAC()");

      result = "";
      val += paCfgReadValue(conffile, sec, "FSMExcludeAC",result);
      ACExcludePrivi = result;
     	result = "";
      val += paCfgReadValue(conffile, sec, "FSMDisableAC",result);
      ACDisablePrivi = result;
      alidcsUi_log("Read the AC Exclude/Disable  :"+ACExcludePrivi+","+ACDisablePrivi,LOGINFO,"DCSUI::manageAC()");
      if(val>0) {
        alidcsUi_log("Some parameters are wrong in config file",LOGERRO,"DCSUI::manageAC()");
        return(false);
      }

      if(!bLoginOk) { // The param into the config are Wrong ! go throw the normal login
        alidcsUi_log("Request the fw AC login with dialog",LOGINFO,"DCSUI::manageAC()");
     	  disableAllAC();
        fwAccessControl_logout();
        delay(1,0);
        fwAccessControl_login();
        User = "NO USER";
        int count = 20;
        while(User == "NO USER" && count > 0) {
          fwAccessControl_getUserName(User);
          delay(1);
          count--;
        }
        if(count == 0) {
          alidcsUi_log("No valid user in login",LOGERRO,"DCSUI::manageAC()");
          return(false);
        }
      }
      fwAccessControl_getUser(User, osFullName, osDescription, UserId, bLoginOk, osGroups, exceptionInfo);
      alidcsUi_log("The actual logged user is : "+osFullName,LOGINFO,"DCSUI::manageAC()");
    } else {
      // We don't use the FW access control
      getCurrentOSUser(osUser, osFullName, osDescription, osGroups); // get the current user

      // try to obtain an enabled group name
      result = "";
      val = paCfgReadValue(conffile, sec, "EnabledOSusersGroup",result);
      alidcsUi_log("Read the OS users group defined :"+result,LOGINFO,"DCSUI::manageAC()");
      if(result != "") {
        if( dynContains(osGroups, result) < 1) { // ok the win user is enabled
          alidcsUi_log("The actual logged user was not enabled ! ("+osFullName+")",LOGERRO,"DCSUI::manageAC()");
          ACDomain = "";
          return(false);
        }
      }
      // the user is enabled and we assign the root role
      ACDomain = getOSDomainName();
      User = osUser;
      Password = "";
      UserId = 0;
      ACExcludePrivi =  "Debug";
      ACDisablePrivi =  "Debug";
      setUserId(0);
      alidcsUi_log("The actual logged user is : "+User+"("+getUserId()+")",LOGINFO,"DCSUI::manageAC()");
    }
    return(true);
  }
  /* ------------------------------------------------------------
  	Funtion for Get the AC grants defined into the config
  	file Grants list

  	F.A. ver 1.0  19/6/2006

  	History

  		19/06/2006	- Creation
  ------------------------------------------------------------ */
  public static void GetACGrants(bool & isDeveloper,bool & isExpert, bool & isOperator, bool & isObserver)
  {
    getACGrants(isDeveloper,isExpert,isOperator,isObserver);
    return;
  }
  private static void getACGrants(bool & isDeveloper,bool & isExpert, bool & isOperator, bool & isObserver)
  {
    dyn_string exceptionInfo;
    if(!useAC) {
      if(ACDomain != "") {
        isObserver = true;
        isOperator = true;
        isExpert = true;
        isDeveloper = true;
      } else {
        isObserver = false;
        isOperator = false;
        isExpert = false;
        isDeveloper = false;
      }
      return;
    }

  		fwAccessControl_isGranted(ACDomain+":Monitor",isObserver, exceptionInfo);
  		if (dynlen(exceptionInfo)) { return; }
      fwAccessControl_isGranted(ACDomain+":Control",isOperator, exceptionInfo);
  		if (dynlen(exceptionInfo)) { return; }
  		fwAccessControl_isGranted(ACDomain+":Debug",isExpert, exceptionInfo);
  		if (dynlen(exceptionInfo)) { return; }
  		fwAccessControl_isGranted(ACDomain+":Modify",isDeveloper, exceptionInfo);
  		if (dynlen(exceptionInfo)) { return; }
    return;
  }
  // Permission Access Control Function
  private static void disableAllAC()
  {
    return;
  }
  private static void enableAC(bool isObserver, bool isOperator, bool isExpert, bool isDeveloper)
  {
    return;
  }
  public static void LoggedUserAC(string dp, string username_from_ui)
  {
    // Set the minimum users grants
    bool isObserver=false;
    bool isOperator=false;
    bool isExpert=false;
    bool isDeveloper=false;

    // Disable all items in the panel
    disableAllAC();
    // Get Grants for the specified user
    getACGrants(isDeveloper,isExpert,isOperator,isObserver);
    // Now enable  - this is related to the specific Policy rules
    enableAC(isObserver,isOperator,isExpert,isDeveloper);

    // Verify if it's a logoff or a return from
    bool bIsNoUserLogin = !(isObserver || isOperator || isExpert || isDeveloper);
    if(!bIsNoUserLogin) disableAllAC();
    return;
  }

   /* -------------------------------------------------------------
	  Function to Display a Panel in the dcsUi (outside tab/button control !)
    F.A. ver 1.0   18/11/2019
  -------------------------------------------------------------- */
  public static void DisplayPanel(string sPanelFileName,
                           dyn_string dsParameters,
                           string MenuID = "MAIN")
  {
    shared_ptr<Menu> theMenu = nullptr;
    theMenu = GetMenuPtr(MenuID);
    theMenu.LoadAndDisplayPanel(sPanelFileName, dsParameters);
    return;
  }

  /* -------------------------------------------------------------
	  Function to open a PopUp windows inside the dcsUi
    F.A. ver 1.0   18/09/2019
  -------------------------------------------------------------- */
  public static int MODLESS = 1;
  public static int MODAL = 2;

  public static void OpenPopUpPanel(string sPanelFileName,
                             string sPanelName,
                             dyn_string dsParameters,
                             int iType = MODLESS )
  {
    if(sPanelFileName == "" || sPanelName == "") return;
    ClosePopUpPanel(sPanelName);
    // then open the pop-up
    if(iType == MODLESS) {
      ChildPanelOnCentral(sPanelFileName, sPanelName, dsParameters);
    } else if(iType == MODAL) {
      ChildPanelOnCentralModal(sPanelFileName, sPanelName, dsParameters);
    }
    return;
  }

  public static void ClosePopUpPanel(string sPanelName)
  {
    if( isPanelOpen(sPanelName, myModuleName() ) ) { // verify if the panel is open
      dyn_anytype da, daa;
      da[1]  = myModuleName();
      da[2]  = sPanelName;
      daa[1] = 0.0; daa[2] = "FALSE"; // Return value optional
      da[3] = daa;                    // dyn_anytype binding
      panelOff(da);
    }
    return;
  }

  // ------------------------------------------------------------
  public static void Dialog(string Message, string ButtonList = "OK", int Type=MODLESS )
  {
    if(Type == MODAL)
      ChildPanelOnCentralModal(DIALOGPANELFILENAME,DIALOGPANELNAME,
                               makeDynString("$Message:"+Message,"$Buttons:"+ButtonList));
    else
      ChildPanelOnCentral(DIALOGPANELFILENAME,DIALOGPANELNAME,
                             makeDynString("$Message:"+Message,"$Buttons:"+ButtonList));
    return;
  }

};

/* -----------------------------------------

   ACCESS CONTROL Wrapper function !

  --------------------------------------- */
void alidcsUi_LoggedUserAC(string dp, string username_from_ui)
{
  DCSUI::LoggedUserAC(dp, username_from_ui);
}

