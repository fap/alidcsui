// $License: NOLICENSE
/* ------------------------------------------------------------

    	ALICE DCS - DCS UI FSM LIBRARY

  This library collect all functions linked to the ALICE UI
  for the FSM Hierarchy objects.

  Ver. 0.6  01/01/2020
  Auth: A.Franco
  Inst. : INFN Sez. BARI - ITALY

       * HISTORY *

05/02/2020    Trasform Hierarchy in non static class
11/05/2020    Add the Save/Restore methods
20/05/2020    Integrate the Restore from File in the Init
24/11/2020    Add the associated Panel to the hierarchy node
18/02/2021    Add the get visibility function and visibility member
22/02/2021    Add versioning in the Hierarchy Files
30/04/2021    Fix the Diff/Restore
06/12/2021    Fix the creation of hierarchy file is the restore fails !
04/04/2022    move the hierarchy files in config/alidcsui/ subfolder

---------------------------------------------------------------*/
#uses "fwFSM/fwFsmUi.ctl"
#uses "fwFSM/fwFsmUtil.ctl"
#uses "fwFSM/fwFsm"
#uses "fwFSM/fwUi"
#uses "fwFSM/fwCU"
#uses "alidcsUi/dcsUiLibrary.ctl"

// ----------  Constant definition ----------------------------
const string DCSFSM_VERSION = "0.9.0"; // Library Version
const string HIERFIELDSEPARATOR = "\t";
const string HIERFILEEXT = ".hyer";

// ==============  Global constant definitions ==================

// FSM node types
const int ALIFSM_CU = 1;
const int ALIFSM_DU = 2;
const int ALIFSM_LU = 0;

const int BUILDOK = 0;
const int BUILDERROR = 1;
const int BUILDREFRESH = 2;

// ==============  Data type/structures definition ==============

// FSM Node descriptor
struct HierarchyNode
{
  int Type; // DU/CU/LU
  string Domain;
  string Node;
  string Label;
  int ptChildren;
  int ptParent;
  int ptBrother;
  bool selected;
  bool visible;
};

// ============== Library Generic/Wrappers functions ============
/* -------------------------------------------------------------
	Get the Label of a Fsm Node.
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : a lot of panels...
void dcsUiFSM_getLabel(string domain, string object, string &label)
{
  string sAp;
  fwUi_getLabel(domain, object, sAp);
  label = sAp;
 // DebugTN(">>  dcsUiFSM_getLabel >>>", domain, object, sAp);
  return;
}
/* -------------------------------------------------------------
	Function to connect the FSM Status with the dpExists test
	F.A. ver 1.0   01/03/2007
        node  := DOMAIN::OBJECT
        dispever := true := calls the CBFunction every time
        return := true if connecton is done

	History
      16/04/2009 - Use the Dpconnect in order to display the
                   status every time
-------------------------------------------------------------- */
bool dcsUiFSM_connectState(string CBFunction, string node, bool dispever=false)
{
  // Verify the existence of FSM Dp
  string sDpName;
  fwCU_getDp(node, sDpName, "STATE");
  if(!dpExists(sDpName)){
    alidcsUi_log(sDpName+" DP not exists for the node :"+node+"!", LOGERRO, "dcsUiFSM_ConnectState()");
    return(false);
  }
  // connect
  if(dispever)
    dpConnect(CBFunction,true,sDpName);
  else
    fwCU_connectState(CBFunction,node);
  return(true);
}
/* -------------------------------------------------------------
	Get the user Panel associated with an FSM object.
	F.A. ver 1.0   25/11/2006
 History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiAuxiliaryWindow.pnl
//           panels\dcsUi\dcsUiMainPanel.pnl
void dcsUiFSM_getUserPanel(string domain, string object, string &PanelName)
{
  string sAp;
  fwUi_getUserPanel(domain, object, sAp);
//DebugTN("dcsUiFSM_getUserPanel()",domain, object, sAp);
  PanelName = sAp;
  return;
}

/* -------------------------------------------------------------
	Get the user Panel Path .
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiAuxiliaryWindow.pnl
string dcsUiFSM_getPanelPath(string Panel)
{
  return(fwUi_getPanelPath(Panel));
}

/* -------------------------------------------------------------
 Test if an object is a DU
 F.A. ver 1.0   25/11/2006
 History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// called by : fwFSMuser\fwUi.pnl
// used in : dcsUi\dcsUiFsmUi.pnl
//           dcsUi\dcsUiFSMtree.pnl
//           dcsUi\dcsUiAuxiliaryWindow.pnl
bool dcsUiFSM_isDU(string domain, string object)
{
  return(fwFsm_isDU(domain,object));
}

/* -------------------------------------------------------------
	Test if an object is a CU
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// calledy by fwFSMuser\fwUi.pnl
// used in : panels\dcsFsmTools\dcsFsmObjNodeWidget.pnl
//          panels\dcsUi\dcsUiFSMObjStatusDisplay.pnl
//       panels\dcsUi\dcsUiFsmUi.pnl
bool dcsUiFSM_isCU(string domain, string object)
{
  return(fwFsm_isCU(domain,object));
}

/* -------------------------------------------------------------
	Get the Node/Obj for associated
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiFsmObjWithDevLock.pnl,
//           panels\dcsUi\dcsUiFsmObjWithLock.pnl,
//           panels\dcsUi\dcsUiFsmUi.pnl
void dcsUiFSM_translateNodeObj(string &node, string &obj)
{
  if(fwFsm_isAssociated(obj)){
    node = fwFsm_getAssociatedDomain(obj);
    obj = fwFsm_getAssociatedObj(obj);
  }else{
    node = node;
    obj = obj;
  }
  return;
}

/* -------------------------------------------------------------
	Get all the domains of the FSM Hierarchy
	F.A. ver 1.0   25/11/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// -----------  called in panels\fwFSM\Root.pnl  ----------------
// used by : dcsUi\dcsUiFSMtree.pnl
dyn_string dcsUiFSM_getDomains()
{
  return( fwFsm_getDomains());
}


/* -------------------------------------------------------------
	Get the Visibility
	F.A. ver 1.0   18/02/2021
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsFsmTools\dcsFsmObjNodeWidget.pnl
bool dcsUiFSM_getVisibility(string node, string object) {
  bool isVisible = false;
  fwUi_getVisibility(node, object, isVisible);
  return(isVisible);
}

/* -------------------------------------------------------------
	Get the Ownership
	F.A. ver 1.0   07/12/2006
	History
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsFsmTools\dcsFsmControllHierarchyWidget.pnl
string dcsUiFSM_getOwnership( string node)
{
  string result;
  fwUi_getOwnership(node, result);
  return(result);
}
// *** NOT Public FSM Function call ***
// used by : panels\dcsFsmTools\dcsFsmObjNodeWidget.pnl
string dcsUiFSM_getModeObj(string node, string object) {
  return(fwUi_getModeObj(node,object));
}
// *** NOT Public FSM Function call ***
// used by : panels\dcsFsmTools\dcsFsmObjNodeWidget.pnl
void dcsUiFSM_checkOwnership(string cu, int &enabled, string &owner) {
  fwUi_checkOwnership(cu,enabled,owner);
  return;
}
// *** NOT Public FSM Function call ***
// used by : panels\dcsFsmTools\dcsFsmObjNodeWidget.pnl
string dcsUiFSM_getManagerIdInfo(string id) {
  return(fwUi_getManagerIdInfo(id));
}
/* ------------------------------------------------------
	Verify that I'm the Owner
	F.A. ver 1.0   27/05/2008
	History
------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// used by : panels\dcsUi\dcsUiFsmUi.pnl
bool dcsUiFSM_isMine(string domain, string object)
{
  bit32 statusBits = fwFsmUi_getModeBits(domain,object);
  string state = fwUi_getCUMode(domain, object);
  string cu = alidcsUi_FsmGetModeObj(domain, object);
  int enabled;
  string owner;
  fwUi_checkOwnership(cu, enabled, owner);
  // FSM cotrol
  if( owner == getSystemName()+"Manager"+myManNum() && getBit(statusBits,FwOwnerBit) ) return(true);
  if( owner != "" && !getBit(statusBits,FwExclusiveBit) )  return(true);
  return(false);
}


/* -------------------------------------------------------------
	Start stop Tree on distributed systems
	F.A. ver 1.0   18/01/2007
	History
    1/1/2012 -  Redefined  and Add the domain
-------------------------------------------------------------- */
// *** NOT Public FSM Function call ***
// -----------  called in fwFsmTreeDisplay.ctl  ----------------
// used by : dcsFsmTools\dcsFsmObjNodeWidget.pnl
int dcsUiFSM_restartTree(string domain, string sysName)
{
    if(substr(sysName,strlen(sysName)-1,1) == ":") {
      sysName = substr(sysName,0,strlen(sysName)-1);
    }
    fwFsm_restartTreeDomains(domain, sysName);
    delay(0,200);
    return(0);
}
void dcsUiFSM_stopTree(string domain, string sysName)
{
    if(substr(sysName,strlen(sysName)-1,1) == ":") {
      sysName = substr(sysName,0,strlen(sysName)-1);
    }
    fwFsm_stopTreeDomains(domain, sysName);
    delay(0,200);
    return;
}
// *** NOT Public FSM Function call ***
// -----------  called in ui/fwFsmOperation.pnl ----------------
// used by : dcsFsmTools\dcsFsmControllHierarchyWidget.pnl
void dcsUiFSM_stopDomain(string sysName="") {
  if(sysName == "")
    fwFsm_stopAllDomains();
  else
    fwFsm_stopAllDomains(sysName);
  delay(0,200);
  return;
}
void dcsUiFSM_restartDomain(string sysName="") {
  if(sysName == "")
    fwFsm_restartAllDomains();
  else
    fwFsm_restartAllDomains(sysName);
  delay(0,200);
  return;
}

// ====================  CLASS DEFINITION =================

class FSMHierarchy
{
  // === Members ===
  private dyn_anytype daTheTree = makeDynAnytype();
  private bool bHierarchyValid = "false";

  // === Methods ===

  // The constructor
  public FSMHierarchy()
  {
    dynClear(daTheTree);
    bHierarchyValid = false;
  }

  public bool IsHierarchyValid() { return(bHierarchyValid); }
  public int GetType(int NodeId) { return(daTheTree[NodeId].Type); }
  public string GetDomain(int NodeId) { return(daTheTree[NodeId].Domain); }
  public string GetObject(int NodeId) { return(daTheTree[NodeId].Node); }
  public string GetLabel(int NodeId) { return(daTheTree[NodeId].Label); }
  public int GetParentId(int NodeId) { return(daTheTree[NodeId].ptParent); }
  public bool GetVisibility(int NodeId) { return(daTheTree[NodeId].visible); }

  public dyn_int GetChildren(int theParent)
  {
    dyn_int diAp;
    int k = daTheTree[theParent].ptChildren;
    while(k > 0) {
      dynAppend(diAp,k);
      k = daTheTree[k].ptBrother;
    }
    return(diAp);
  }
  public int GetNodeId(string objNodeName)
  {
    for(int i=1;i<=dynlen(daTheTree); i++) {
      if(daTheTree[i].Node == objNodeName) {
        return(i);
      }
    }
    return(-1);
  }

  // Dumps the Hierarchy
  public void Dump(int Key = 1, int lev = 1)
  {
    while(Key > 0){
      dumpNode(lev, Key);
      if(daTheTree[Key].ptChildren > 0)
        Dump(daTheTree[Key].ptChildren,lev+1);
      Key = daTheTree[Key].ptBrother;
    }
    return;
  }
  private void dumpNode(int lev, int key)
  {
    string sep = "";
    for(int i=1;i<=lev;i++) sep += "   ";
    DebugTN(sep + daTheTree[key].Domain +"::"+daTheTree[key].Node);
    return;
  }

  private string hierVer = "0.1"; // change this if the format change
  // -------- Save/Restore Hierarchy --------
  private string makeHierFileName(string name)
  {
    return(getPath(CONFIG_REL_PATH)+"alidcsUi/"+ name + HIERFILEEXT);
  }


  public bool Save(string RootName)
  {
    return(save(RootName, daTheTree));
  }

  private bool save(string RootName, dyn_anytype TheTree)
  {
    string fileName = makeHierFileName(RootName);
    string buffer;
    int mx = dynlen(TheTree);

    file fh = fopen(fileName, "w");
    if ( fh <= 0 ) {
      alidcsUi_log("Error to create the file :" +fileName +" Abort !",LOGERRO,"FSMHierarchy::Save()");
      return(false);
    }
    fputs("ALIDCSUI FSMHierarchy version: "+hierVer+"\n", fh);
    int idx;
    for(idx=1; idx<=mx; idx++) {
      buffer = TheTree[idx].Type +HIERFIELDSEPARATOR;
      buffer += TheTree[idx].Domain +HIERFIELDSEPARATOR;
      buffer += TheTree[idx].Node +HIERFIELDSEPARATOR;
      buffer += TheTree[idx].Label +HIERFIELDSEPARATOR;
      buffer += TheTree[idx].ptChildren +HIERFIELDSEPARATOR;
      buffer += TheTree[idx].ptParent +HIERFIELDSEPARATOR;
      buffer += TheTree[idx].visible +HIERFIELDSEPARATOR;
      buffer += TheTree[idx].ptBrother +"\n";
      fputs(buffer, fh);
    }
    fclose(fh);
    alidcsUi_log("Save FSM Hierarchy in the file :" +fileName +". saved :"+ (idx-1) + " nodes !",LOGINFO,"FSMHierarchy::Save()");
    return(true);
  }

  public bool Restore( string RootName )
  {
    string fileName = makeHierFileName(RootName);
    string buffer;
    dyn_string dsAppo;
    shared_ptr<HierarchyNode> n1;
    int count;

    file fh = fopen(fileName, "r");
    if ( fh <= 0 ) {
      alidcsUi_log("Error to access the file :" +fileName +".",LOGWARN,"FSMHierarchy::Restore()");
      return(false);
    }
    dynClear(daTheTree);

    // check the version
    fgets(buffer, 4096, fh); buffer = substr(buffer,0,strlen(buffer)-1);
    dsAppo = strsplit(buffer, " ");
    if(dynlen(dsAppo) != 4 || dsAppo[4] != hierVer) {
//      DebugTN(">>>",dynlen(dsAppo),dsAppo[4] ,strlen(dsAppo[4]), hierVer);
      alidcsUi_log("Wrong version in the file :" +fileName +".",LOGWARN,"FSMHierarchy::Restore()");
      fclose(fh);
      return(false);
    }

    fgets(buffer, 4096, fh); buffer = substr(buffer,0,strlen(buffer)-1);
    while( !feof(fh) ) {
      dsAppo = strsplit(buffer,HIERFIELDSEPARATOR);
      if(dynlen(dsAppo) < 8){
        alidcsUi_log("Wrong format in the file :" +fileName +".",LOGWARN,"FSMHierarchy::Restore()");
        fclose(fh);
        return(false);
      }
      n1 = nullptr;
      n1 = new HierarchyNode;

      n1.Type = (int)(dsAppo[1]);
      n1.Domain = dsAppo[2];
      n1.Node = dsAppo[3];
      n1.Label = dsAppo[4];
      n1.ptChildren = (int)(dsAppo[5]);
      n1.ptParent = (int)(dsAppo[6]);
      n1.ptBrother = (int)(dsAppo[8]);
      n1.visible = (bool) (dsAppo[7]);
      n1.selected = false;//(bool)(dsAppo[8]);

      count++;
      dynAppend(daTheTree, n1);
      fgets(buffer, 4096, fh); buffer = substr(buffer,0,strlen(buffer)-1);
    }
    alidcsUi_log("Restored " + count + " FSM Hierarchy nodes from the file :" +fileName +" !",LOGINFO,"FSMHierarchy::Restore()");
    fclose(fh);
    return(true);
  }

  public bool Diff(string OldFile, string NewFile )
  {
    string oldFileName = makeHierFileName(OldFile);
    file fo = fopen(oldFileName, "r");
    if ( fo <= 0 ) {
      alidcsUi_log("Error to access the file :" +oldFileName +".",LOGWARN,"FSMHierarchy::Diff()");
      return(false);
    }
    string newFileName = makeHierFileName(NewFile);
    file fn = fopen(newFileName, "r");
    if ( fn <= 0 ) {
      alidcsUi_log("Error to access the file :" +newFileName +".",LOGWARN,"FSMHierarchy::Diff()");
      return(false);
    }

    string bufold, bufnew;
    int count = 0;
    fgets(bufold, 4096, fo);
    fgets(bufnew, 4096, fn);
    while( !feof(fo) && !feof(fn) ) {
      // very stupid  diff test
      if( bufold != bufnew) {
        count++;
      }
      fgets(bufold, 4096, fo);
      fgets(bufnew, 4096, fn);
    }
    if( !feof(fo) || !feof(fn)) {
      count++;
    }
    alidcsUi_log("Diff of FSM Hierarchy files "+newFileName+" => "+oldFileName+" Reports "+count+" diferrences !",LOGINFO,"FSMHierarchy::Diff()");
    fclose(fo);
    fclose(fn);
    return( (count == 0) ? true : false);
  }

  // ---- initialization -----
  public void Init(string rootNode)
  {
    string id;
 //   alidcsUi_log("Ver."+ DCSFSM_VERSION+ " FSM component inizialization for Root Node="+rootNode, LOGINFO, "FSMHierarchy::Init()");
    fwFsm_initialize(0);
    fwUi_getAccess(rootNode);
    fwUi_getDisplayInfo(makeDynString(rootNode));
    if(!globalExists("FwFSMUi_ModePanelBusy"))
    		addGlobal("FwFSMUi_ModePanelBusy", INT_VAR);
    id = fwUi_getGlobalUiId();
    fwUi_setManagerIdInfo(id);
    alidcsUi_log(" FSM inizialization for node="+rootNode+" DONE !", LOGINFO, "FSMHierarchy::Init()");
    return;
  }


  public int BuildHierarchy(string sRootNode)
  {
    int result = BUILDOK;
    bHierarchyValid = true;
    if(sRootNode == "") {
      alidcsUi_log("No Root Node specified. Abort !", LOGCRIT, "FSMHierarchy::BuildHierarchy()");
      bHierarchyValid = false;
      return(BUILDERROR);
    }
    if( !Restore(sRootNode) ) {
      result = CreateHierarchy(sRootNode) ? BUILDOK : BUILDERROR;
      if(result == BUILDOK) result = save(sRootNode, daTheTree) ? BUILDOK : BUILDERROR;
    } else {
      dyn_anytype daAppo;
      if( createHierarchy(sRootNode, daAppo) ) {
        if( save(sRootNode+"_new", daAppo)) {
          if(!Diff(sRootNode, sRootNode+"_new" )) {
            remove(makeHierFileName(sRootNode+"_old"));
            rename(makeHierFileName(sRootNode), makeHierFileName(sRootNode+"_old"));
            rename(makeHierFileName(sRootNode+"_new"), makeHierFileName(sRootNode));
            result = BUILDREFRESH;
          }
        } else {
          result = BUILDERROR;
        }
      } else {
        result = BUILDERROR;
      }
      dynClear(daAppo);
    }
    if(result == BUILDERROR) {
      bHierarchyValid = false;
    }
    alidcsUi_log("Terminate to build FSM Hierarchy for "+sRootNode+" result="+result, LOGINFO, "FSMHierarchy::BuildHierarchy()");
    return(result);
  }
  // Builds the Hierarchy
  public bool CreateHierarchy(string sRootNode)
  {
    return(createHierarchy(sRootNode, daTheTree));
  }

  // Builds the Hierarchy : private
  private bool createHierarchy(string sRootNode, dyn_anytype &TheTree)
  {
    alidcsUi_log("Creating the FSM Hierarchy from "+sRootNode+" ... ", LOGINFO, "FSMHierarchy::CreateHierarchy()");

    shared_ptr<HierarchyNode> n1 = new HierarchyNode;
    string sLabel = "";
    string sRootDomain = sRootNode;
    int NumberOfNodes;

    // Create the Root Node
    dcsUiFSM_getLabel(sRootDomain, sRootDomain, sLabel);
    n1.Type = ALIFSM_CU;
    n1.Domain = sRootDomain;
    n1.Node = sRootNode;
    n1.Label = sLabel;
    n1.ptParent = -1;
    n1.ptBrother = 0;
    n1.ptChildren = 0;
    n1.selected = false;
    dynAppend(TheTree, n1);
    NumberOfNodes = buildChildren(1, TheTree) + 1;

    alidcsUi_log("FSM Hierarchy created. "+NumberOfNodes+" nodes in the tree !", LOGINFO, "FSMHierarchy::CreateHierarchy()");
    bHierarchyValid = true;
    return(true);
  }

  private int buildChildren(int theParent, dyn_anytype &TheTree)
  {
    dyn_string dsAppo;
    dyn_int diAppo;
    string  sDomEx, sLabel, localDomain, dom;
    int num, theBrother, count = 0;
    shared_ptr<HierarchyNode> n1;

    dsAppo = fwCU_getChildren(diAppo, TheTree[theParent].Domain + "::"+ TheTree[theParent].Node);
    num = dynlen(dsAppo);
    for(int i = 1; i <= num; i++){
      sDomEx = TheTree[theParent].Domain;
      if(fwFsm_isAssociated(dsAppo[i])) {
		      sDomEx = fwFsm_getAssociatedDomain(dsAppo[i]);
		      if(sDomEx != TheTree[theParent].Domain) {
			       dsAppo[i] = fwFsm_getAssociatedObj(dsAppo[i]);
			       if(fwFsm_isDU(sDomEx,dsAppo[i]))
            diAppo[i] = ALIFSM_DU;
  		    }
      } else {
        sDomEx = TheTree[theParent].Domain;
      }
//DebugTN(">>->",TheTree[theParent].Domain + "::"+ TheTree[theParent].Node,diAppo);
      bool isVisible;
      switch(diAppo[i]) {
      		case ALIFSM_DU:  // DU:
          dcsUiFSM_getLabel(sDomEx, dsAppo[i], sLabel);
          isVisible = dcsUiFSM_getVisibility(sDomEx, dsAppo[i]);
			       localDomain = sDomEx;
	       	 dom = TheTree[theParent].Domain;
      		  localDomain = "";
			       break;
		      case ALIFSM_CU: //CU:
			       dcsUiFSM_getLabel(dsAppo[i], dsAppo[i], sLabel);
          isVisible = dcsUiFSM_getVisibility(dsAppo[i], dsAppo[i]);
			       localDomain = dsAppo[i];
			   	   dom = dsAppo[i];
			       break;
		      case ALIFSM_LU:  // LU OBJ:
			       dcsUiFSM_getLabel(sDomEx, dsAppo[i], sLabel);
          isVisible = dcsUiFSM_getVisibility(sDomEx, dsAppo[i]);
			       localDomain = TheTree[theParent].Domain;
			       dom = localDomain;
			       break;
      }
      n1 = nullptr;
      n1 = new HierarchyNode;
      n1.Type = diAppo[i];
      n1.Domain = dom;
      n1.Node = dsAppo[i];
      n1.Label = sLabel;
      n1.ptParent = theParent;
      n1.ptBrother = 0;
      n1.ptChildren = 0;
      n1.selected = false;
      n1.visible = isVisible;

//DebugTN(">Visible ",dsAppo[i],isVisible?"visible":"hidden");
      dynAppend(TheTree, n1);
      int k = dynlen(TheTree);
      if(i==1) {
        TheTree[theParent].ptChildren = k;
      } else {
        TheTree[theBrother].ptBrother = k;
      }
      theBrother = k;
      if(localDomain != "")			{
        count += buildChildren(k, TheTree);
      }
    }
    count += num;
    return(count);
  }

  public string GetPanel(int theNode)
  {
    string sPanel;
    dcsUiFSM_getUserPanel(daTheTree[theNode].Domain,
                          daTheTree[theNode].Node,
                          sPanel);
    return(sPanel);
  }

  public void SelectNode(int theNode)
  {
    daTheTree[theNode].selected = true;
    return;
  }

  public void DeselectNode(int theNode)
  {
    daTheTree[theNode].selected = false;
    return;
  }

  public void DeselectBrothers(int theNode)
  {
    int theParent = daTheTree[theNode].ptParent;
    int ptV = daTheTree[theParent].ptChildren;
    while(ptV >0) {
      daTheTree[ptV].selected = false;
      ptV = daTheTree[ptV].ptBrother;
    }
    return;
  }

  public bool IsSelected(int theNode)
  {
    return(daTheTree[theNode].selected);
  }


};
