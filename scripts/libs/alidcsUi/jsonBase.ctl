/* -------------------------------------

   JSON library
   auth. Antonio FRANCO -- INFN Bari
   date : 23/Sept/2015
   ver. 1.0

   26/10/2022 -  Add Multidimensional Array support

   -------------------------------------- */


// Positional definition in the Obj array
const int OBJ_TYPE = 1;
const int OBJ_PARENT = 2;
const int OBJ_CHILD = 3;
const int OBJ_SIZE = 4;
const int OBJ_FORMAT = 4;
const int OBJ_VALUE = 5;
const int OBJ_KEY = 5;

// Type of Objects
const int JSONTYPE_ROOT = 0;
const int JSONTYPE_NULL = 1;
const int JSONTYPE_OBJECT = 2;
const int JSONTYPE_ARRAY = 3;
const int JSONTYPE_STRING = 4;
const int JSONTYPE_NUMBER = 5;
const int JSONTYPE_BOOL = 6;
const int JSONTYPE_EXADECIMAL = 7;

const int JSONNUMBER_INTEGER = 1;
const int JSONNUMBER_FLOAT = 2;

// -------  Parsing Error Codes ----------
const int JSONERROR_NOERROR = 0;


// ------------------------
global int parAct = 0;
global int parStr = 0;
global int parEnd = 0;
global char parChar = "";
global int parActItem = 0;
global int jsL = 0;
global int parError =0;
// ------------------------
global bool gbIsPlane = true;
global int giTabSteps=0;

// ------------- a dumping utility --------------
void jsonDumpObject(dyn_dyn_anytype ddaObj)
{
  int i;
  DebugTN( "jsonDumpObject():: Ptr\tType\tParent\tChild\tValue\tSize");
  for(i=1;i<=dynlen(ddaObj); i++){
    DebugTN( i + "\t" + _jspConvertType(ddaObj[i][OBJ_TYPE])+ "\t" + ddaObj[i][OBJ_PARENT] + "\t" + ddaObj[i][OBJ_CHILD]  + "\t" + ddaObj[i][OBJ_VALUE] + "\t" + ddaObj[i][OBJ_SIZE]);
  }
  DebugTN( "------------------------------------------------------------------");
  return;
}

// Converts the type into a plane string
string _jspConvertType(int type)
{
  switch(type) {
    case JSONTYPE_ROOT:
      return("ROOT");
    case JSONTYPE_NULL:
      return("NULL");
    case JSONTYPE_OBJECT:
      return("OBJ");
    case JSONTYPE_NUMBER:
      return("NUM");
    case JSONTYPE_STRING:
      return("STR");
    case JSONTYPE_BOOL:
      return("BOOL");
    case JSONTYPE_EXADECIMAL:
      return("EXD");
    case JSONTYPE_ARRAY:
      return("ARR");
  }
}

// ----------------------------------------------------
// Make a String rappresantation of the JSON object
// Par: bIsPlane := <true> inserts tabs and new line for better rappresentation
//
string jsonStringify(dyn_dyn_anytype ddaObj, bool bIsPlane=false)
{
  string sOut = "";
  int i =1;
  giTabSteps = -1;
  gbIsPlane = bIsPlane;
  sOut =  _jspStringfyItem(i, ddaObj);
  //return(_jspUTFEncodeString( sOut ));
  return(sOut);
}

string _jspStringfyItem(int &i, dyn_dyn_anytype ddaObj)
{
  anytype aBuf;
  int iBuf;
  float fBuf;
  string sBuf;
  string sOut = "";
  int child;
  int parent;
  string sep;


    if (i==0) lilo=pippo;
    int index = i;
    if(i>0)
    switch(ddaObj[i][OBJ_TYPE]){
      case JSONTYPE_NULL: // the root node
        giTabSteps++;
        sOut = sOut +  _jspFormatOut( "{" , false);
        child = ddaObj[i][OBJ_CHILD];
        if(child !=0) parent = ddaObj[child][OBJ_PARENT];
        sep = "";
        while(child != 0 && parent == i) {
          sOut = sOut + _jspFormatOut( _jspStringfyItem(child, ddaObj), false) ;
          if(child > 0 && child<=dynlen(ddaObj)) i = ddaObj[child][OBJ_PARENT];
          if(child != 0 && parent == i) {
            sOut = sOut + ",";
          }
        }
        sOut = sOut +  _jspFormatOut("}", false);
        i = child;
        giTabSteps--;
        break;

      case JSONTYPE_OBJECT:
        giTabSteps++;
        sep = "";
        if(ddaObj[i][OBJ_KEY] != "")
          sOut = sOut + _jspFormatOut("\""+(ddaObj[i][OBJ_KEY])+"\"" +" :" , false);
        child = ddaObj[i][OBJ_CHILD];
        if(ddaObj[child][OBJ_TYPE] == JSONTYPE_OBJECT){ sOut = sOut + "{"; sep = "}";}
        parent = ddaObj[child][OBJ_PARENT];
        while(child != 0 && parent == i) {
          sOut = sOut + _jspStringfyItem(child, ddaObj);
          if(child > 0 && child<=dynlen(ddaObj)) i = ddaObj[child][OBJ_PARENT];
          sOut = sOut + ((child != 0 && parent == i) ?  "," : "");
        }
        if(sep !="") sOut = sOut +  _jspFormatOut(sep, false);
        i = child;
        giTabSteps--;
        break;

      case JSONTYPE_ARRAY:
        giTabSteps++;
        sOut = sOut + _jspFormatOut("\""+(ddaObj[i][OBJ_KEY])+"\"" +" :" , false);
        sOut = sOut +  _jspFormatOut("[" , false);
        child = ddaObj[i][OBJ_CHILD];
        if(child !=0) parent = ddaObj[child][OBJ_PARENT];
        sep = "";
        while(child != 0 && parent == i) {
          sOut = sOut + sep + _jspStringfyItem(child, ddaObj);
          if(child > 0 && child<=dynlen(ddaObj)) i = ddaObj[child][OBJ_PARENT];
          sep = ",";
        }
        sOut =sOut + "]";
        i = child;
        giTabSteps--;
        break;

      case JSONTYPE_NUMBER:
        if(ddaObj[i][OBJ_FORMAT] == JSONNUMBER_INTEGER) {
          iBuf = ddaObj[i][OBJ_VALUE];
          aBuf = iBuf;
        } else {
          fBuf = ddaObj[i][OBJ_VALUE];
          aBuf = fBuf;
        }
        sOut = sOut + aBuf;// _jspFormatOut(aBuf, false);
        i = ddaObj[i][OBJ_CHILD];
        break;
      case JSONTYPE_EXADECIMAL:
        sOut = sOut +"0x"+_jspIntToHEx(ddaObj[i][OBJ_VALUE]);//  _jspFormatOut("0x"+  _jspIntToHEx(ddaObj[i][OBJ_VALUE]), false);
        i = ddaObj[i][OBJ_CHILD];
        break;
      case JSONTYPE_BOOL:
        sOut = sOut + (ddaObj[i][OBJ_VALUE]?"TRUE":"FALSE");//_jspFormatOut( (ddaObj[i][OBJ_VALUE]?"TRUE":"FALSE"), false);
        i = ddaObj[i][OBJ_CHILD];
        break;
      case JSONTYPE_STRING:
        sOut = sOut + "\""+(ddaObj[i][OBJ_VALUE])+"\"";// _jspFormatOut("\""+(ddaObj[i][OBJ_VALUE])+"\"", false);
        i = ddaObj[i][OBJ_CHILD];
        break;
      case JSONTYPE_ROOT:
        //sOut = sOut +  _jspFormatOut( "{" , false);
        child = ddaObj[i][OBJ_CHILD];
        parent = i;
        if(child == 0) { sOut = sOut + "{}"; return(sOut); };
        if(ddaObj[child][OBJ_TYPE] == JSONTYPE_OBJECT){ sOut = sOut + "{"; sep = "}";}
        while(child != 0 && parent == i) {
          sOut = sOut + _jspFormatOut( _jspStringfyItem(child, ddaObj), false) ;
          if(child > 0 && child<=dynlen(ddaObj)) i = ddaObj[child][OBJ_PARENT];
          if(child != 0 && parent == i)  sOut = sOut + ",";
        }
        if(sep !="") sOut = sOut +  _jspFormatOut(sep, false);
        break;
      default:
        i++;
        DebugTN("_jspStringfyItem() :: ITEM TYPE UNKNOWN !");
        break;
    }
  return(sOut);
}


string _jspFormatOut(string sIn, bool newline)
{
  string sOut;
  int i,j;
  if (gbIsPlane) {
    if(true){
      sOut = sOut + "\n";
      for(i=1;i <= giTabSteps; i++)
        sOut = sOut + "\t";
    }
    sOut = sOut + sIn;
    return(sOut);
  }
  return(sIn);
}
// ==========================================================


// ----------------------------------------------------------
// Parse a string and build a JSON object
//
// Return -1 := error
int jsonParse(string js, dyn_dyn_anytype &ddaObj)
{
  _jspReset(ddaObj);
  jsL = strlen(js);
  if(jsL==0) return(-1);
  if(!_jspSkip(js)) return(-1);
  if(!_jspAllocateObj(ddaObj,0,0, JSONTYPE_ROOT)) return(-1);
  if(!_jspParseValue(js,ddaObj,1)) {
    if (parError ==1) return(0); // ignore the end of line ...
    DebugTN("jsonParse() :: Exit wiwh error :"+parError);// _jspReset(ddaObj);
    return(-1);
  }
  return(0);
}

// -----------------------------------------------------
// Returns the last error code of parsing ...
//
int jsonParseGetError(string &theError)
{
  theError = "jsonParseGetError() :: not yet implemented";
  return(parError);
}

// resets the parser
void _jspReset(dyn_dyn_anytype &ddaObj)
{
  dynClear(ddaObj);
  parAct = -1;
  parStr = -1;
  parEnd = -1;
  parActItem = 0;
  parError = 0;
}
// move the parser pointer to the next valuable char
bool _jspSkip(string js)
{
  while(parAct<=jsL && substr(js,++parAct,1) <= " ");
  if(parAct>jsL) {parError = 1; return(false);};
  parChar = substr(js,parAct,1);
  return(true);
}

// parses a char and run the Item specific parsing
bool _jspParseValue(string js,dyn_dyn_anytype &ddaObj, int ptrParent)
{
  if(parChar == "{"){  // Is an object
    return(_jspParseObject(js,ddaObj,ptrParent));
  } else if(parChar == "[") { // Is an array
    return(_jspParseArray(js,ddaObj,ptrParent));
  } else if(parChar == "'" || parChar == "\"" ) { // Is astring
    return(_jspParseString(js,ddaObj,ptrParent));
  } else if(strpos(js,"0x") == parAct || strpos(js,"0X") == parAct) { // Is a exadecimal number
    return(_jspParseNumber(js,ddaObj, ptrParent, 16));
  } else if(parChar == "+" || parChar == "-" || (parChar >= "0" && parChar <= "9")) { // Is a number
    return(_jspParseNumber(js,ddaObj, ptrParent, 10));
  } else if(strtoupper(substr(js,parAct,4)) == "NULL") { // Is a null
    ddaObj[parActItem][OBJ_TYPE] = JSONTYPE_NULL;
    parAct += 3;
    return(true);
  } else if(strtoupper(substr(js,parAct,5)) == "FALSE") { // Is a false
    _jspParseBool(ddaObj, ptrParent,false);
    parAct += 4;
    return(true);
  } else if(strtoupper(substr(js,parAct,4)) == "TRUE") { // Is a true
    _jspParseBool(ddaObj, ptrParent,true);
    parAct += 3;
    return(true);
  } else {
    parError =2;
    DebugTN(">>>> parse error >>",parChar, parAct, substr(js, parAct-5, 10));
    return(false);
  }
}
/*
// parse of object
bool _jspParseObject(string js,dyn_dyn_anytype &ddaObj, int ptrParent)
{
  if(parChar == "{") if(!_jspSkip(js)) return(false);
  if(parChar == "{") { // this is an epty object container ...
//    if(!_jspAllocateObj(ddaObj, ptrParent, parActItem,JSONTYPE_NULL)) return(false);
    if(!_jspParseObject(js, ddaObj, parActItem)) return(false);
    return(true);
  }
   if(parChar == "[") { // this is an epty object container ...
    if(!_jspParseArray(js, ddaObj, ptrParent)) return(false);
    return(true);
  }
  if(parChar == "}"){
//    if(!_jspAllocateObj(ddaObj, ptrParent, parActItem, JSONTYPE_NULL)) return(false);
    if(!_jspSkip(js)) return(false);
    while (parChar == ",") {
      if(!_jspSkip(js)) return(false);
      if(!_jspParseObject(js, ddaObj, ptrParent )) return(false);
      if(!_jspSkip(js)) return(true);
    }
    return(true);
  }
  // Now we espect a string that is a key
  if(!_jspParseString(js,ddaObj, ptrParent)) return(false);// some things goes wrong :(
  ddaObj[parActItem][OBJ_TYPE] = JSONTYPE_OBJECT;
  int objPtr = parActItem; // save the pointer to actual obj

  // now search for the associate value ...
  if(!_jspSkip(js)) return(false);
  if(parChar == ":"){ // OK !
    if(!_jspSkip(js)) return(false);
    if(!_jspParseValue(js, ddaObj, objPtr )) return(false);
    if(!_jspSkip(js)) return(false);
    while (parChar == ",") {
      if(!_jspSkip(js)) return(false);
      if(!_jspParseObject(js, ddaObj, ptrParent )) return(false);
      if(!_jspSkip(js)) return(true);
    }
    if(parChar == "}"){
      return(true);
    } else {
      return(true);
    }
  } else {
    parError =4;
    return(false);
  }
}

bool _jspParseArray(string js,dyn_dyn_anytype &ddaObj, int ptrParent)
{
  if(!_jspAllocateObj(ddaObj, ptrParent, parActItem, JSONTYPE_ARRAY)) return(false); // allocate a new object
  ddaObj[parActItem][OBJ_SIZE] = 0;
  int objPtr = parActItem; // save the pointer to actual array obj
  do {
    if(!_jspSkip(js)) return(false);
    if(parChar == "]") return(true); // is an empty object !
    // Now we espect a value or a Object...
    if(parChar == "{") {
      if(!_jspParseObject(js,ddaObj,objPtr)) return(false);// some things goes wrong :(
    } else {
      if(!_jspParseValue(js,ddaObj,objPtr)) return(false);// some things goes wrong :(
    }
    ddaObj[objPtr][OBJ_SIZE]++;
    DebugTN("_jspParseArray() :: Size of the array ",objPtr,ddaObj[objPtr][OBJ_SIZE]);
    DebugTN("_jspParseArray() :: "+substr(js,parAct-10,20));
    if(!_jspSkip(js)) return(false);
  } while(parChar == ",");
  if(parChar == "]") return(true);
  DebugTN(">>>parse>>",parChar);
  parError =5;
  return(false);
}
*/
// parse of object
bool _jspParseObject(string js,dyn_dyn_anytype &ddaObj, int ptrParent)
{
  if(parChar == "{") if(!_jspSkip(js)) return(false);
  if(parChar == "{") { // this is an epty object container ...
//    if(!_jspAllocateObj(ddaObj, ptrParent, parActItem,JSONTYPE_NULL)) return(false);
    if(!_jspParseObject(js, ddaObj, parActItem)) return(false);
    return(true);
  }
   if(parChar == "[") { // this is an epty object container ...
    if(!_jspParseArray(js, ddaObj, ptrParent)) return(false);
    return(true);
  }
  if(parChar == "}"){
//    if(!_jspAllocateObj(ddaObj, ptrParent, parActItem, JSONTYPE_NULL)) return(false);
    if(!_jspSkip(js)) return(false);
    while (parChar == ",") {
      if(!_jspSkip(js)) return(false);
      if(!_jspParseObject(js, ddaObj, ptrParent )) return(false);
      if(!_jspSkip(js)) return(true);
    }
    return(true);
  }
  // Now we espect a string that is a key
  if(!_jspParseString(js,ddaObj, ptrParent)) return(false);// some things goes wrong :(
  ddaObj[parActItem][OBJ_TYPE] = JSONTYPE_OBJECT;
  int objPtr = parActItem; // save the pointer to actual obj

  // now search for the associate value ...
  if(!_jspSkip(js)) return(false);
  if(parChar == ":"){ // OK !
    if(!_jspSkip(js)) return(false);
    if(!_jspParseValue(js, ddaObj, objPtr )) return(false);
    if(!_jspSkip(js)) return(false);
    while (parChar == ",") {
      if(!_jspSkip(js)) return(false);
      if(!_jspParseObject(js, ddaObj, ptrParent )) return(false);
 //     if(!_jspSkip(js)) return(true);
    }
    if(parChar == "}"){
     // _jspSkip(js);
      return(true);
    } else {
      return(true);
    }
  } else {
    parError =4;
    return(false);
  }
}

bool _jspParseArray(string js,dyn_dyn_anytype &ddaObj, int ptrParent)
{
  if(!_jspAllocateObj(ddaObj, ptrParent, parActItem, JSONTYPE_ARRAY)) return(false); // allocate a new object
  ddaObj[parActItem][OBJ_SIZE] = 0;
  int objArrayPtr = parActItem; // save the pointer to actual array obj
  do {
    if(!_jspSkip(js)) return(false);
    if(parChar == "]") return(true); // is an empty object !
    // Now we espect a value or a Object...
    if(parChar == "{") {
      if(!_jspAllocateObj(ddaObj, objArrayPtr, parActItem,JSONTYPE_OBJECT)) return(false);
      int objObjPtr = parActItem;
      if(!_jspParseObject(js,ddaObj,objObjPtr)) return(false);// some things goes wrong :(
    } else {
      if(!_jspParseValue(js,ddaObj,objArrayPtr)) return(false);// some things goes wrong :(
    }
    ddaObj[objArrayPtr][OBJ_SIZE]++;
//    DebugTN("_jspParseArray() :: Size of the array ",objArrayPtr,ddaObj[objArrayPtr][OBJ_SIZE]);
//    DebugTN("_jspParseArray() :: "+substr(js,parAct-10,20));
    if(!_jspSkip(js)) return(false);
  } while(parChar == ",");
  if(parChar == "]") return(true);
//  DebugTN(">>>parse>>",parChar);
  parError =5;
  return(false);
}

bool _jspParseString(string js,dyn_dyn_anytype &ddaObj, int ptrParent)
{
  if(!_jspAllocateObj(ddaObj, ptrParent, parActItem,JSONTYPE_STRING)) return(false);
  string delim = parChar; // this is the starting delimiter
  string theChar = " ";
  string theBuffer = "";
  string theBackSlash; sprintf(theBackSlash, "%c", 0x5C );
  string theDoubleQuote; sprintf(theDoubleQuote, "%c", 0x22 );
  parStr = parAct+1;
  parEnd = parStr;
  while(parEnd <=jsL && substr(js,parEnd,1) != delim) {
    theChar = substr(js,parEnd,1);
    if(theChar == theBackSlash) { // the escape
      parEnd++;if(parEnd > jsL) return(false);
      switch(substr(js,parEnd,1)){
        case "b": theChar = "\b"; break;
        case "f": theChar = "\f"; break;
        case "n": theChar = "\n"; break;
        case "r": theChar = "\r"; break;
        case "t": theChar = "\t"; break;
        case "u":
          if(++parEnd+4 > jsL) return(false);
          string sUTFcode = substr(js,parEnd,4);
          parEnd += 3;
          sprintf(theChar, "%c", _jspHexToInt(sUTFcode) );
          break;
        case theDoubleQuote:
          sprintf(theChar, "%c", 0x22 );
          break;
        case theBackSlash:
          sprintf(theChar, "%c", 0x5C );
          break;
        default:
          theChar = "";
          break;
      }
    }
    theBuffer += theChar;
    parEnd++;
  }
  if(parEnd>jsL) return(false);
  ddaObj[parActItem][OBJ_VALUE] = theBuffer;
  parAct = parEnd;
  return(true);
}

bool _jspParseNumber(string js,dyn_dyn_anytype &ddaObj,  int ptrParent, int Base=10)
{
  if(!_jspAllocateObj(ddaObj, ptrParent, parActItem, Base == 10 ? JSONTYPE_NUMBER : JSONTYPE_EXADECIMAL )) return(false);
  parStr = parAct;
  parEnd = parStr;
  char theChar = substr(js,parEnd,1);
  while(parEnd <=jsL && _jspIsAValidDigit(substr(js,parEnd,1), Base) ) {
    parEnd++;
  }
  if(parEnd==parStr) {parError =6;return(false);};
  string theNumber = substr(js, parStr, parEnd-parStr);
  anytype theValue;
  if(Base == 16) {
    theValue = _jspHexToInt(theNumber);
    ddaObj[parActItem][OBJ_FORMAT] = JSONNUMBER_INTEGER;
  } else {
    if(strpos(theNumber,".")>=0) {
      theValue = theNumber;
      ddaObj[parActItem][OBJ_FORMAT] = JSONNUMBER_FLOAT;
    } else {
      theValue = theNumber;
      ddaObj[parActItem][OBJ_FORMAT] = JSONNUMBER_INTEGER;
    }
  }
  ddaObj[parActItem][OBJ_VALUE] = theValue;
  parAct = parEnd-1;
  return(true);
}

bool _jspParseBool(dyn_dyn_anytype &ddaObj,  int ptrParent, bool value)
{
  if(!_jspAllocateObj(ddaObj, ptrParent, parActItem, JSONTYPE_BOOL)) return(false);
  ddaObj[parActItem][OBJ_VALUE] = value;
  return(true);
}
string _jspUTFEncodeString( string input)
{
  string theBuffer;
  int i,l;
  unsigned  val;
  blob target;
  l = strlen(input);
  blobZero(target, l+50);
  blobSetValue(target,0 ,input, l);
  for(i=0;i<l;i++) {
    blobGetValue( target, i, val, 1, true );
    if(val >127) {
      theBuffer += "\\u0x"+_jspIntToHEx(1024+val);
    }else if(val == 34) {
      theBuffer += "\\u0x"+_jspIntToHEx(val);
    }else if(val == 39) {
      theBuffer +=  "\\u0x"+_jspIntToHEx(val);
    }else if(val == 92) {
      theBuffer += "\\u0x"+_jspIntToHEx(val);
    }else  if(val < 32) {
      theBuffer += "\\u0x"+_jspIntToHEx(val);
    }else{
      theBuffer += substr(input,i-1,1);
    }
 }
 return(theBuffer);
}

long _jspHexToInt(string theHex)
{
  long res = 0;
  long expo = 1;
  for(int i=strlen(theHex)-1;i>=0;i--) {
    res += expo * _jspHexDigit(substr(theHex,i,1));
    expo = expo * 16;
  }
  return res;
}
string _jspIntToHEx(long theInt)
{
  string sBuf;
  int a = 99;
  int b;
  char c;
  do {
    a = theInt / 16;
    b = theInt % 16;
    c = intToHexchar(b);
    sBuf = c+sBuf;
    theInt = a;
  } while(a>0);
  return(substr("0000"+sBuf,strlen(sBuf),4 ));
}
long _jspHexDigit(string digit)
{
  digit = strtoupper(digit);
  if(digit>="0" && digit <= "9") return(digit);
  switch(digit){
    case "A": return(10);
    case "B": return(11);
    case "C": return(12);
    case "D": return(13);
    case "E": return(14);
    case "F": return(15);
    }
  return(0);
}
bool _jspIsAValidDigit(char theChar, int base=10)
{
  if(theChar >= "0" && theChar <="9") return(true);
  if(theChar >= "A" && theChar <="F" && base == 16) return(true);
  if(theChar >= "a" && theChar <="f" && base == 16) return(true);
  if((theChar == "-" || theChar == "+" || theChar == "." || theChar == "E" || theChar == "e") && base == 10) return(true);
  if((theChar == "X" || theChar == "x") && base == 16) return(true);
  return(false);
}
bool _jspAllocateObj(dyn_dyn_anytype &ddaObj, int parent, int previus,int type )
{
  dynAppend(ddaObj, _jspMakeObj(type, parent));
  parActItem = dynlen(ddaObj);
  if(previus>0) ddaObj[previus][OBJ_CHILD] = parActItem;
  else if(parent>0) ddaObj[parent][OBJ_CHILD] = parActItem;
  return(true);
}
// ==========================================================

// ----------------------------------------------------------
// Create a JSON object
//
// Ret : the pointer to the root object
//
int jsonCreateJSON(dyn_dyn_anytype &ddaObj)
{
  dynClear(ddaObj);
  dynAppend(ddaObj, _jspMakeObj(JSONTYPE_NULL, 0));
  return(1);
}

// ----------------------------------------------------------
// Append an Object item to A JSON Object
//
// Param :  parentObj := a pointer to the node parent of the object to append
//          key := string with the name of new object
//          ddaChildObj := the JSON object to a append
//
// Return = -1 error, else the pointer
//
int jsonAppendItemObject(dyn_dyn_anytype &ddaObj, int parentObj, string key, dyn_dyn_anytype ddaChildObj )
{
  if(parentObj < 1 && parentObj > dynlen(ddaObj)) return(-1); // wrong parent
  if(ddaObj[parentObj][OBJ_TYPE] != JSONTYPE_OBJECT && ddaObj[parentObj][OBJ_TYPE] != JSONTYPE_NULL) return(-1);
  int j = _jspLocateTheLastElement(ddaObj, parentObj);

  int i;
  int l = dynlen(ddaChildObj);
  int k = 1;
  i = dynAppend(ddaObj,ddaChildObj[k]);
  ddaObj[j][OBJ_CHILD] = i;
  ddaObj[i][OBJ_PARENT] = parentObj;
  ddaObj[i][OBJ_KEY] = key;
  ddaObj[i][OBJ_TYPE] = JSONTYPE_OBJECT;
  int parent = i;
  for(k=2;k<=l;k++){
    j = i;
    i = dynAppend(ddaObj,ddaChildObj[k]);
    ddaObj[i][OBJ_PARENT] = ddaObj[i][OBJ_PARENT] + parent -1;
    ddaObj[j][OBJ_CHILD] = i;
  }
  return(i);
}

// ----------------------------------------------------------
// Append a String item to A JSON Object
//
// Param :  parentObj := a pointer to the node parent of the item to append
//          key := string with the name of new object
//          value := the string to append
//
// Return = -1 error, else the pointer
//
int jsonAppendItemString(dyn_dyn_anytype &ddaObj, int parentObj, string key, string value)
{
  int i = _jspAppendItem(ddaObj, parentObj, key);
  if(i==-1) return(-1);
  ddaObj[i+1][OBJ_TYPE] = JSONTYPE_STRING;
  ddaObj[i+1][OBJ_VALUE] = value;
  ddaObj[i+1][OBJ_SIZE] = strlen(value);
  return(i);
}

// ----------------------------------------------------------
// Append an Integer number item to A JSON Object
//
// Param :  parentObj := a pointer to the node parent of the item to append
//          key := string with the name of new object
//          value := the integer number to append
//
// Return = -1 error, else the pointer
//
int jsonAppendItemInteger(dyn_dyn_anytype &ddaObj, int parentObj, string key, int value)
{
  int i = _jspAppendItem(ddaObj, parentObj, key);
  if(i==-1) return(-1);
  ddaObj[i+1][OBJ_TYPE] = JSONTYPE_NUMBER;
  ddaObj[i+1][OBJ_VALUE] = value;
  ddaObj[i+1][OBJ_FORMAT] = JSONNUMBER_INTEGER;
  return(i);
}
// ----------------------------------------------------------
// Append a Float number item to A JSON Object
//
// Param :  parentObj := a pointer to the node parent of the item to append
//          key := string with the name of new object
//          value := the float number to append
//
// Return = -1 error, else the pointer
//
int jsonAppendItemFloat(dyn_dyn_anytype &ddaObj, int parentObj, string key, float value)
{
  int i = _jspAppendItem(ddaObj, parentObj, key);
  if(i==-1) return(-1);
  ddaObj[i+1][OBJ_TYPE] = JSONTYPE_NUMBER;
  ddaObj[i+1][OBJ_VALUE] = value;
  ddaObj[i+1][OBJ_FORMAT] = JSONNUMBER_FLOAT;
  return(i);
}
// ----------------------------------------------------------
// Append an Exadecimal number item to A JSON Object
//
// Param :  parentObj := a pointer to the node parent of the item to append
//          key := string with the name of new object
//          value := the exadecimal number to append
//
// Return = -1 error, else the pointer
//
int jsonAppendItemExadecimal(dyn_dyn_anytype &ddaObj, int parentObj, string key, int value)
{
  int i = _jspAppendItem(ddaObj, parentObj, key);
  if(i==-1) return(-1);
  ddaObj[i+1][OBJ_TYPE] = JSONTYPE_EXADECIMAL;
  ddaObj[i+1][OBJ_VALUE] = value;
  return(i);
}
// ----------------------------------------------------------
// Append a Boolean value item to A JSON Object
//
// Param :  parentObj := a pointer to the node parent of the item to append
//          key := string with the name of new object
//          value := the boolean value to append
//
// Return = -1 error, else the pointer
//
int jsonAppendItemBool(dyn_dyn_anytype &ddaObj, int parentObj, string key, bool value)
{
  int i = _jspAppendItem(ddaObj, parentObj, key);
  if(i==-1) return(-1);
  ddaObj[i+1][OBJ_TYPE] = JSONTYPE_BOOL;
  ddaObj[i+1][OBJ_VALUE] = value;
  return(i);
}
// ----------------------------------------------------------
// Append an Array item to A JSON Object
//
// Param :  parentObj := a pointer to the node parent of the item to append
//          key := string with the name of new object
//
// Return = -1 error, else the pointer
//
int jsonAppendItemArray(dyn_dyn_anytype &ddaObj, int parentObj, string key)
{
  int i,j,l;
  l = dynlen(ddaObj);
  if(parentObj < 1 && parentObj > l) return(-1); // wrong parent
  if(ddaObj[parentObj][OBJ_TYPE] != JSONTYPE_OBJECT && ddaObj[parentObj][OBJ_TYPE] != JSONTYPE_NULL && ddaObj[parentObj][OBJ_TYPE] != JSONTYPE_ARRAY) {
    return(-1);
  }
  j = _jspLocateTheLastElement(ddaObj, parentObj);
  i = dynAppend(ddaObj,_jspMakeObj(JSONTYPE_ARRAY, parentObj));
  ddaObj[i][OBJ_KEY] = key;
  ddaObj[i][OBJ_CHILD] = 0;
  ddaObj[i][OBJ_SIZE] = 0;
  ddaObj[j][OBJ_CHILD] = i;
  return(i);
}
// ----------------------------------------------------------
// Delete a JSON node into the JSON Object
//
// Param :  name := string with the name of the object to delete
//
// Return = -1 error, else 0
//
int jsonDeleteItem(dyn_dyn_anytype &ddaObj, string name)
{
  int ptr = jsonGetItemByName(ddaObj,name);
  if(ptr == -1) return(-1);
  int pch = _jspFindPreviousChild(ddaObj, ptr);
  if(pch == -1) return(-1);
  int nb = _jspFindNextBrother(ddaObj, ptr);
  if(nb == -1) return(-1);
  ddaObj[pch][OBJ_CHILD] = nb;
  while(ptr != nb && ptr != 0) {
    ddaObj[ptr][OBJ_TYPE] = -1;
    ptr = ddaObj[ptr][OBJ_CHILD];
  }
  return(0);
}

int _jspFindPreviousChild(dyn_dyn_anytype ddaObj, int child)
{
  int i;
  for(i=1;i<=dynlen(ddaObj);i++){
    if(ddaObj[i][OBJ_CHILD] == child) {
      return(i);
    }
  }
  return(-1);
}
int _jspFindNextBrother(dyn_dyn_anytype ddaObj, int brother)
{
  int parent = ddaObj[brother][OBJ_PARENT];
  int i = ddaObj[brother][OBJ_CHILD];
  while(i>0 && ddaObj[i][OBJ_PARENT] != parent) {
     i = ddaObj[i][OBJ_CHILD];
  }
  if(i==0) return(-1);
  return(i);
}
// ========================================================

// ----------------------------------------------------------
// ARRAY functions

// ----------------------------------------------------------
// Append an Integer Value to an Array Item
//
// Param :  array := a pointer to the array item
//          value := the integer value to append
//
// Return = -1 error, else the pointer
//
int jsonAppendIntegerToArray(dyn_dyn_anytype &ddaObj, int array, int value)
{
  if(array < 1 && array > dynlen(ddaObj)) return(-1); // wrong parent
  if(ddaObj[array][OBJ_TYPE] != JSONTYPE_ARRAY) return(-2);
  int j = _jspLocateTheLastElement(ddaObj, array);
  int i = dynAppend(ddaObj,_jspMakeObj(JSONTYPE_NUMBER, array));
  ddaObj[i][OBJ_VALUE] = value;
  ddaObj[i][OBJ_FORMAT] = JSONNUMBER_INTEGER;
  ddaObj[i][OBJ_CHILD] = 0;
  ddaObj[j][OBJ_CHILD] = i;
  ddaObj[array][OBJ_SIZE]++;
  return(i);
}
// ----------------------------------------------------------
// Append an Float Value to an Array Item
//
// Param :  array := a pointer to the array item
//          value := the float value to append
//
// Return = -1 error, else the pointer
//
int jsonAppendFloatToArray(dyn_dyn_anytype &ddaObj, int array, float value)
{
  if(array < 1 && array > dynlen(ddaObj)) return(-1); // wrong parent
  if(ddaObj[array][OBJ_TYPE] != JSONTYPE_ARRAY) return(-2);
  int j = _jspLocateTheLastElement(ddaObj, array);
  int i = dynAppend(ddaObj,_jspMakeObj(JSONTYPE_NUMBER, array));
  ddaObj[i][OBJ_VALUE] = value;
  ddaObj[i][OBJ_FORMAT] = JSONNUMBER_FLOAT;
  ddaObj[i][OBJ_CHILD] = 0;
  ddaObj[j][OBJ_CHILD] = i;
  ddaObj[array][OBJ_SIZE]++;
 return(i);
}
// ----------------------------------------------------------
// Append a String Value to an Array Item
//
// Param :  array := a pointer to the array item
//          value := the string value to append
//
// Return = -1 error, else the pointer
//
int jsonAppendStringToArray(dyn_dyn_anytype &ddaObj, int array, string value)
{
  if(array < 1 && array > dynlen(ddaObj)) return(-1); // wrong parent
  if(ddaObj[array][OBJ_TYPE] != JSONTYPE_ARRAY) return(-1);
  int j = _jspLocateTheLastElement(ddaObj, array);
  int i = dynAppend(ddaObj,_jspMakeObj(JSONTYPE_STRING, array));
  ddaObj[i][OBJ_VALUE] = value;
  ddaObj[i][OBJ_SIZE] = strlen(value);
  ddaObj[i][OBJ_CHILD] = 0;
  ddaObj[j][OBJ_CHILD] = i;
  ddaObj[array][OBJ_SIZE]++;
  return(i);
}
// ----------------------------------------------------------
// Append an Boolean Value to an Array Item
//
// Param :  array := a pointer to the array item
//          value := the bool value to append
//
// Return = -1 error, else the pointer
//
int jsonAppendBoolToArray(dyn_dyn_anytype &ddaObj, int array, bool value)
{
  if(array < 1 && array > dynlen(ddaObj)) return(-1); // wrong parent
  if(ddaObj[array][OBJ_TYPE] != JSONTYPE_ARRAY) return(-1);
  int j = _jspLocateTheLastElement(ddaObj, array);
  int i = dynAppend(ddaObj,_jspMakeObj(JSONTYPE_BOOL, array));
  ddaObj[i][OBJ_VALUE] = value;
  ddaObj[i][OBJ_CHILD] = 0;
  ddaObj[j][OBJ_CHILD] = i;
  ddaObj[array][OBJ_SIZE]++;
  return(i);
}
// ----------------------------------------------------------
// Append a Object Value to an Array Item
//
// Param :  array := a pointer to the array item
//          key := a string rapresent the name of the object to append
//          ddaChildObj := the JSON object to append
//
// Return = -1 error, else the pointer
//
int jsonAppendObjectToArray(dyn_dyn_anytype &ddaObj, int array, string key, dyn_dyn_anytype ddaChildObj )
{
  if(array < 1 && array > dynlen(ddaObj)) return(-1); // wrong parent
  if(ddaObj[array][OBJ_TYPE] != JSONTYPE_ARRAY) return(-1);
  int j = _jspLocateTheLastElement(ddaObj, array);
  int i;
  int l = dynlen(ddaChildObj);
  int k = 1;
  int parent = dynAppend(ddaObj,ddaChildObj[k]);
  ddaObj[parent][OBJ_TYPE] = JSONTYPE_OBJECT;
  ddaObj[parent][OBJ_KEY] = ""; // !!! NOT YET IMPLEMENTED ASSOCIATIVE ARRAYS  key;
  ddaObj[parent][OBJ_PARENT] = array;
  ddaObj[j][OBJ_CHILD] = parent;
  j = parent;
  for(k=2;k<=l;k++){
    i = dynAppend(ddaObj,ddaChildObj[k]);
    ddaObj[i][OBJ_PARENT] = ddaObj[i][OBJ_PARENT]+parent-1;
    ddaObj[j][OBJ_CHILD] = i;
    j = i;
  }
  ddaObj[array][OBJ_SIZE]++;
  return(parent);
}

// ----------------------------------------------------------
// Append an Array Item to an Array Item
//
// Param :  array := a pointer to the array item
//          key := a string rapresent the name of the object to append
//          ddaChildObj := the JSON object to append
//
// Return = -1 error, else the pointer
//
int jsonAppendArrayToArray(dyn_dyn_anytype &ddaObj, int array, dyn_dyn_anytype ddaChildObj )
{
  if(array < 1 && array > dynlen(ddaObj)) return(-1); // wrong parent
  if(ddaObj[array][OBJ_TYPE] != JSONTYPE_ARRAY) return(-1);
  int j = _jspLocateTheLastElement(ddaObj, array);
  int i;
  int l = dynlen(ddaChildObj);
  int k = 1;
  int parent = dynAppend(ddaObj,ddaChildObj[k]);
  ddaObj[parent][OBJ_TYPE] = JSONTYPE_ARRAY;
  ddaObj[parent][OBJ_KEY] = ""; // !!! NOT YET IMPLEMENTED ASSOCIATIVE ARRAYS  key;
  ddaObj[parent][OBJ_PARENT] = array;
  ddaObj[j][OBJ_CHILD] = parent;
  j = parent;
  for(k=2;k<=l;k++){
    i = dynAppend(ddaObj,ddaChildObj[k]);
    ddaObj[i][OBJ_PARENT] = ddaObj[i][OBJ_PARENT]+parent-1;
    ddaObj[j][OBJ_CHILD] = i;
    j = i;
  }
  ddaObj[array][OBJ_SIZE]++;
  return(parent);
}

// ----------------------------------------------------------

int _jspLocateTheLastElement(dyn_dyn_anytype ddaObj, int parentObj)
{
  int i = ddaObj[parentObj][OBJ_CHILD];
  int l =dynlen(ddaObj);
  int j = parentObj;
  while(i>0 && i<=l) {
    j=i;
    i=ddaObj[i][OBJ_CHILD];
  }
  return(j);
}
int _jspAppendItem(dyn_dyn_anytype &ddaObj, int parentObj, string key)
{
  int i,j,l;
  l = dynlen(ddaObj);
  if(parentObj < 1 && parentObj > l) return(-1); // wrong parent
  if(ddaObj[parentObj][OBJ_TYPE] != JSONTYPE_OBJECT && ddaObj[parentObj][OBJ_TYPE] != JSONTYPE_NULL && ddaObj[parentObj][OBJ_TYPE] != JSONTYPE_ARRAY) {
    return(-1);
  }
  j = _jspLocateTheLastElement(ddaObj, parentObj);
  i = dynAppend(ddaObj,_jspMakeObj(JSONTYPE_OBJECT, parentObj));
  ddaObj[j][OBJ_CHILD] = i;
  ddaObj[i][OBJ_KEY] = key;
  j = dynAppend(ddaObj,_jspMakeObj(JSONTYPE_NULL, i));
  ddaObj[i][OBJ_CHILD] = j;
  return(i);
}
dyn_anytype _jspMakeObj(int type, int parent)
{
  dyn_anytype daAppo = makeDynAnytype(0,0,0,0,"");
  daAppo[OBJ_TYPE] = type;
  daAppo[OBJ_PARENT] = parent;
  daAppo[OBJ_CHILD] = 0;
  return(daAppo);
}
// ================================================================

// --------------------------------------------------------
// Objects service functions

// --------------------------------------------------------
// Get the pointer to an Object referenced by name
//
// Param : name := string that contains the object name in the dot notation "node1.node2.objectname"
//                 if only the object name is given, the first occurence of theat name is founded
//
// Return = -1 error, else the pointer
//
int jsonGetItemByName(dyn_dyn_anytype ddaObj, string name)
{
  int i,j,n;
  dyn_string dsNames = strsplit(name, ".");
  n = dynlen(dsNames);
  if(n < 1) return(-1);
  int par = 1;
  for(i=1;i<=n;i++) {
    int ptAr = strpos(dsNames[i], "[");
    if(ptAr>=0) { // this is an array element
      string arrname = substr( dsNames[i], 0,ptAr);
      int idx = substr(dsNames[i],ptAr+1, strpos(dsNames[i], "]")-ptAr-1);
      par = _jspFindItemByName(ddaObj, par, arrname);
      if(par == -1) return(-1);
      par = _jspGetArrayElementPointer(ddaObj, par, idx);
    } else {
      par = _jspFindItemByName(ddaObj, par, dsNames[i]);
    }
    if(par == -1) return(-1);
  }
  return(par);
}
int _jspFindItemByName(dyn_dyn_anytype ddaObj, int parent, string name)
{
  int i = parent;
  while(i<=dynlen(ddaObj) && i>0) {
    if(strtoupper(ddaObj[i][OBJ_KEY]) == strtoupper(name)) return(i);
    i = ddaObj[i][OBJ_CHILD];
  }
  return(-1);
}

// --------------------------------------------------------
// Get the value of an item referenced by name
//
// Param : name := string that contains the object name in the dot notation "node1.node2.objectname"
//                 if only the object name is given, the first occurence of theat name is founded
//
// Return = -1 error, 0 ok
//         value := the item value
//
// This return error if an object or an array is adressed
//
int jsonGetItemValue(dyn_dyn_anytype ddaObj, string name,anytype &value)
{
  int ptr = jsonGetItemByName(ddaObj,name);
  if(ptr == -1) return(-1);
  ptr = ddaObj[ptr][OBJ_CHILD];

  if(ddaObj[ptr][OBJ_TYPE] != JSONTYPE_NUMBER &&
     ddaObj[ptr][OBJ_TYPE] != JSONTYPE_STRING &&
     ddaObj[ptr][OBJ_TYPE] != JSONTYPE_EXADECIMAL &&
     ddaObj[ptr][OBJ_TYPE] != JSONTYPE_BOOL) return (-1);
  value = ddaObj[ptr][OBJ_VALUE];
  return(0);
}

// --------------------------------------------------------
// Get the type of an item referenced by name
//
// Param : name := string that contains the object name in the dot notation "node1.node2.objectname"
//                 if only the object name is given, the first occurence of theat name is founded
//
// Return = -1 error, else the type
//
//
int jsonGetItemType(dyn_dyn_anytype ddaObj, string name)
{
  int ptr = jsonGetItemByName(ddaObj,name);
  if(ptr == -1) return(-1);
  ptr = ddaObj[ptr][OBJ_CHILD];
  return(ddaObj[ptr][OBJ_TYPE]);
}
// --------------------------------------------------------
// True if the item, referenced by name, is of the specified type
//
// Param : name := string that contains the object name in the dot notation "node1.node2.objectname"
//                 if only the object name is given, the first occurence of theat name is founded
//
int jsonIsItemNumber(dyn_dyn_anytype ddaObj, string name)
{
  return( jsonGetItemType(ddaObj, name) == JSONTYPE_NUMBER ? true: false);
}
int jsonIsItemExadecimal(dyn_dyn_anytype ddaObj, string name)
{
  return( jsonGetItemType(ddaObj, name) == JSONTYPE_EXADECIMAL ? true: false);
}
int jsonIsItemString(dyn_dyn_anytype ddaObj, string name)
{
  return( jsonGetItemType(ddaObj, name) == JSONTYPE_STRING ? true: false);
}
int jsonIsItemBool(dyn_dyn_anytype ddaObj, string name)
{
  return( jsonGetItemType(ddaObj, name) == JSONTYPE_BOOL ? true: false);
}
int jsonIsItemObject(dyn_dyn_anytype ddaObj, string name)
{
  return( jsonGetItemType(ddaObj, name) == JSONTYPE_OBJECT ? true: false);
}
int jsonIsItemArray(dyn_dyn_anytype ddaObj, string name)
{
  int ptr = jsonGetItemByName(ddaObj,name);
  if(ptr == -1) return(false);
  return( (ddaObj[ptr][OBJ_TYPE] == JSONTYPE_ARRAY) ? true: false) ;
}

// --------------------------------------------------------
// Set the value of an item referenced by name
//
// Param : name := string that contains the object name in the dot notation "node1.node2.objectname"
//                 if only the object name is given, the first occurence of theat name is founded
//         value := the value to set
//
// Return = -1 error, 0 ok
//
// This return error if an object or an array is adressed
//
int jsonSetItemValue(dyn_dyn_anytype &ddaObj, string name,anytype value)
{
  int ptr = jsonGetItemByName(ddaObj,name);
  if(ptr == -1) return(-1);
  ptr = ddaObj[ptr][OBJ_CHILD];
  if(ddaObj[ptr][OBJ_TYPE] == JSONTYPE_NUMBER) {
    if(ddaObj[ptr][OBJ_FORMAT] == JSONNUMBER_INTEGER) {
      int val = value ;
       ddaObj[ptr][OBJ_VALUE] = val;
    } else {
      float val = value ;
       ddaObj[ptr][OBJ_VALUE] = val;
    }
    return(0);
  }
  if(ddaObj[ptr][OBJ_TYPE] == JSONTYPE_STRING) {
    string sval = value ;
    ddaObj[ptr][OBJ_VALUE] = sval;
    return(0);
  }
  if(ddaObj[ptr][OBJ_TYPE] == JSONTYPE_BOOL) {
    bool bval = value ;
    ddaObj[ptr][OBJ_VALUE] = bval;
    return(0);
  }
  if(ddaObj[ptr][OBJ_TYPE] == JSONTYPE_EXADECIMAL) {
    anytype aval = value ;
    ddaObj[ptr][OBJ_VALUE] = aval;
    return(0);
  }
  return(-1);
}

// --------------------------------------------------------
// Get the size of an item referenced by name
//
// Param : name := string that contains the object name in the dot notation "node1.node2.objectname"
//                 if only the object name is given, the first occurence of theat name is founded
//
// Return = the size (-1 error), for the array the number of elements, for strings the lenght of string
//
int jsonGetItemSize(dyn_dyn_anytype ddaObj, string name)
{
  int ptr = jsonGetItemByName(ddaObj,name);
  if(ptr == -1) return(-1);
  if(ddaObj[ddaObj[ptr][OBJ_CHILD]][OBJ_TYPE] == JSONTYPE_ARRAY) {
    ptr = ddaObj[ptr][OBJ_CHILD];
  }
  return(ddaObj[ptr][OBJ_SIZE]);
}

// --------------------------------------------------------
// Get the value of an array element referenced by positional index
//
// Param : name := string that contains the array name in the dot notation "node1.node2.objectname"
//                 if only the object name is given, the first occurence of theat name is founded
//         index := the positional index into the array
//
// Return = -1 error, else 0
//          value := the content of the Indexed element of the array
//
int jsonGetArrayElement(dyn_dyn_anytype ddaObj, string name, int index, anytype &value)
{
  int pt = jsonGetItemByName(ddaObj,name);
  if(pt == -1) return(-1);
  pt = _jspGetArrayElementPointer(ddaObj, pt, index);
  if(pt == -1) return(-1);
  if(ddaObj[pt][OBJ_TYPE] == JSONTYPE_OBJECT) {
  } else {
    value = ddaObj[pt][OBJ_VALUE];
  }
  return(0);
}
// --------------------------------------------------------
// Set the value of an array element referenced by positional index
//
// Param : name := string that contains the array name in the dot notation "node1.node2.objectname"
//                 if only the object name is given, the first occurence of theat name is founded
//         index := the positional index into the array
//         value := the value of the Indexed element of the array
//
// Return = -1 error, else 0
//
int jsonSetArrayElement(dyn_dyn_anytype &ddaObj, string name, int index, anytype value)
{
  int pt = jsonGetItemByName(ddaObj,name);
  if(pt == -1) return(-1);
  pt = _jspGetArrayElementPointer(ddaObj, pt, index);
  if(pt == -1) return(-1);
  ddaObj[pt][OBJ_VALUE] = value;
  return(0);
}

int _jspGetArrayElementPointer( dyn_dyn_anytype ddaObj, int ar, int index )
{
  ar = ddaObj[ar][OBJ_CHILD]; // skips to the Array node
  if(index<=0 || ddaObj[ar][OBJ_TYPE] != JSONTYPE_ARRAY || ddaObj[ar][OBJ_SIZE]<index) return(-1);
  int ptr =ddaObj[ar][OBJ_CHILD];
  int i=1;
  while(i != index) {
    if(ptr == 0 || ptr > dynlen(ddaObj)) return(-1);
    ptr = _jspFindNextBrother(ddaObj,ptr);
    i++;
  }
  return(ptr);
}
// =========================== EOF ================================================
