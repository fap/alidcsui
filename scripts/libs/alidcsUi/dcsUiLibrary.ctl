// $License: NOLICENSE
/* ------------------------------------------------------------

    	ALICE DCS - UI BASE LIBRARY V3

  This library collect all functions linked to the ALICE UI
  Ver. 3.0  11/07/2019
  Auth: A.Franco
  Inst. : INFN Seza. BARI - ITALY

       * HISTORY *

   12/02/2020 _- add alidcsUi_changeColorBrightness(string color, float change)
   27/11/2020 - add alidcsUi_getAllSystems() function
   11/05/2021 - add the DP-Driven mode for notifications
   10/12/2021 - Add the LogBook integration

---------------------------------------------------------------*/
#uses "fwAccessControl/fwAccessControl"
/* ------------------------------------------------------------
	Funtion for Get the Ui Version
	F.A. ver 1.0  24/01/2011

 	History

------------------------------------------------------------ */
const string ALIDCSUI_VERSION = "3.0.9"; // Library Version

string alidcsUi_getVersion()
{
		return(ALIDCSUI_VERSION);
}

/* ------------------------------------------------------------
	Funtion for Log Messages
	F.A. ver 1.0 7/7/2019

 	History

------------------------------------------------------------ */
const int LOGINFO = 4;
const int LOGWARN = 3;
const int LOGERRO = 2;
const int LOGCRIT = 1;
const int LOGDEBU = 9;

global int dcsUiLogLevel = LOGDEBU;
global dyn_string dcsUiLogLevDescriptions = makeDynString("CRITICAL","ERROR","WARNING","INFO","","","","","DEBUG");
global dyn_string dcsUiInfoLogFunctions = makeDynString();

global const string DCSUILOGDPNAME = "dcsUiLoggger";
global const string DCSUILOGDPTYPE = "DcsUiLoggger";

const int LOG_DESTINATION_NULL = 0;
const int LOG_DESTINATION_LOGVIEW = 1;
const int LOG_DESTINATION_FILE = 2;
const int LOG_DESTINATION_DB = 3;
global int dcsUiLogDestinationType = LOG_DESTINATION_LOGVIEW;

global const string DCSUILOGFILENAMEPREFIX = "dcsUiLog";
global const string DCSUILOGFILENAMESUFFIX = ".log";
global string dcsUiLogDestinationFileName = "Template.log";
global string dcsUiLogDestinationBasePath = "";
global int dcsUiLogDestinationFileRotate = 0;
global string dcsUiLogUser = "user";


void alidcsUi_setupLog()
{
  if(!dpExists(DCSUILOGDPNAME)) {
    if(!dpTypeExists(DCSUILOGDPTYPE)) {
      dyn_dyn_string xxdepes;
      dyn_dyn_int xxdepei;

      // Create the data type
      xxdepes[1] = makeDynString (DCSUILOGDPTYPE,"");
      xxdepes[2] = makeDynString ("","level");
      xxdepes[3] = makeDynString ("","source");
      xxdepes[4] = makeDynString ("","message");

      xxdepei[1] = makeDynInt (DPEL_STRUCT);
      xxdepei[2] = makeDynInt (0,DPEL_INT);
      xxdepei[3] = makeDynInt (0,DPEL_STRING);
      xxdepei[4] = makeDynInt (0,DPEL_STRING);
      dpTypeCreate(xxdepes,xxdepei);
      delay(0,200);
    }
    dpCreate(DCSUILOGDPNAME,DCSUILOGDPTYPE);
    delay(0,200);
  }
  return;
}

void alidcsUi_setupLogDestination(int Type, string FileBasePath = "", int FileRotate = 0)
{
  dcsUiLogDestinationType = Type;
  dcsUiLogDestinationBasePath = strrtrim(FileBasePath,"/") + "/";
  dcsUiLogDestinationFileName = dcsUiLogDestinationBasePath + DCSUILOGFILENAMEPREFIX + DCSUILOGFILENAMESUFFIX;
  dcsUiLogDestinationFileRotate = FileRotate;
  mapping m = getOSUser();
  dcsUiLogUser = m["Name"];
  return;
}

void alidcsUi_log(string Message, int level, string Head="dcsUiLibrary.ctl" )
{
  string prefix;
  if(level <= dcsUiLogLevel && level >= 1) {
    if(level <= LOGINFO) {
      prefix = "["+dcsUiLogLevDescriptions[level]+"],"+Head+" {"+Message +"}" ;
    } else {
      dyn_string dsStack = getStackTrace();
      prefix = "[DEBUG],DCSUI::"+Head +" {"+Message+"}" ;
     	prefix += "\n\tCalls stack :";
      for(int j=2;j<=dynlen(dsStack);j++){
        prefix += "\n\t\t" + dsStack[j];
      }
    }
    switch(dcsUiLogDestinationType) {
      case LOG_DESTINATION_NULL:
        return;
      case LOG_DESTINATION_LOGVIEW:
        DebugTN(prefix);
        return;
      case LOG_DESTINATION_FILE:
        alidcsUi_openLogStream();
        alidcsUi_injectLogStream(prefix);
        alidcsUi_closeLogStream();
        return;
      case LOG_DESTINATION_DB:
        DebugTN(" Log on DB not yet implemented ! ");
        return;
    }
  }
  return;
}

int alidcsUi_logGetTypeLevel(string description)
{
  int i = dynContains(dcsUiLogLevDescriptions, description);
  return(i);
}

string alidcsUi_logTypeTranslation(int level)
{
  return(dcsUiLogLevDescriptions[level]);
}

string alidcsUi_logTypeColor(int level)
{
  string color;
  switch(level) {
    case LOGINFO:
      color = "FwStateOKPhysics";
      break;
    case LOGWARN:
      color = "FwStateAttention2";
      break;
    case LOGERRO:
      color = "FwStateAttention3";
      break;
    case LOGCRIT:
      color = "FwTrendingCurve7";
      break;
    case LOGDEBU:
      color = "FwStateOKNotPhysics";
      break;
    default:
      color = "FwTrendingFaceplateBackground";
  }
  return(color);
}

/* -------------------------------------------------------------------------

  Function that decode a packed message into a Dyn string
  Return the result : dyn string

------------------------------------------------------------------------- */
dyn_string alidcsUi_logDecodePackedMsg(string packed, string sep = DCSUILOG_SEP)
{
  dyn_string Buffer = strsplit(packed, sep);
  if(dynlen(Buffer) < 4) dynAppend(Buffer, makeDynString("", "", "", ""));
  Buffer[3] = substr(Buffer[3],1,strlen(Buffer[3])-2); // remove '['
  Buffer[4] = strrtrim(Buffer[4], " "); // remove blancks
  return(makeDynString(Buffer[3],Buffer[2],Buffer[4],Buffer[1]));
}

string __alidcsUi_logGetTSfromPacked(string packed, int precision = 23)
{
   return(substr(packed,0,precision));
}

void alidcsUi_setLogLevel(int newLogLevel)
{
  alidcsUi_setupLog();
  dcsUiLogLevel = newLogLevel;
  return;
}
int alidcsUi_getLogLevel()
{
  return(dcsUiLogLevel);
}

void alidcsUi_registerLoggerFunction(string functionName)
{
  if(isFunctionDefined(functionName)) {
    if(dynContains(dcsUiInfoLogFunctions, functionName) == 0) {
      dynAppend(dcsUiInfoLogFunctions,functionName);
      alidcsUi_log("Register the log wrapper function "+functionName+"()",LOGINFO,"alidcsUi_registerLoggerFunction()");
    }
  } else {
    alidcsUi_log("Fail to register log wrapper function "+functionName+"() !",LOGERRO,"alidcsUi_registerLoggerFunction()");
  }
  return;
}

void alidcsUi_unregisterLoggerFunction(string functionName)
{
  int i =  dynContains(dcsUiInfoLogFunctions, functionName);
  if(i>0) {
    dynRemove(dcsUiInfoLogFunctions,i);
  }
  return;
}

void alidcsUi_sendLogMsg(int type, string source, string message)
{
  dpSetWait(DCSUILOGDPNAME+".level",type,DCSUILOGDPNAME+".source",source,DCSUILOGDPNAME+".message",message);
  if(dynlen(dcsUiInfoLogFunctions) > 0) {
    alidcsUi_log("Redirect the log message:"+message+" to LogFunctions()",LOGDEBU,"alidcsUi_sendLogMsg");
    for(int i=1;i<=dynlen(dcsUiInfoLogFunctions);i++) {
      callFunction(dcsUiInfoLogFunctions[i],type, source, message);
    }
  } else {
    alidcsUi_log(message,type,source);
  }
  return;
}

void alidcsUi_getLoggerDPEs(string &type, string &source, string &message)
{
  alidcsUi_setupLog();
  type = DCSUILOGDPNAME+".level";
  source = DCSUILOGDPNAME+".source";
  message = DCSUILOGDPNAME+".message";
  return;
}

global file dcsUiLogFileHandler;
global const int DCSUILOGMAXFILEDIMENSION = 10000000;
const string DCSUILOG_TIMEFORMAT = "%Y.%m.%d %H:%M:%S";
global const int DCSUILOGMAXMESSAGEDIMENSION = 256;
global const char DCSUILOG_SEP = ',';

bool alidcsUi_injectLogStream(string Message)
{
  string Buffer;
  string timestamp;
  timestamp = formatTime(DCSUILOG_TIMEFORMAT, getCurrentTime(),".%03d");
  Buffer = timestamp + DCSUILOG_SEP + dcsUiLogUser +"@"+ getSystemName()+DCSUILOG_SEP+ Message;
  Buffer = substr(Buffer + strexpand("\\fill{ }", DCSUILOGMAXMESSAGEDIMENSION), 0,DCSUILOGMAXMESSAGEDIMENSION-1) ;
  int res = fputs(Buffer+"\n",dcsUiLogFileHandler);
  if(res!=0) {
    DebugTN(" alidcsUi_injectLogStream(): Error Writing into the File ! ("+res+")");
    return(false);
  }
  return(true);
}

bool alidcsUi_openLogStream()
{
  bool bResult = true;
  dyn_errClass  err;
  int res;
  int leng,seek;

  switch(dcsUiLogDestinationType) {
    case LOG_DESTINATION_FILE:
      // Open the file
      res = access(dcsUiLogDestinationFileName, F_OK); // Does the file already exist ?
      if (res!=0) {
        DebugTN(" alidcsUi_openLogStream(): File not exists! Create... ["+dcsUiLogDestinationFileName+"]");
        dcsUiLogFileHandler = fopen(dcsUiLogDestinationFileName,"w+"); // Create a new file
      } else {
        dcsUiLogFileHandler = fopen(dcsUiLogDestinationFileName,"a"); // Create a new file
      }
      res = ferror(dcsUiLogFileHandler); // export error
      if (res!=0) {
        DebugTN(" alidcsUi_openLogStream(): Error Opening the File ! ("+res+")");
        return(false);
      }

      // Verify the lenght & switch the file
      seek=fseek(dcsUiLogFileHandler, 0, SEEK_END);
      leng = ftell(dcsUiLogFileHandler);
      if(leng > DCSUILOGMAXFILEDIMENSION)
        if (!alidcsUi_switchLogFile()) return(false);

      bResult = true;
      break;
    case LOG_DESTINATION_DB:
      DebugTN(" alidcsUi_openLogStream(): DB log not yet implemented ");
      bResult = false;
      break;

  }
  return(bResult);
}

void __alidcsUi_analyzeFilter(dyn_string dsFilter, string &Sour,
                               string &Mes, dyn_string &dsTy, string &Search )
{
  int i;
  dyn_string dsAp;
  dyn_string dsRes;
  string sAp;

  Sour = "";
  Mes = "";
  dynClear(dsTy);

  for(i=1;i<=dynlen(dsFilter);i++){
    dsAp = strsplit(dsFilter[i],"=");
    if(dynlen(dsAp) == 2){
    switch(dsAp[1]){
      case "TYPE":
        dynAppend(dsTy,dsAp[2]);
        break;
      case "MESSAGE":
        Mes = dsAp[2];
        break;
      case "SOURCE":
        Sour = dsAp[2];
        break;
      case "TIME":
        Search = dsAp[2];
        break;
    }
    } else
      DebugN(">> Filter ? "+dsFilter[i]);
  }
  return;
}

bool __alidcsUi_isGoodForFilter(string sPacked, string Sour, string Mes, dyn_string dsTy)
{
  bool bResult = true;
  int i;
  if(dynlen(dsTy) > 0){
    int s = strpos(sPacked,"[")+1;
    int e = strpos(sPacked,"]") - s;
    if(dynContains(dsTy, substr(sPacked,s,e)) == 0) return(false);
  }
  if(Sour != "") if(strpos(sPacked,Sour) < 0) return(false);
  if(Mes != "") if(strpos(sPacked,Mes) < 0) return(false);
  return(bResult);
}


/* -------------------------------------------------------------------------
  Function that query the server streams
  Return the result : True / False <bool>
------------------------------------------------------------------------- */
bool alidcsUi_logServerQuery(int &start, int &end, dyn_string filter, dyn_string &results, string sSource="ONLINE ARCHIVE" )
{
  long startpoint, numofrecords;
  int val,i;
  string sFileName,sHandle,fi;
  file fHn;

  switch(dcsUiLogDestinationType) {
    case LOG_DESTINATION_FILE:

      if(sSource == "ONLINE ARCHIVE") {
        sFileName = dcsUiLogDestinationFileName;
      } else {
        sFileName = dcsUiLogDestinationBasePath + sSource;
      }

      i = access(sFileName, R_OK); // Does the file already exist ?
      if (i!=0) {
        DebugTN("alidcsUi_ServerQuery() :  File "+sFileName+" not exists! ");
        return(false);
      } else {
        fHn = fopen(sFileName,"r"); // Open
      }
      i = ferror(fHn); // export error
      if (i!=0) {
        DebugTN("alidcsUi_ServerQuery() : Error Opening the File "+sFileName+" ! ("+i+")");
        return(false);
      }

       // filter preparation
      string Sour,Mes,Search;
      dyn_string dsTy;
      __alidcsUi_analyzeFilter(filter, Sour, Mes, dsTy, Search);
      if(Search != "") start = alidcsUi_logServerSearch(fHn,Search);  // if it is a search
      if(start == 0) {
        fclose(fHn);
        return(false);
      }
      startpoint = DCSUILOGMAXMESSAGEDIMENSION * (start-1); // Start the extractionread
      numofrecords = end;
      dynClear(results);
      long seek = fseek(fHn, startpoint, SEEK_SET);

      i=0;
      int p=start;
      string sAp;
      while(i<numofrecords) {
        if (fgets(sAp,DCSUILOGMAXMESSAGEDIMENSION,fHn) > 0 )  {
          sAp = strrtrim(sAp,"\n");
          if( __alidcsUi_isGoodForFilter(sAp, Sour, Mes, dsTy)){
            dynAppend(results,sAp);
            if(i==0) start = p;
            i++;
          }
        } else {
          end = p;
          fclose(fHn);
          return(i);
        }
        p++;
      }
      end = p-1;
      fclose(fHn);
      return(i);
    break;
  }
  return(false);
}

int alidcsUi_logServerSearch(file StreamHandler, string TimeStamp)
{
  int i,n,j,k;
  string sBuffer, sAp, sApF;
  int len,nofrec,seek,pnt,st,en;
  int pack_len = DCSUILOGMAXMESSAGEDIMENSION+1;
  sApF = substr(TimeStamp+"                ",0,19);
  // start the binomial search....
  long seek = fseek(StreamHandler, 0, SEEK_END);
  len = ftell(StreamHandler);
  nofrec = (len / pack_len);
  st = 1; en = nofrec;
  while(st <= en){
    j = (en + st) / 2;
    pnt = pack_len * (j-1);
    seek = fseek(StreamHandler, pnt, SEEK_SET);
    fgets(sBuffer,pack_len+2,StreamHandler);
    sAp = __alidcsUi_logGetTSfromPacked(sBuffer, 19); // Search @ seconds
    if(sAp == sApF)
      return(j);
    else {
      if(sAp < sApF){
        st = j+1;
      } else {
        en = j-1;
      }
    }
  }
  return(j);
}

bool alidcsUi_closeLogStream()
{
  switch(dcsUiLogDestinationType) {
    case LOG_DESTINATION_FILE:
      fclose( dcsUiLogFileHandler );
      break;
    case LOG_DESTINATION_DB:
      DebugTN(" alidcsUi_closeLogStream(): DB log not yet implemented ");
      break;
  }
  return(true);
}

/* -------------------------------------------------------------------------

  Function that Switch the Local file in order to mantain a low dimension

------------------------------------------------------------------------- */
bool alidcsUi_switchLogFile()
{
  int res;
  string sCommand,sAp;
  string sFileName, sFileName1;
  sFileName = dcsUiLogDestinationFileName;
  sFileName1 = dcsUiLogDestinationBasePath + DCSUILOGFILENAMEPREFIX  + DCSUILOGFILENAMESUFFIX + formatTime(".%Y%m%d%H%M%S",getCurrentTime());

  fclose(dcsUiLogFileHandler);
  copyFile(sFileName, sFileName1, true);

//  if(_WIN32){
//    strreplace(sFileName, "/", "\\");
//    sCommand = "cmd /C ren "+sFileName+" "+DCSUILOGFILENAMEPREFIX + formatTime("%Y%m%d%H%M%S",getCurrentTime()) + DCSUILOGFILENAMESUFFIX;
//     DebugTN(" >>>>>>>>>> "+sCommand);
//    system(sCommand);
//  } else {
//    system("mv "+sFileName+" "+sFileName1);
//  }
 // doesn't works  rename(sFileName,sFileName1);
  dcsUiLogFileHandler = fopen(sFileName,"w+"); // Create a new file
  res = ferror(dcsUiLogFileHandler); // export error
  if (res!=0) {
     DebugTN(" alidcsUi_switchLogFile(): Error Opening the File ! ("+res+")");
     return(false);
  }
  DebugTN(" alidcsUi_switchLogFile(): Store to "+sFileName1);
  return(true);
}

bool alidcsUi_logGetOfflineFileList(dyn_string &dsArchiveFileName)
{
 // DebugTN(">>",dcsUiLogDestinationBasePath, DCSUILOGFILENAMEPREFIX  + DCSUILOGFILENAMESUFFIX + ".*");
  dsArchiveFileName = getFileNames(dcsUiLogDestinationBasePath, DCSUILOGFILENAMEPREFIX  + DCSUILOGFILENAMESUFFIX + ".*", FILTER_FILES);
//DebugTN(dsArchiveFileName);
  return(true);
}


/* ------------------------------------------------------------
	Funtions for Manage the scrolling Area

	F.A. ver 1.0  07/11/2019

	History

		07/11/2019	- Creation
  07/04/2021 - Add the Broadcast on the Distributed

------------------------------------------------------------ */
const string SCRMSG_DPTYPENAME = "DcsUiScrMsg";
const string SCRMSG_SYSTEMNAME = getSystemName();
const string SCRMSG_DPBASENAME = "dcsUiScrMsg";
const string SCRMSG_DPNAME = SCRMSG_SYSTEMNAME + SCRMSG_DPBASENAME;

const int SCRMSG_MODE_HIDDEN = 0;
const int SCRMSG_MODE_SCROLL = 1;
const int SCRMSG_MODE_BLINK = 2;
const int SCRMSG_MODE_FIXED = 3;
const int SCRMSG_MODE_DPDRIVEN = 9;

string alidcsUi_getScrolMessageModeName(int iMode)
{
  switch(iMode) {
    case SCRMSG_MODE_HIDDEN:
      return("HIDDEN");
      break;
    case SCRMSG_MODE_SCROLL:
      return("SCROLL");
      break;
    case SCRMSG_MODE_BLINK:
      return("BLINK");
      break;
    case SCRMSG_MODE_FIXED:
      return("FIXED");
      break;
    case SCRMSG_MODE_DPDRIVEN:
      return("DPDRIVEN");
  }
  return("FIXED");
}

int alidcsUi_getScrolMessageModeIdx(string sMode)
{
  switch(sMode) {
    case "HIDDEN":
      return(SCRMSG_MODE_HIDDEN);
      break;
    case "SCROLL":
      return(SCRMSG_MODE_SCROLL);
      break;
    case "BLINK":
      return(SCRMSG_MODE_BLINK);
      break;
    case "FIXED":
      return(SCRMSG_MODE_FIXED);
      break;
    case "DPDRIVEN":
      return(SCRMSG_MODE_DPDRIVEN);
  }
  return(SCRMSG_MODE_FIXED);
}

void alidcsUi_createScrollMessageInterface(int dpInterfaceNumber)
{
  if(!dpTypeExists(SCRMSG_DPTYPENAME)) {  // Create the data type
    dyn_dyn_string dpelements;
    dyn_dyn_int dpelementstype;
    dpelements[1] = makeDynString (SCRMSG_DPTYPENAME,"");
    dpelements[2] = makeDynString ("","message");
    dpelements[3] = makeDynString ("","id");
    dpelements[4] = makeDynString ("","velocity");
    dpelements[5] = makeDynString ("","date");
    dpelements[6] = makeDynString ("","mode");

    dpelementstype[1] = makeDynInt (DPEL_STRUCT);
    dpelementstype[2] = makeDynInt (0,DPEL_STRING);
    dpelementstype[3] = makeDynInt (0,DPEL_INT);
    dpelementstype[4] = makeDynInt (0,DPEL_INT);
    dpelementstype[5] = makeDynInt (0,DPEL_TIME);
    dpelementstype[6] = makeDynInt (0,DPEL_INT);
    int n = dpTypeCreate(dpelements,dpelementstype);
    delay(0,200);
  }
  if(dpCreate(SCRMSG_DPBASENAME+dpInterfaceNumber, SCRMSG_DPTYPENAME) != 0) {
    alidcsUi_log("ERROR creating the Scrolling Message DP ! ("+dpInterfaceNumber+")",LOGERRO,"alidcsUi_createScrollMessageInterface()");
  }
  return;
}

void alidcsUi_setupScrollMessage(string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  string dpN = sSys + SCRMSG_DPBASENAME + interface;
  if(!dpExists(dpN)) { // Install It!
    alidcsUi_createScrollMessageInterface(interface);
  }
  return;
}

string alidcsUi_getScrollingMessage(int &MessageId, string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  string dpN = sSys + SCRMSG_DPBASENAME + interface;
  string sMes = "N.D.";
  if(!dpExists(dpN)) { // Install It!
    alidcsUi_createScrollMessageInterface(interface);
  }
  dpGet(dpN+".message", sMes, dpN+".id", MessageId);
  return(sMes);
}

int alidcsUi_setScrollingMessage(string Message, string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  string dpN = sSys + SCRMSG_DPBASENAME + interface;
  if(!dpExists(dpN)) { // Install It!
    alidcsUi_createScrollMessageInterface(dpN);
  }
  int Id = rand();
  dpSet(dpN+".message", Message, dpN+".id", Id, dpN+".date", getCurrentTime() );
  return(Id);
}

void alidcsUi_setScrollingParameters(int Velocity, int Mode, string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  string dpN = sSys + SCRMSG_DPBASENAME + interface;
  if(!dpExists(dpN)) { // Install It!
    alidcsUi_createScrollMessageInterface(dpN);
  }
  dpSet(dpN+".velocity", Velocity, dpN+".mode", Mode );
  return;
}

void alidcsUi_getScrollingParameters(int &Id, int &Velocity, int &Mode, string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  string dpN = sSys + SCRMSG_DPBASENAME + interface;
  if(!dpExists(dpN)) { // Install It!
    alidcsUi_createScrollMessageInterface(dpN);
  }
  dpGet(dpN+".velocity", Velocity, dpN+".mode", Mode, dpN+".id", Id);
  return;
}

void alidcsUi_setScrollingVelocity(int Velocity, string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  string dpN = sSys + SCRMSG_DPBASENAME + interface;
  if(!dpExists(dpN)) { // Install It!
    alidcsUi_createScrollMessageInterface(dpN);
  }
  dpSet(dpN+".velocity", Velocity );
  return;
}
void alidcsUi_setScrollingMode(int Mode, string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  string dpN = sSys + SCRMSG_DPBASENAME + interface;
  if(!dpExists(dpN)) { // Install It!
    alidcsUi_createScrollMessageInterface(dpN);
  }
  dpSet(dpN+".mode", Mode );
  return;
}
int alidcsUi_getScrollingVelocity(string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  int iVel = 0;
  int iMode = 0;
  int iId = 0;
  alidcsUi_getScrollingParameters(iId, iVel, iMode, sSys, interface);
  return(iVel);
}
int alidcsUi_getScrollingMode(string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  int iVel = 0;
  int iMode = 0;
  int iId = 0;
  alidcsUi_getScrollingParameters(iId, iVel, iMode, sSys, interface);
  return(iMode);
}
int alidcsUi_getScrollingId(string sSys = SCRMSG_SYSTEMNAME, int interface = 0)
{
  int iVel = 0;
  int iMode = 0;
  int iId = 0;
  alidcsUi_getScrollingParameters(iId, iVel, iMode, sSys, interface);
  return(iId);
}

/* ------------------------------------------------------------
	Funtion for Get a Param from the Config File

	F.A. ver 1.0  19/6/2006

	History

		19/06/2006	- Creation
------------------------------------------------------------ */
string alidcsUi_getConfigParam(string section, string name, string user="")
{
  int val = 0;
  string result= "";
  string fi;
  fi = (user == "") ? getPath(CONFIG_REL_PATH) + "dcsUi.config" : getPath(CONFIG_REL_PATH) + "dcsUi_"+user+".config";
  if(paCfgReadValue(fi, section, name ,result) != 0 ) {
    alidcsUi_log("ERROR accessing dcsUi config file! ("+fi+":"+section+":"+name+")",LOGERRO,"alidcsUi_getConfigParam()");
  }
	return(result);
}

bool alidcsUi_setConfigParam(string section, string name, anytype value)
{
  int val = 0;
  string result= "";
  string fi = getPath(CONFIG_REL_PATH) + "dcsUi.config";
  val = paCfgDeleteValue(fi, section, name);
  if(paCfgInsertValue(fi, section, name ,value) != 0 ) {
    alidcsUi_log("ERROR writing dcsUi.config file! ("+fi+":"+section+":"+name+")",LOGERRO,"alidcsUi_setConfigParam()");
    return(false);
  }
	return(true);
}

string alidcsUi_getUserConfigFile(string user="")
{
  if(user=="") fwAccessControl_getUserName(user);
  string fi = getPath(CONFIG_REL_PATH) + "dcsUi_"+user+".config";
  if(!isfile(fi)) {
    alidcsUi_log("ERROR accessing config file! ("+fi+")",LOGERRO,"alidcsUi_getUserConfigFile()");
  }
	return(fi);
}


/* -------------------------------------------------------------
	Function for get the status of a One Manager

	F.A. ver 1.0   03/10/2006

	History

		03/10/2006	- Creation
-------------------------------------------------------------- */
bool alidcsUi_getMgrState(string sSystemName, string sManagerName, string sManType, int iManNum = -1 )
{
	dyn_int diManNums;
	string sManTypeU = "";
	sManType = strtoupper(substr(sManType,0,1))+strtolower(substr(sManType,1));
	sManTypeU = strtoupper(sManType);

	if(iManNum == -1)
		dpGet(sSystemName+":_Connections."+sManType+".ManNums:_original.._value",diManNums);
	else
		dynAppend(diManNums,iManNum);

	return(alidcsUi_getMgrsState(sSystemName, sManagerName, sManType, diManNums));
}

/* -------------------------------------------------------------
	Function for get the status of a List of Managers

	F.A. ver 1.0   03/10/2006

	History

		03/10/2006	- Creation
-------------------------------------------------------------- */
bool alidcsUi_getMgrsState(string sSystemName, string sManagerName, string sManType, dyn_int diManNums)
{
  dyn_string dsResults;
 	dyn_string dsAppo;
  string sResult = "";
  int count =0;
 	int j = 0;
 	int i=0;
 	string sDebDP = "";
 	string sManTypeU = "";

  sManType = strtoupper(substr(sManType,0,1))+strtolower(substr(sManType,1));
  sManTypeU = strtoupper(sManType);
  for(i=1;i<=dynlen(diManNums);i++) {
  		sDebDP = sSystemName+":_CtrlDebug_"+sManTypeU+"_"+(diManNums[i]);
    if (dpExists(sDebDP)) {
      count = 0;
      dynClear(dsResults);
      dpSet(sDebDP+".Result:_original.._value", makeDynString());
     	dpSetWait(sDebDP+".Command:_original.._value", "info scripts");
      while ( dynlen(dsResults) < 1 && count < 40) {
  		   	delay(0,50);
      		dpGet(sDebDP+".Result:_online.._value",  dsResults);
    				count ++;
   			}
    		for(j=1; j <= dynlen(dsResults);j++) {
    				dsAppo = strsplit(dsResults[j], ";");
    				if( dsAppo[dynlen(dsAppo)] == sManagerName)	return(true);
			   }
		  }
	 }
  return(false);
}

const int FONT_NORMAL = 0;
const int FONT_BOLD = 1;

string alidcsUi_makeFont(int dim=11, bool isBold=false, bool isItalic=false) {
  return(  DCSUI_FONT_FAMILY +","+dim+",-1,5,"+(isBold ? FONT_BOLD:FONT_NORMAL)+","+(isItalic ? 1: 0)+",0,0,0,0");
}

string alidcsUi_getLineString(int iLineType, int iThick=1)
{
  switch(iLineType) {
    case 0: // Continuous
      return("[solid,oneColor,JoinMiter,CapNotLast,"+iThick+"]");
    case 1:
      return("[dashed,oneColor,JoinMiter,CapNotLast,"+iThick+"]");
    case 2:
      return("[dotted,oneColor,JoinMiter,CapNotLast,"+iThick+"]");
    case 3:
      return("[dash_dot,oneColor,JoinMiter,CapNotLast,"+iThick+"]");
    case 4:
      return("[dash_dot_dot,oneColor,JoinMiter,CapNotLast,"+iThick+"]");
    default:
      return("[solid,oneColor,JoinMiter,CapNotLast,"+iThick+"]");
  }
}

// =============================================================
//     DCS UI COLOR MANAGEMENT SECTION
// =============================================================

/* ------------------------------------------------

Name: 	dcsUiColor_val2Color()
Desc:   Convert a value into a colour string

inp :   temp := float    (value to convert)
	t_min := int      (minimum of the scale)
	t_max := int      (maximum of the scale)

out :   Colour := string  "{red,green,blu}"

Note : the range values varies from 0 to 130
       over range values are marked with blue
       and red colours

Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
string alidcsUi_val2Color(float val, int v_min = 0, int v_max = 130)
{
  int slo = 4;
  float r,g,b;
  int ir,ig,ib;
  // scale the value
  val = val - v_min;
  val = val / ((float)(v_max - v_min));

  if(val < 0.125) {
    r = 0.0; g = 0.0; b = (val + 0.125) * slo;
  } else	if(val < 0.375) {
    r = 0.0; g = (val - 0.125) * slo; b = 1.0;
  } else if(val < 0.625) {
    r = (val - 0.375) * slo; g = 1.0; b = 1.0 - (val - 0.375) * slo;
  } else if(val < 0.875) {
    r = 1.0; g = 1.0 - (val - 0.625) * slo; b = 0.0;
  } else {
    r = 1.0 - (val - 0.875) * slo; g = 0.0; b = 0.0;
  }
  ir = r * 255;
  ig = g * 255;
  ib = b * 255;
  return("{"+ir+","+ig+","+ib+"}");
}


/* ------------------------------------------------

Name: 	dcsUiColor_convertColorName()
Desc:   Convert a colour name into a colour string

inp :  sSearchColor := string    (colour name)

out :   Colour := string  "{red,green,blu}"


Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
string alidcsUi_convertColorName( string sSearchColor )
{
  file f;
  dyn_string dsNames;
  string sBuffer, sPath;
  int i,j,n, res, len;

 // DebugTN("Start search..." );

  for(i=1;i<= SEARCH_PATH_LEN ;i++){
    sPath = getPath(COLORDB_REL_PATH,"", 1,i);
    dsNames = getFileNames(sPath,"*");
    n = dynlen(dsNames);
    for(j=1;j<=n;j++){
      if(strpos(dsNames[j],".lock")>0) continue;
      f = fopen(sPath+dsNames[j],"r");
      while(feof(f) == 0){
        if( (len = fgets(sBuffer,1000,f)) > 0)
          if( (res = strpos(sBuffer, sSearchColor)) > 0)
            if( (res = strpos(sBuffer, "[")) > 0) {fclose(f); return(substr(sBuffer,res,len-res-1));}
            else if( (res = strpos(sBuffer, "{")) > 0) {fclose(f); return(substr(sBuffer,res,len-res-1));}
            else if( (res = strpos(sBuffer, "<")) > 0) {fclose(f); return(substr(sBuffer,res,len-res-1));}
      }
      fclose(f);
    }
  }
//  DebugTN("...end search!" );
  return("");
 }

/* ------------------------------------------------

Name: 	dcsUiColor_string2RGB()
Desc:   Convert a colour string into the RGB components

inp :   color := string    (colour string)

out :   r:= int      (red component)
	g:= int      (green component)
	b:= int      (blu component)

Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
dyn_int alidcsUi_string2RGB(string color, int &r, int &g, int &b)
{
  dyn_string dsAp;
  color = substr(color,1,strlen(color)-1);
  dsAp = strsplit(color,",");
  r = dsAp[1];
  g = dsAp[2];
  b = dsAp[3];
  return(makeDynInt(r,g,b));
}

/* ------------------------------------------------

Name: 	dcsUiColor_luminosity()
Desc:   Calculate the luminosity from RGB

inp :   r:= int      (red component)
        g:= int      (green component)
        b:= int      (blu component)

out :  luminosity:= float

Auth : A.Franco - INFN Bari

// from Hermann Fuchs

Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
float alidcsUi_luminosity(int r, int g, int b)
{
  float normalizedR,normalizedG,normalizedB;
  float calcR,calcG,calcB;
  float luminosity;

  normalizedR = r/255.0;
  normalizedG = g/255.0;
  normalizedB = b/255.0;

  //calcR
      if (normalizedR<=0.03928) calcR=normalizedR/12.92;
      else  calcR= pow((normalizedR+0.055),2.4);
  //calcG
      if (normalizedG<=0.03928) calcG=normalizedG/12.92;
      else  calcG= pow((normalizedG+0.055),2.4);
  //calcB
      if (normalizedB<=0.03928) calcB=normalizedB/12.92;
      else  calcB= pow((normalizedB+0.055),2.4);

  luminosity = 0.2126 * calcR + 0.7152 * calcG + 0.0722 * calcB;
  return(luminosity);

}

/* ------------------------------------------------

Name: 	dcsUiColor_RGB2Croma()
Desc:   Convert RGB components into a HSV

inp :  red:= int      (red component)
      	green:= int      (green component)
       blu:= int      (blu component)

out : 	h := float  (hue value)
	      s := float  (saturation value)
      	v := float  (intensity value)
 return hue value;

Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
float alidcsUi_RGB2Croma(int red, int green, int blu, float &h, float &s, float &v)
{
  // -------------
  float mn=red;
  float mx=red;
  int maxVal=0;

  if (green > mx){
    mx=green;
    maxVal=1;
  }
  if (blu > mx){
    mx = blu;
    maxVal=2;
  }
  if (green < mn)
    mn=green;
  if (blu < mn)
    mn=blu;

  float delta = mx - mn;

  v = mx;
  if( mx != 0 )
    s = delta / mx;
  else {
    s = 0;
    h = 0;
    return(0.0);
  }
  if (s == 0.0) {
    h=-1;
    return(h);
  } else {
    switch (maxVal) {
      case 0:
        h = (float)( green - blu ) / delta;
        break;
      case 1:
        h = 2 + ( blu - red ) / delta;
        break;
      case 2:
        h = 4 + ( red - green ) / delta;
        break;
    }
  }
  if( h < 0.0 ) h = 5 + h;
  if(h<=1) h *= 120.0;
  else if(h<=2) h = 60.0 + h * 60.0;
  else if(h<=3) h = 120.0 + h * 30.0;
  else if(h<=5) h = 30.0 + h * 60.0;
  else h = 180.0 + h * 30.0;

  return(h);
}

/* ------------------------------------------------

Name: 	dcsUiColor_croma2RGB()
Desc:   convert HSV to RGB

inp: 	h := float  (hue value)
      s := float  (saturation value)
      v := float  (intensity value)

out:    red:= int      (red component)
        green:= int      (green component)
        blu:= int      (blu component)

 return color string;

Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
string alidcsUi_croma2RGB(float h, float s, float v, int &red, int &green, int &blu)
{
    int i;
    float f, p, q, t;
    float hTemp;

    if( s == 0.0 || h == -1.0)
    {
      red = green = blu = v;
      return("{"+red+","+green+","+blu+"}");
    }

    if (h>=360.0) h -= 360.0;

    if(h<=120.0) hTemp = h / 120.0;
    else if(h<=180.0) hTemp = (h-120.0) / 60.0 + 1;
    else if(h<=210.0) hTemp = (h-180.0) / 30.0 + 2;
    else if(h<=270.0) hTemp = (h-210.0) / 60.0 + 3;
    else if(h<=330.0) hTemp = (h-270.0) / 60.0 + 4;
    else hTemp = (h-330.0) / 30.0 + 5;

    i = (int)floor( hTemp );    // which sector
    f = hTemp - i;              // how far through sector
    p = v * ( 1 - s );
    q = v * ( 1 - s * f );
    t = v * ( 1 - s * ( 1 - f ) );

    switch( i ) {
    case 0:{red = v;green = t;blu = p;break;}
    case 1:{red = q;green = v;blu = p;break;}
    case 2:{red = p;green = v;blu = t;break;}
    case 3:{red = p;green = q;blu = v;break;}
    case 4:{red = t;green = p;blu = v;break;}
    case 5:{red = v;green = p;blu = q;break;}
    }
    return("{"+red+","+green+","+blu+"}");
}


/* ------------------------------------------------

Name: 	dcsUiColor_getComplementaryRGB()
Desc:   get the complementary RGB components

inp: 	red:= int      (red component)
     	green:= int      (green component)
      blu:= int      (blu component)

      satTh:= float (Saturation threshold for the Contrast)
      lumTh:= int (Luminosity threshold for the Contrast,	-1 = differenzial)

out:  red:= int     (red component)
      green:= int   (green component)
	     blu:= int     (blu component)

Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
void alidcsUi_getComplementaryRGB(int &red, int &green, int &blu, float satTh = 0.3, int lumTh = -1)
{
  // calculate the luminosity
  float lum;
  lum = alidcsUi_luminosity(red, green, blu);

  // First calculate the Croma
  float croma,sat,val;
  alidcsUi_RGB2Croma(red, green, blu, croma, sat, val);

  if(sat < satTh) {
    if(val < 128) { red = green = blu = 255 ;}
    else { red = green = blu = 0 ;}
    return;
  }
  //  move the hue of 180 degrees
  croma += 180.0;
  if(croma < 0.0) { croma += 360.0; }
  if(croma >= 360.0) { croma -= 360.0; }

  // correct the luminosity
  // -1 := differntial
  if(lumTh == -1) {
    val += 128.0;
    if(val > 255.0) { val -= 255.0; }
  } else {
    // flip flop
    if(lum >= lumTh) {
      val = 0.0;
    } else {
      val = 255.0;
    }
  }

  // return back to RGB
  alidcsUi_croma2RGB(croma, sat, val, red, green, blu);
  return;
}

/* ------------------------------------------------

Name: 	dcsUiColor_getComplementaryColor()
Desc:   return a color string that is the complementary color

  inp: 	color:= string      (color string or static name)
  isBW:= bool (true = Black or White,  false = Color return)

  out:    color string

Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
string alidcsUi_getComplementaryColor(string color, bool isBW = true)
{
  int r,g,b;
  float l;

  if(strpos(color,"{") < 0)
    color = alidcsUi_convertColorName( color );

  alidcsUi_string2RGB(color, r, g, b);

  if(isBW) {
    l = alidcsUi_luminosity(r, g, b);
    if(l<0.7) r = 255; else r = 0;
    return("{"+r+","+r+","+r+"}");
  } else {
    alidcsUi_getComplementaryRGB(r, g, b, 0.3, 120);
    return("{"+r+","+g+","+b+"}");
  }
}

/* ------------------------------------------------

Name: 	dcsUiColor_setColor()
Desc:   sets the foreCol and backCol properties of a shape

  inp: 	shapename:= string      (name of shape)
  color:= string      (color string or color name)
  isBW:= bool (true = Black or White color,	false = Color)

Auth : A.Franco - INFN Bari
Date : 23/09/2008
Ver. : 1.0

Hist : -

------------------------------------------------ */
void alidcsUi_setColor(string shapename, string color, bool isBW = true)
{
  int r,g,b;
  if(strpos(color,"{") < 0)
    color = alidcsUi_convertColorName( color );
  alidcsUi_string2RGB(color, r, g, b);
  if(isBW) {
    l = alidcsUi_luminosity(r, g, b);
    if(l<0.7) r = 255; else r = 0;
    g = r;
	   b = r;
  } else {
    alidcsUi_getComplementaryRGB(r, g, b, 0.3, 120);
  }
  setMultiValue(shapename, "backCol", color,
                shapename, "foreCol", "{"+r+","+g+","+b+"}" );
  return;
}

/* ------------------------------------------------

Name: 	dcsUiColor_changeColorBrightness()
Desc:  returns a color changed for the Brightness

  inp: 	color:= string (starting color)
        brightness:= float   (brightness change percentage)

  out : color string

Auth : A.Franco - INFN Bari
Date : 12/02/2020
Ver. : 1.0

Hist : -

------------------------------------------------ */
string alidcsUi_changeColorBrightness(string color, float change)
{
  if(strpos(color, "{") < 0) color = alidcsUi_convertColorName(color);
  if(color == "") return("{128,128,128}");

  int red, green, blu;
  float h,s,v;
  alidcsUi_string2RGB(color, red, green, blu);
  alidcsUi_RGB2Croma( red, green, blu, h, s, v);
  v = v + (v * change / 100.0);
  return( alidcsUi_croma2RGB(h, s, v, red, green, blu));
}

// -------------------------------------------------------------


// ----- TIme functions --------
time alidcsUi_strToTime(string sTime)
{
  int a;
  int y = 1970;
  int m = 1;
  int d = 1;
  int h = 6;
  int mi = 0;
  int s = 0;
  dyn_string dsAppo = strsplit(sTime, "/.:- ");
  a = dsAppo[1];
  if(dynlen(dsAppo) == 2) {
    h = dsAppo[1];
    mi = dsAppo[2];
  } else {
    if(a > 1000) {
      y = dsAppo[1];
      m = dsAppo[2];
      d = dsAppo[3];
    } else {
      y = (dsAppo[3] < 100 ? 2000 + dsAppo[3]: dsAppo[3]);
      m = dsAppo[2];
      d = dsAppo[1];
    }
    if(dynlen(dsAppo) > 3){
      h = dsAppo[4];
      if(dynlen(dsAppo) > 4){
        mi = dsAppo[5];
        if(dynlen(dsAppo) > 5){
          s = dsAppo[6];
        }
      }
    }
  }
  return( makeTime(y,m,d,h,mi,s) );
}

/* -------------------------------------------------------------
	Function that returns the list of systems with the same
 project prefix

	F.A. ver 1.0   27/11/2020

	History


-------------------------------------------------------------- */
dyn_string alidcsUi_getAllSystems()
{
  dyn_uint ids;
  dyn_string allSystems;
  string localSysName = substr(getSystemName(),0,4)+"*";

  getSystemNames(allSystems, ids);
  dyn_bool istpc=patternMatch(localSysName,allSystems);
  for(int i=dynlen(allSystems); i>0; i--)
    if(!istpc[i])
      dynRemove(allSystems,i);
  return(allSystems);
}

/* -------------------------------------------------------------
	Function to test the status of Net Connection

	F.A. ver 1.0   19/12/2006

	History
  13/11/2009 -  Fixed bug related to multiple access to results...

-------------------------------------------------------------- */
bool alidcsUi_TestNetworkConnection(string sHostName)
{
  string sh_com;
  if(_UNIX) {
  		sh_com = "ping -c 1 -w 1 "+sHostName;
  		system(sh_com + " > " + getPath(DPLIST_REL_PATH) + "res"+sHostName+"ping.txt &");
  		fileToString(getPath(DPLIST_REL_PATH) + "res"+sHostName+"ping.txt",sh_com);
  		return( (strpos(sh_com,"0% packet loss") < 0) ? false : true);
  } else {
  		sh_com = "ping -n 1 -w 1000 "+sHostName;
  		system("cmd /MIN /c " + sh_com + " > " + getPath(DPLIST_REL_PATH) + "res"+sHostName+"ping.txt");
  		fileToString(getPath(DPLIST_REL_PATH) + "res"+sHostName+"ping.txt",sh_com);
  		return( (strpos(sh_com,"Lost = 0") < 0) ? false : true);
  }
}

/* -------------------------------------------------------------
	Function to use Timed Funcion timers...

	F.A. ver 1.0   07/11/2009


	History
          07/11/2009 - created
-------------------------------------------------------------- */
// constants
const string ALIDCSTIMERDP_PREFIX = "dcsUi_";
const string ALIDCSTIMERDP_TAIL = "_timer";
const string ALIDCSTIMERDP_DEFAULT = "generic";

const string ALITIMEDFUNCTION_DPT = "_TimedFunc";
const int ALIDCSTIMER_DEFAULTINTERVAL = 60;

// get the timer DP name
string alidcsUi_getTimerDPName(string type)
{
  if(type == "") type = ALIDCSTIMERDP_DEFAULT;
  return(ALIDCSTIMERDP_PREFIX+strtolower(type)+ALIDCSTIMERDP_TAIL);
}

// Set the timer variable
bool alidcsUi_setTimerDP(string type, int interval = ALIDCSTIMER_DEFAULTINTERVAL)
{
  bool bResult;
  string sDpname = alidcsUi_getTimerDPName(type);
  time t;

  if(!dpExists(sDpname)) {
    if(dpCreate(sDpname, ALITIMEDFUNCTION_DPT) != 0) {
      bResult= false;
      alidcsUi_log("Failed to create a timer="+type+"!",LOGERRO,"alidcsUi_setTimerDP()");
      return(bResult);
    }
  }
  if(interval < 1) interval = ALIDCSTIMER_DEFAULTINTERVAL;

  if( dpSet(getSystemName() +sDpname+".validFrom:_original.._value",setPeriod(t, 0),
        getSystemName() +sDpname+".validUntil:_original.._value",setPeriod(t, 0),
        getSystemName() +sDpname+".time:_original.._value",makeDynInt(),
        getSystemName() +sDpname+".interval:_original.._value",interval,
        getSystemName() +sDpname+".syncTime:_original.._value",-1) == 0) {

    dyn_errClass err = getLastError();
    if (dynlen(err) > 0) {
      errorDialog(err);
      bResult= false;
    } else {
      bResult = true;
    }
  } else {
    bResult= false;
  }
  if(!bResult) {
    alidcsUi_log("Failed to setup a timer="+type+"!",LOGERRO,"alidcsUi_setTimerDP()");
  }
  return(bResult);
}

// stop the timer
void alidcsUi_stopTimer(string type)
{
  string sDpname = alidcsUi_getTimerDPName(type);
  alidcsUi_log("Stop the timer="+type+"!",LOGINFO,"alidcsUi_stopTimer()");
  dpSet(sDpname+".validUntil", getCurrentTime() );
  return;
}


// remove the timer variable
void alidcsUi_removeTimer(string type)
{
  string sDpname = alidcsUi_getTimerDPName(type);
  if(dpExists(sDpname)) {
    dpDelete(sDpName);
    alidcsUi_log("Remove the timer="+type+"!",LOGINFO,"alidcsUi_removeTimer()");
  }
  return;
}

// ============================ EOF ===========================
