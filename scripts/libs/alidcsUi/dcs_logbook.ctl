// OP 2012/10/02 version 1.0
//    2012/11/12 changed DAQ server alias instead of machine name
//---------------------- uses dcs_curl_fill_luminosity_scan.vbs -------------------------//

int logbook_post_fill_luminosity_scan(int ifill, string spng)
{
  int ret;
  string comm, fpath, cpath, fill, filen, dest, out;
  fpath = "C:/Programs/curl/";  // vbscript path
  cpath = "C:/Programs/curl/";  // curl path
  fill  = "\"-F \"fill="+ifill+"\"\"";
  filen = "\"-F \"file=@"+spng+"\"\"";
  dest  = "https://aldaqrest-logbook/logbook/api/rest/fill-luminosity-scan";
  out   = fpath+"http_code.txt";                 // output code
  comm = " cscript //nologo "+fpath+"dcs_curl_fill_luminosity_scan.vbs "+fill+" "+filen+" "+dest+" "+out;
  DebugN("curl_vbs: executing command = "+comm);
  ret = system(comm);
  // examining the return code from the output file
  delay(1); // 1 s is needed to access the return code in the output file!
  if (ret!=0) {
    DebugN("curl_vbs: Error executing system call: command = "+comm+" return_code = "+ret);
  }
  else {
    // Reading http_code
    file f;
    string txt,t3;
    f = fopen(out,"r");
    ret=ferror(f); 
    if (ret!=0) {
      DebugN("curl_vbs: Error opening file "+out+" for reading: err = "+ret);
    }
    else {
      while(feof(f)==0) {
        fscanf(f,"%s",txt);
        //DebugN("reading "+txt);
      }
      t3=substr(txt,strlen(txt)-3,3);
      //DebugN("t3 = "+t3);
      ret = t3;
      fclose(f);
    }
  }
  return ret;
}

string logbook_translate_http_code(int i)
{
  string res;
  if (i>=200 && i<300) {
    res = "Success";
    if (i==201) res=res+": logbook entry created";
  }
  else if (i>=400 && i<500) {
    res = "Failure due to client error";
    if (i==400) res=res+": Bad request (missing field, invalid field (e.g. fill not a number, file not a file))";
    if (i==403) res=res+": Forbidden (e.g. too many requests)";
    if (i==404) res=res+": Not Found (normally wrong URL)";
    if (i==422) res=res+": Unprocessable Entity (fill number not registered in logbook)";
  }
  else if (i>=500 && i<600) {
    res = "Failure due to server error";
    if (i==500) res=res+": Internal Server Error (server problem, experts (DAQ) should be contacted)";
  }
  else res = "Unknown return code: "+i;
  return res;
}
