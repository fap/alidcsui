#uses "alidcsUi/jsonBase.ctl"
/* -----------------------------------------

                     SCOPE LIB

        ver.2
        Auth. A.FRANCO   INFN Italy Bari

                     HISTORY
  13/03/2012   -  CREATION
  16/02/2021   - Sets the default Ruler Management
  22/02/2021   - Some fixing
  12/04/2021   - Parametrrize the extraction from DB or not
  14/04/2021   - Fix the derivative and Integral Math Recalculation
  06/07/2021   - Add the Get Configuration Label aliDcsUi_ScopeGetConfigurationLabel()
  13/09/2021   - Add some Helpers for the Panel - Scope integration
  13/10/2021   - Use the ALIAS to get data ...
  08/11/2021   - rearrange the colors
  03/02/2022   - add function to set Name into the config file
  04/02/2022   - FIx a bug in the Restore configuration
  03/05/2022   - Add the  aliDcsUi_isDpeArchived(string DPEname) function
  22/06/2022   - Dump in the TrendTable the strings
  20/07/2022   - Save scales definition in trd file
  25/10/2022   - Add the DumpTrends & JSON extraction method
  27/10/2022   - Make the TrendName unique whith the Module+Panel tag

  ------------------------------------------ */
const string ALIDCSUIPLOT_VERSION = "3.9a";

const int ALIDCSUIPLOT_AUTOSCALE = -1;
const int ALIDCSUIPLOT_NOSCALE = 1;
const int ALIDCSUIPLOT_LEFTSCALE = 2;
const int ALIDCSUIPLOT_RIGHTSCALE = 3;
const int ALIDCSUIPLOT_LEFTLOGSCALE = 6; // 2 +4
const int ALIDCSUIPLOT_RIGHTLOGSCALE = 7; // 3 +4
const int ALIDCSUIPLOT_LINKEDSCALE = 9;


// ------------ Defines Structures
enum RTConnection
{
Disconnected = -1,
Paused = 0,
Connected = 1
};

enum TypeOfCurve
{
Points = 0,
Steps = 1,
Linear = 2
};

enum TypeOfPoint
{
None = 0,
Rectangle = 1,
X = 2,
Plus = 3,
Star = 4,
Circle = 5,
Triangle = 6,
FillCircle = 7,
FillTriangle = 8
};

enum TypeOfScalePosition
{
Bottom = 0,
Top = 1,
Left = 2,
Right = 3,
LinkedLeft = 4,
LinkedRight = 5
};

enum TypeOfTimeStamp
{
Text = 0,
Linux = 1
};

// ----- Plot
class Plot
{
  // Members
  public int ID;
  public string TrendShapeName;
  public string Label;
  public dyn_string DPEsName;
  public string Unit;
  public string Comment;
  public string Format;

  public dyn_string ValueSources;
  public int NumOfSources;

  // Curve descriptors
  public string CurveName;
  public int CurveIndex;
  private RTConnection isRealTimeConnected;
  public string CBFunctionName;

  public bool Autoscale;
  public float Min;
  public float Max;

  public bool ScaleVisible;
  public int ScalePosition;
  public int ParentCurve;
  public bool Logaritmic;
  public float ScaleMin;
  public float ScaleMax;

  public string Color;
  public string LineType;
  public TypeOfPoint PointType;
  public TypeOfCurve CurveType;

  // Math extension
  public string OriginalFormula;
  public string MathScript;
  public bool isMath;
  public bool isDerivative;
  public bool isIntegral;
  public float PreviousValue;
  public float PreviousTime;

  // Text extension
  public bool isText;
  public dyn_string Dictionary;


  // Getter / Setter
  public TypeOfPoint SetPointType(TypeOfPoint ty) { PointType = ty; return(PointType); }
  public TypeOfCurve SetCurveType(TypeOfCurve ty) { CurveType = ty; return(CurveType); }

  public TypeOfPoint GetPointType() { return(PointType); }
  public TypeOfCurve GetCurveType() { return(CurveType); }
  public RTConnection GetRTConnection() { return(isRealTimeConnected); }
  public TypeOfScalePosition GetScalePosition() { return(ScalePosition <0 ? TypeOfScalePosition::Bottom : (TypeOfScalePosition)ScalePosition); }
  public string GetFormula() { return(OriginalFormula); }

  public Plot()
  {
    ScaleVisible = true;
    Color = "{252,20,20}";
    PointType = SetPointType(TypeOfPoint::X);
    CurveType = SetCurveType(TypeOfCurve::Steps);
    LineType = "[solid,oneColor,JoinMiter,CapButt,0]";
    isRealTimeConnected = RTConnection::Disconnected;
    dynClear(DPEsName);
    PreviousValue = 0.0;
    isDerivative = false;
    isIntegral = false;
    SetFormula("0");
    isText = false;
    dynClear(Dictionary);
  }

  public void Init(string sTrendShapeName, string sCurveName, string sLabel,
                   dyn_string dsListOfDPEs, int iPlotID, string sFormula = "p1")
  {
    NumOfSources = dynlen(dsListOfDPEs);
    isMath = (NumOfSources > 1 || sFormula != "p1") ? true : false;  // We will perform different init

    for(int i=1;i<=NumOfSources;i++)  // VERIfy the existence
      if(!dpExists(dpSubStr(dsListOfDPEs[i],DPSUB_SYS_DP_EL))) {
        DebugN("Plot::Init() Bad DP definition : ",dsListOfDPEs);
        return;
      }

    if(sLabel == "" || sTrendShapeName == "" || iPlotID < 1 || NumOfSources < 1) {
      DebugN("Plot::Init() Bad parameters : "+sTrendShapeName+" "+sCurveName+" "+sLabel+" "+sFormula+" "+iPlotID);
      return;
    }
    if( dpElementType(dsListOfDPEs[1]) == DPEL_STRING ) {
        isText = true;
    }

    ID = iPlotID;
    TrendShapeName = sTrendShapeName;
    Label = sLabel;
    DPEsName = dsListOfDPEs;
    CurveName = sCurveName;
    CBFunctionName = "DisplayANewVal";
    time ts =getCurrentTime();
    PreviousTime = (float)period(ts) + ((float)milliSecond(ts)/1000.0);
    SetFormula(sFormula);

    if(isMath) {
      Unit = "";   // keep all description params
      Comment = sFormula;
      Format = "%f7.3";
      Min = 0.0;
      Max = 0.0;
      Autoscale = true;
    } else  {
      Unit = dpGetUnit(dsListOfDPEs[1]);   // keep all description params
      Comment = dpGetDescription(dsListOfDPEs[1]);
      Format = dpGetFormat(dsListOfDPEs[1]);
      dpGet(dsListOfDPEs[1]+":_pv_range.._min",Min,dsListOfDPEs[1]+":_pv_range.._max",Max);
      Autoscale = (Min == 0.0 && Max == 0.0) ? true : false;
    }

    for(int i=1;i<=NumOfSources;i++) {
      dynAppend(ValueSources, dsListOfDPEs[i]+":_online.._value");
      dynAppend(ValueSources, dsListOfDPEs[i]+":_online.._stime");
    }
    return;
  }

  public void SetFormula(string sFormula) {
    string sAp = "";
    OriginalFormula = sFormula;
    if(strpos(sFormula,"d(") >=0) { // is derivative
      isDerivative = true;
      sAp = substr(sFormula,strpos(sFormula,"d(")+1);
    } else if(strpos(sFormula,"S(") >=0) { // is integral
      isIntegral = true;
      sAp = substr(sFormula,strpos(sFormula,"S(")+1);
    } else {
      sAp = sFormula;
    }
    MathScript = "float main(dyn_float dfV){ float result;";
    for(int i=1;i<=10;i++) {
      strreplace(sAp,"p"+i,"dfV["+i+"]");
    }
    MathScript += " result = " + sAp + "; return(result); }";
    return;
  }

  // The check was not real. Exclude it : TO DO
  private bool checkRDBConnection(string sDPEname)
  {
    return(true);
    string sSys;
    bool bIsConnected;
    sSys = dpSubStr(DPEsName[1], DPSUB_SYS);  // test the DB connection
    dpGet(sSys+"_RDBArchive.dbConnection.connected" , bIsConnected);
    if(!bIsConnected) {  // No RDB connection
      return(false);
    }
    return(true);
  }

  private int getDataSeries(string sDPEname, dyn_float &dfValues,dyn_time &dtTimes)
  {
    dynClear(dfValues);
    dynClear(dtTimes);
    time ts,te; // Get the Time definition
    getValue(TrendShapeName,"visibleTimeRange",0,ts,te);

    // If the alias exists, then use it for take data
    string sAlias = dpGetAlias(sDPEname);
    if(sAlias != ""){
      sDPEname = dpSubStr(sDPEname,DPSUB_SYS)+ "@" +sAlias;
    }
    int i,n,j;
    int count = 2;  // number of iterations

    do {
      if(isText) {
        dyn_string dsVal;
        dpGetPeriod(ts,te,0,sDPEname+":_offline.._value",dsVal, dtTimes);   // RDB connction OK make the query
        n = dynlen(dsVal);
        for(i=1; i<=n; i++) {
          j = dynContains(Dictionary, dsVal[i]);
          if(j==0) j = dynAppend(Dictionary, dsVal[i]);
          dynAppend(dfValues,j);
        }
      } else {
        dpGetPeriod(ts,te,0,sDPEname+":_offline.._value",dfValues, dtTimes);   // RDB connction OK make the query
      }
      if(dynlen(dfValues)== 0) {  // try to obtain boundary values
        ts = ts-(te-ts);
        te = te+(te-ts);
        count--;
      } else {
        count = 0;
      }
    } while (count > 0);

    n = dynlen(dfValues);
    for(i=n;i>=1;i--) {
      if( (Logaritmic && dfValues[i] <= 0.0) || (dfValues[i] > 1.0e+30) ){
          dynRemove(dfValues,i);
          dynRemove(dtTimes,i);
      }
    }
    return(dynlen(dfValues));
  }

  private int seekMinMaxTime(dyn_dyn_time dttT,dyn_int indx, time &tmax)
  {
    time t = makeTime(2100,1,1,1,1,1);
    tmax = makeTime(1970,1,1,1,1,1);
    int s = 0;
    for(int i=1;i<=dynlen(dttT);i++){
      if(dttT[i][indx[i]] < t) {
        t = dttT[i][indx[i]];
        s = i;
      }
      if(dttT[i][indx[i]] > tmax) {
        tmax = dttT[i][indx[i]];
      }
    }
    return(s);
  }

  private void calculateAtTime(dyn_float enpla,time ts,dyn_float &dfValues,dyn_time &dtTimes)
  {
    float v,calculatedValue ;
    float actTime = (float)period(ts) + ((float)milliSecond(ts)/1000.0);
    evalScript(v, MathScript, makeDynString(), enpla);
    if(isDerivative) {
      calculatedValue = (v - PreviousValue) / (actTime - PreviousTime);
    } else if(isIntegral) {
      calculatedValue = PreviousValue + v * (actTime - PreviousTime);
      v = calculatedValue;
    } else{
      calculatedValue = v;
    }
    PreviousValue = v;
    PreviousTime = actTime;
    dynAppend(dfValues,calculatedValue);
    dynAppend(dtTimes,ts);
    return;
  }

  private void skipIndex(int s,dyn_dyn_time dttT,dyn_int &indx)
  {
    if(indx[s] < dynlen(dttT[s])) indx[s]++;
    return;
  }

  private bool notEnd(dyn_dyn_time dttT,dyn_int indx)
  {
    for(int i=1;i<=dynlen(dttT);i++)
      if(indx[i] < dynlen(dttT[i])) return(true);
    return(false);
  }

  private int recalculateSeries(dyn_dyn_float dffV,dyn_dyn_time dttT,dyn_float &dfValues,dyn_time &dtTimes)
  {
    int num = dynlen(dffV);
    dyn_float enpla;
    dynClear(dfValues); dynClear(dtTimes);
    for(int i=1;i<=num;i++) dynAppend(enpla,dffV[i][1]); // the calculation buffer
    dyn_int indx;
    for(int i=1;i<=num;i++) dynAppend(indx,1); // the indexs
    int s;
    time ts;
    while(notEnd(dttT,indx)) {
      s = seekMinMaxTime(dttT,indx,ts);
      enpla[s] = dffV[s][indx[s]];
      skipIndex(s,dttT,indx);
      calculateAtTime(enpla,ts,dfValues,dtTimes);
    }
    return(dynlen(dfValues));
  }

  private void removeCurveData(bool interval = false)
  {
    time ts,te;
    getValue(TrendShapeName,"visibleTimeRange",0,ts,te);
    if(interval) {
      setValue(TrendShapeName,"curveRemoveInterval", CurveName,ts,  te);
    } else {
      te = getCurrentTime();
      setValue(TrendShapeName,"curveRemoveData", CurveName, te);
    }
    PreviousValue = 0;
    PreviousTime = ts;
    return;
  }

  public string ReloadData()
  {
    dyn_float dfValues;
    dyn_time dtTimes;
    int nOfsamples = 0;

    if(isMath) {
      for(int i=1;i<=NumOfSources;i++) {
        if(!checkRDBConnection(DPEsName[i])) return("Plot::ReloadData "+DPEsName[i]+" the system has no RDB connection !");
      }
      dyn_dyn_float dffV;
      dyn_dyn_time dttT;
      removeCurveData();
      for(int i=1;i<=NumOfSources;i++) {
        dynClear(dfValues); dynClear(dtTimes);
        nOfsamples = getDataSeries(DPEsName[i], dfValues, dtTimes);
        if(nOfsamples >0) {
          dynAppend(dffV, dfValues);
          dynAppend(dttT, dtTimes);
        }
      }
      nOfsamples = recalculateSeries(dffV,dttT,dfValues,dtTimes);
    } else {
      if(!checkRDBConnection(DPEsName[1])) return("Plot::ReloadData "+DPEsName[1]+" the system has no RDB connection !");
      nOfsamples = getDataSeries(DPEsName[1], dfValues, dtTimes);
    }
    bit32 stat = 15;
    stat = 15;
    setValue(TrendShapeName,"curveValues", CurveName,dfValues, dtTimes,stat);
    return(nOfsamples+"_");
  }

  public bool ConnectRtPlot(anytype hinstPtr, bool bPause=false)
  {
    int ret;
    switch(isRealTimeConnected){
    case RTConnection::Paused:
      isRealTimeConnected = RTConnection::Connected;
      break;
    case RTConnection::Disconnected:
      ret = dpConnectUserData(CBFunctionName,hinstPtr, true,ValueSources);//,TimeSources);
      if (ret != 0) return(false);
      isRealTimeConnected = bPause ? RTConnection::Paused :RTConnection::Connected;
      break;
    case RTConnection::Connected:
      // do nothing
      break;
    default:
      // some problems ?!
      break;
    }
    return(true);
  }

  public bool DisconnectRtPlot(anytype hinstPtr)
  {
    int ret;
    switch(isRealTimeConnected){
    case RTConnection::Paused:
    case RTConnection::Connected:
      ret = dpDisconnectUserData(CBFunctionName,hinstPtr,ValueSources);//,TimeSources);
      isRealTimeConnected = RTConnection::Disconnected;
      if(ret != 0) return(false);
    case RTConnection::Disconnected:
      // do nothing
      break;
    default:
      // some problems ?!
      break;
    }
    return(true);
  }

  public void PauseRtPlot(bool bPause=true)
  {
    if(bPause && isRealTimeConnected == RTConnection::Connected)
      isRealTimeConnected = RTConnection::Paused;
    else if(!bPause && isRealTimeConnected == RTConnection::Paused)
      isRealTimeConnected = RTConnection::Connected;
    return;
  }

  private void DisplayANewVal(anytype aHinPtr, dyn_string dp, dyn_anytype v)
  {
    if(aHinPtr.isRealTimeConnected == RTConnection::Connected) {
      int n=dynlen(v);
      if(aHinPtr.isMath) {
        float actualValue = 0.0;
        float calculatedValue = 0.0;
        time ts =getCurrentTime();
        float actualTime = (float)period(ts) + ((float)milliSecond(ts)/1000.0);
        for(int i=n;i>1;i-=2)dynRemove(v,i);
        evalScript(actualValue, aHinPtr.MathScript, makeDynString(), v); // Now we have the calculated value
        if(aHinPtr.isDerivative) {
          calculatedValue = (actualValue - aHinPtr.PreviousValue) / (actualTime - aHinPtr.PreviousTime);
        } else if(aHinPtr.isIntegral) {
          calculatedValue = aHinPtr.PreviousValue + actualValue * (actualTime - aHinPtr.PreviousTime);
          actualValue = calculatedValue;
        } else{
          calculatedValue = actualValue;
        }
        aHinPtr.PreviousValue = actualValue;
        aHinPtr.PreviousTime = actualTime;
      } else {
        if(aHinPtr.isText) {
          int j = dynContains(aHinPtr.Dictionary, v[1]);
          if(j == 0) j = dynAppend(aHinPtr.Dictionary, v[1]);
          aHinPtr.PreviousValue = j;
        } else {
          aHinPtr.PreviousValue = v[1];
        }
        aHinPtr.PreviousTime = v[2];
      }

      if((aHinPtr.Logaritmic && aHinPtr.PreviousValue <= 0.0) || (aHinPtr.PreviousValue > 1.0e+30) ) return;
      setValue(aHinPtr.TrendShapeName,"curveVal", aHinPtr.CurveName, aHinPtr.PreviousValue, aHinPtr.PreviousTime);
    }
    return;
  }

  // Graphic methods
  public bool SetAutoscale(bool bAutoScale = true)
  {
    Autoscale = bAutoScale;
    setValue(TrendShapeName, "curveAutoscale", CurveName, Autoscale);
    delay(0,100);
    setValue(TrendShapeName, "trendRefresh");
    return(bAutoScale);
  }

  public float ReadScaleMin()
  {
    float fMin;
    getValue(TrendShapeName, "curveMin", CurveName, fMin);
    return(fMin);
  }

  public void SetScaleMin(float fMin)
  {
    setValue(TrendShapeName, "curveMin", CurveName, fMin);
    ScaleMin = fMin;
    return;
  }

  public float ReadScaleMax()
  {
    float fMax;
    getValue(TrendShapeName, "curveMax", CurveName, fMax);
    return(fMax);
  }

  public void SetScaleMax(float fMax)
  {
    setValue(TrendShapeName, "curveMax", CurveName, fMax);
    ScaleMax = fMax;
    return;
  }

  public void SetVisibility(bool bVisibility = true)
  {
    setValue(TrendShapeName, "curveVisible",CurveName, bVisibility);
    return;
  }

  public bool GetVisibility()
  {
    bool bVisibility;
    getValue(TrendShapeName, "curveVisible",CurveName, bVisibility);
    return(bVisibility);
  }

  public void Scale(float fMin, float fMax)
  {
    Min = fMin;
    Max = fMax;
    setMultiValue(TrendShapeName,"curveMin",CurveName,Min,TrendShapeName,"curveMax",CurveName,Max);
    return;
  }

  private string formatTimeStamp(time TS, TypeOfTimeStamp deTimeType = TypeOfTimeStamp::Text)
  {
    if(deTimeType == TypeOfTimeStamp::Text)
      return( formatTime( "%Y.%m.%d %H:%M:%S", TS) );
    else
      return( period(TS) );
  }

  public void ExtractSerie(string sFileName, string delimiter = ",", TypeOfTimeStamp timestamptype = TypeOfTimeStamp::Text )
  {
    if(NumOfSources != 1) return; // The recalculation of math could be time consuming !

    time tStart,tEnd;
    dyn_float daValues;
    dyn_time dtTimes;
    dyn_bit64 status;

    // Get timerange
    getValue(TrendShapeName, "visibleTimeRange", 0,tStart,tEnd);
    // Gets all the values ...
    getValue(TrendShapeName, "curveValues", CurveName, daValues, dtTimes, status);

    // Registrazione sullo stream
    string sDpNa, sDes, sUni;
    file nF;
    float fAp;
    nF = fopen(sFileName,"w");
    string sAp;
    int i;
    sAp = "TIME STAMP" + delimiter;// Crea l'intestazione
    sDpNa = Label;// prima cerca per l'alias del dpe
    if(Comment != "") sDpNa = sDpNa + " - "+ Comment ;
    if(Unit != "") sDpNa = sDpNa + " ("+Unit+")";// completa con l'unit� di misura
    sAp = sAp + sDpNa+ delimiter;// e aggiunge al precedente
    fputs(sAp+'\n',nF);
    // ---------------------------------
    for(i=1;i<=dynlen(dtTimes);i++)  {// Scrive la tabella
      if(dtTimes[i] > tEnd) break;
      if(dtTimes[i] >= tStart) {
        sAp = formatTimeStamp(dtTimes[i], timestamptype);
        if(!isText)
          sAp = sAp + delimiter + dpValToString( DPEsName[1],daValues[i] ,FALSE);
        else
          sAp = sAp + delimiter + daValues[i];
        fputs(sAp+'\n',nF);
      }
    }
    fclose(nF);
    return;
  }

  public void Update()
  {
    setValue(TrendShapeName,"curveColor", CurveName, Color);
    setValue(TrendShapeName,"pointType", CurveName, (int)(GetPointType()) );
    setValue(TrendShapeName,"curveLineType", CurveName, LineType );
    setValue(TrendShapeName,"curveType", CurveName, (int)(GetCurveType()) );
    setValue(TrendShapeName,"curveLegendName", CurveName, Label);
    setValue(TrendShapeName,"curveScaleFormat", CurveName, Format);
    setValue(TrendShapeName,"curveLegendUnit", CurveName, Unit);
    setValue(TrendShapeName,"curveLogScale", CurveName, Logaritmic);
    return;
  }
};

// General functions
global dyn_string gPlotTrendColor = makeDynString("{252,20,20}","{20,20,252}","{12,140,12}",
                                   "{252,20,252}","{30,180,200}","{60,60,60}","{252,252,20}",
                                   "{159,20,20}","{20,20,159}","{20,159,20}",
                                   "{159,20,159}","{20,159,159}","{120,120,120}","{220,170,33}");

string aliDcsUi_peekTrendColor(int index)
{
  index = (index % dynlen(gPlotTrendColor));
  if(index == 0)
    index = dynlen(gPlotTrendColor);
  return( gPlotTrendColor[index] );
}

global string aliDcsUi_sLastChMask = "Last change : %y/%m/%d %H:%M:%S";
void aliDcsUi_setTS(string sSha, time tTS = 0){
  setValue(sSha,"toolTipText",formatTime(aliDcsUi_sLastChMask,tTS == 0 ? getCurrentTime() : tTS));
  return;
}

string aliDcsUi_ScopeGetConfigurationLabel(string sFileName)
{
  string buf = "";
  if( !fileToString(sFileName,buf)) {
    return("None");
  }
  int npos = strpos(buf, "Name", 0);
  if(npos < 0) {
    return("Not defined");
  }
  npos += 8;
  string title = substr(buf, npos, strpos(buf, ",", npos) - npos -1);
  return( title );
}

bool aliDcsUi_ScopeSetConfigurationLabel(string sFileName, string sConfigurationName)
{
  string buf = "";
  if( !fileToString(sFileName,buf)) {
    return(false);
  }
  int npos = strpos(buf, "Name", 0);
  if(npos < 0) {
    return(false);
  }
  npos += 8;
  buf = substr(buf, 0, npos) + sConfigurationName + substr(buf, strpos(buf, ",", npos)-1);
  file f = fopen(sFileName, "w");
  fputs(buf, f);
  fclose(f);
  return(false);
}

// Trend Class
class Trend
{
  // members
  string Name;
  string TrendName;
  bool RealTimeDisplay;
  bool GridVisible;

  time TimeStart;
  time TimeEnd;

  int plotsCounter;
  dyn_anytype Plots;

  string colBackground;
  string colForeground;
  string colGrid;
  string fonScales;

  bool scaleLeftAuto;
  bool scaleRightAuto;
  int  scaleLeftValue;
  int  scaleRightValue;

  TypeOfTimeStamp deTimeType;
  string deCSVdelimiter;

  int semap;

  public Trend(string sTrendShapeName, string Label="")
  {
    Name = Label;
    TrendName = sTrendShapeName;
    RealTimeDisplay = false;
    GridVisible = false;
    TimeEnd = getCurrentTime();
    setPeriod(TimeStart, period(TimeEnd)-3600);
    plotsCounter = 0;
    dynClear(Plots);
    colBackground = "white";
    colForeground = "black";
    colGrid = "STD_grey_light";
    fonScales = "Verdana,-1,11,5,50,0,0,0,0,0";

    deTimeType = TypeOfTimeStamp::Text;
    deCSVdelimiter = ",";

    setTheChart();
  }

  public string GetName() { return TrendName; }
  public string GetLabel() { return Name; }
  public bool isRealTimeConnect() { return RealTimeDisplay; }
  public bool isTheGridVisible() { return GridVisible; }
  public void SetLabel(string NewLabel) { Name = NewLabel; return; }
  public int GetTypeOfTimeStamp() { return (int)deTimeType; }
  public void SetTypeOfTimeStamp(TypeOfTimeStamp TypeTS = TypeOfTimeStamp::Text) { deTimeType = TypeTS; return; }
  public string GetCSVdelimiter() { return deCSVdelimiter; }
  public void SetCSVdelimeter(string Delimiter = ",") { deCSVdelimiter = Delimiter; return; }

  public void SetRealTimeConnect(bool yes, bool moveTheScale = true) synchronized(semap) {
    RealTimeDisplay = yes;
    for(int i=1;i<=plotsCounter;i++)
      Plots[i].PauseRtPlot(!yes);

    if(moveTheScale) {
      if(yes) {
        setValue(TrendName,"trendRun");
        setValue(TrendName,"gotoNow");
      } else {
        setValue(TrendName,"trendStop");
      }
    }
    return;
  }

  public string GetChartColor(bool isBack = true) {
    return(!isBack ? colForeground : colBackground);
  }

  public void SetChartColor(string foreground, string background, string grid = "") {
    colBackground = background;
    colForeground = foreground;
    colGrid = (grid == "" ? foreground : grid);
    setTheChart();
    return;
  }

  public void SetGridVisible(bool yes) synchronized(semap)
  {
    GridVisible = yes;
    setValue(TrendName,"showGrid", GridVisible);
    for(int i=1;i<=plotsCounter;i++)
      setValue(TrendName,"curveGridVisible",Plots[i].CurveName,yes);
    return;
  }

  public void SetTimeBase(time tStart, time tEnd) synchronized(semap)
  {
    setValue(TrendName,"timeBegin", tStart);
    setValue(TrendName,"timeInterval", tEnd-tStart);
    return;
  }

  public void SetTimeBase_Period(time tStart, time tInterval) synchronized(semap)
  {
    setValue(TrendName,"timeBegin", tStart);
    setValue(TrendName,"timeInterval", tInterval);
    return;
  }

  public long GetTimeBase(time &begin, time &end)
  {
    getValue(TrendName,"visibleTimeRange",0,begin,end);
    return(period(end)-period(begin)+1);
  }

  public bool GetScaleAuto(int position)
  {
    return( (position == ALIDCSUIPLOT_LEFTSCALE) ? scaleLeftAuto : scaleRightAuto);
  }

  public void SetScaleAuto(bool value, int position)
  {
    if(position == ALIDCSUIPLOT_LEFTSCALE) {
      scaleLeftAuto = value;
    } else {
      scaleRightAuto = value;
    }
    return;
  }

  public int GetScaleValue(int position)
  {
    return( (position == ALIDCSUIPLOT_LEFTSCALE) ? scaleLeftValue : scaleRightValue);
  }

  public void SetScaleValue(int value, int position)
  {
    if(position == ALIDCSUIPLOT_LEFTSCALE) {
      scaleLeftValue = value;
    } else {
      scaleRightValue = value;
    }
    return;
  }

  public string SelectTrackColor(int iTrackNum)
  {
    return(aliDcsUi_peekTrendColor(iTrackNum));
  }

  // Plot functions
  public int AddPlot(string sLabel, string sDpEName) synchronized(semap)
  {
    int PlotID = ++plotsCounter;//++plotsIDCounter;
    string sNewTrack = makeTrackName(PlotID);
    addTheCurve(sNewTrack);

    shared_ptr<Plot> newPlot = new Plot();
    newPlot.Init(TrendName, sNewTrack, sLabel, makeDynString(sDpEName), PlotID);

    setScale(ALIDCSUIPLOT_LEFTSCALE, newPlot);
    newPlot.Color = SelectTrackColor(plotsCounter);
    newPlot.ConnectRtPlot(newPlot, true);
    dynAppend(Plots, newPlot);
    setTheCurve(PlotID);
    return(PlotID);
  }

  public int AddMathPlot(string sLabel, dyn_string sDpENames, string sFormula) synchronized(semap)
  {
    int PlotID = ++plotsCounter;//plotsIDCounter;
    string sNewTrack = makeTrackName(PlotID);
    addTheCurve(sNewTrack);

    shared_ptr<Plot> newPlot = new Plot();
    newPlot.Init(TrendName, sNewTrack, sLabel, sDpENames, PlotID, sFormula);

    setScale(ALIDCSUIPLOT_LEFTSCALE, newPlot);
    newPlot.Color = SelectTrackColor(plotsCounter);
    newPlot.ConnectRtPlot(newPlot, true);

    dynAppend(Plots, newPlot);
    setTheCurve(PlotID);
    return(PlotID);
  }

  public int RestoredMathPlot(string sLabel, dyn_string sDpENames, string sTrack, string sFormula) synchronized(semap)
  {
    int PlotID = ++plotsCounter;//plotsIDCounter;
    addTheCurve(sTrack);

    shared_ptr<Plot> newPlot = new Plot();
    newPlot.Init(TrendName, sTrack, sLabel, sDpENames, PlotID, sFormula);

    setScale(ALIDCSUIPLOT_LEFTSCALE, newPlot);
    newPlot.Color = SelectTrackColor(plotsCounter);
    newPlot.ConnectRtPlot(newPlot, true);

    dynAppend(Plots, newPlot);
    setTheCurve(PlotID);
    return(PlotID);
  }

  public int seekTheCurve(int PlotID) synchronized(semap)
  {
    int Index = -1;
    for(int i=1;i<=plotsCounter;i++) {
      if( Plots[i].ID == PlotID) {
        Index = i;
        break;
      }
    }
    return(Index);
  }

  public void SetScale(int PlotID, int ScalePosition, int ParentCurveID=0) synchronized(semap)
  {
    int i = seekTheCurve(PlotID);
    if(i==-1) return;
    setScale(ScalePosition, Plots[i], ParentCurveID);
    setTheScale(PlotID);
    return;
  }

  public int GetPossibleParent(int PlotID, int ScalePosition) synchronized(semap)
  {
    int childIdx = seekTheCurve(PlotID);
    if( childIdx <= 0 || Plots[childIdx].Unit == "") return(-1);
    for (int i=1;i<=plotsCounter;i++) {
      if( Plots[i].ID != PlotID &&
          Plots[childIdx].Unit == Plots[i].Unit &&
          Plots[i].ScalePosition == ScalePosition &&
          Plots[i].ParentCurve == 0) {
        return(i);
      }
    }
    return(-1);
  }

  public dyn_int GetSameScaleTracks(int PlotID, int ScalePosition) synchronized(semap)
  {
    dyn_int diAppo;
    int childIdx = seekTheCurve(PlotID);
    if( childIdx > 0) {
      for (int i=1;i<=plotsCounter;i++) {
        if( Plots[i].ID != PlotID &&
            Plots[i].ScalePosition == ScalePosition &&
            Plots[i].ParentCurve == 0) {
          dynAppend(diAppo, Plots[i].ID);
        }
      }
    }
    return(diAppo);
  }

  public dyn_int GetLinkedTracks(int PlotID) synchronized(semap)
  {
    dyn_int diReturn;
    int ParentIdx = seekTheCurve(PlotID);
    if(ParentIdx <= 0 ) return(diReturn);
    if(Plots[ParentIdx].ParentCurve > 0)
      ParentIdx = Plots[ParentIdx].ParentCurve;

    dynAppend(diReturn, Plots[ParentIdx].ID);
    for(int i=1; i<=dynlen(Plots); i++){
      if(Plots[i].ParentCurve == ParentIdx) {
        dynAppend(diReturn, Plots[i].ID);
      }
    }
    return(diReturn);
  }

  public void RemovePlot(int PlotID, bool isDpDisconnect=true) synchronized(semap)
  {
    int i = seekTheCurve(PlotID);
    if(i>0) {
      dyn_string curves;
      getValue(TrendName,"curveNames",0,curves);
      if(dynContains(curves, Plots[i].CurveName) > 0 ) {
        if(isDpDisconnect) Plots[i].DisconnectRtPlot(Plots[i]);
        setValue(TrendName,"removeCurve",Plots[i].CurveName);
      }
      Plots[i] = nullptr;
      dynRemove(Plots,i);
      plotsCounter--;
    }
    return;
  }

  public string Refresh()
  {
    string Result = "QR=";
    int i;
    SetRealTimeConnect(false, false);
    for (i=1;i<=plotsCounter;i++) {
      Result += Plots[i].ReloadData(TrendName);
    }
    SetRealTimeConnect(true, false);
    return(Result);
  }

  private string makeTrackName(int iTrackNum)
  {
    return( "Trk"+(iTrackNum) );
  }

  private setScale(int iPosition, anytype &Plo, int ParentCurve=1) synchronized(semap)
  {
    int i;
    int n = dynlen(Plots);

    Plo.Logaritmic = false;
    Plo.ParentCurve = -1* Plo.ParentCurve;
    Plo.ScaleVisible = true;

    switch(iPosition) {
    case ALIDCSUIPLOT_AUTOSCALE:       // Now search for
      for(i=1;i<=n;i++){
        if(Plots[i].Unit == Plo.Unit) {
          Plo.ScaleVisible = false;
          Plo.ParentCurve = i;
          Plo.ScalePosition = ALIDCSUIPLOT_LINKEDSCALE;
          break;
        } else {
          if(Plots[i].ScaleVisible) {
            Plo.ScalePosition = ( Plo.ScalePosition == ALIDCSUIPLOT_LEFTSCALE) ? ALIDCSUIPLOT_RIGHTSCALE : ALIDCSUIPLOT_LEFTSCALE  ;
          }
        }
      }
      break;
    case ALIDCSUIPLOT_LEFTSCALE:
    case ALIDCSUIPLOT_RIGHTSCALE:
      Plo.ScalePosition = iPosition;
      break;
    case ALIDCSUIPLOT_LEFTLOGSCALE:
    case ALIDCSUIPLOT_RIGHTLOGSCALE:
      Plo.Logaritmic = true;
      Plo.ScalePosition = iPosition - 4;
      break;
    case ALIDCSUIPLOT_LINKEDSCALE:
      if(Plo.ParentCurve==0) {
        Plo.ScaleVisible = false;
        Plo.ParentCurve = ParentCurve;
        Plo.ScalePosition = Plots[Plo.ParentCurve].ScalePosition;
        Plo.Logaritmic = Plots[Plo.ParentCurve].Logaritmic;
      }
      break;
    case ALIDCSUIPLOT_NOSCALE:
      Plo.ScaleVisible = false;
      Plo.ScalePosition = iPosition;
      break;
    default:
      Plo.ScaleVisible = true;
      Plo.ScalePosition = ALIDCSUIPLOT_LEFTSCALE;
      break;
    }
  }

  public shared_ptr<Plot> GetTrackPtr(int PlotID) synchronized(semap)
  {
    int i = seekTheCurve(PlotID);
    if(i < 1) return(nullptr);
    return(Plots[i]);
  }

  public int GetTrackIndex(string sDpEName) synchronized(semap)
  {
    for(int i=1;i<=plotsCounter;i++){
      if(Plots[i].DPEname == sDpEName) {
        return(i);
      }
    }
    return(-1);
  }

  public string GetTrackName(string sDpEName) synchronized(semap)
  {
    int idx =  GetTrackIndex(sDpEName);
    if(idx > 0) {
        return(Plots[idx].CurveName);
    }
    return("");
  }

  public string GetTrackLabelByID(int PlotID) synchronized(semap)
  {
    for(int i=1;i<=plotsCounter;i++){
      if(Plots[i].ID == PlotID) {
        return(Plots[i].Label);
      }
    }
    return(-1);
  }

  public string GetTrackColor(int PlotID) synchronized(semap)
  {
    for(int i=1;i<=plotsCounter;i++){
      if(Plots[i].ID == PlotID) {
        return(Plots[i].Color);
      }
    }
    return("WF_Text");
  }

  public int GetTracksCount()
  {
    return(plotsCounter);
  }

  public int GetTracksIDbyIndex(int idx) synchronized(semap)
  {
    checkConsistency();
    if(idx >0 && idx <= plotsCounter) {
      return(Plots[idx].ID);
    } else {
      return(-1);
    }
  }

  // graph trend
  private addTheCurve(string sCurveName)
  {
    int ar = 0;//index starts from 0
    dyn_string a_names;
    getValue(TrendName,"curveNames",ar,a_names);
    int nc = dynlen(a_names);
    int i,j;
    for(i=1;i<=nc;i++) {
      for(j=1;j<plotsCounter;j++) {
        if(Plots[j].CurveName == a_names[i]) j = 9999;
      }
      if(j<9999) { // this is a free slot
        setValue(TrendName,"curveName",a_names[i], sCurveName );
        return;
      }
    }
    setValue(TrendName,"addCurve",ar, sCurveName ); // Add the curve if it not exsits
    return;
  }

  public setTheCurve(int PlotID)
  {
    int Track = seekTheCurve(PlotID);
    if(Track<1) return;
    Plots[Track].Update();
    setTheScale(PlotID);

    setValue(TrendName,"curveGridVisible", Plots[Track].CurveName, GridVisible);
    setMultiValue(TrendName,"scaleFont",fonScales,
                  TrendName,"legendFont",fonScales);
    return;
  }

  private setTheScale(int PlotID) synchronized(semap)
  {
    int Track = seekTheCurve(PlotID);
    if(Track<1) return;
    if(!isTheCurve(Plots[Track].CurveName)) return;

    setValue(TrendName,"curveScalePosition", Plots[Track].CurveName, (int)(Plots[Track].GetScalePosition()) );
    setValue(TrendName,"curveScaleVisibility", Plots[Track].CurveName, Plots[Track].ScaleVisible);
    setValue(TrendName,"curveLogScale", Plots[Track].CurveName, Plots[Track].Logaritmic);
    setValue(TrendName,"curveAutoscale", Plots[Track].CurveName, Plots[Track].Autoscale);
    setMultiValue(TrendName,"curveMin",Plots[Track].CurveName, Plots[Track].ScaleMin,
                  TrendName,"curveMax",Plots[Track].CurveName, Plots[Track].ScaleMax);
    if(Plots[Track].ParentCurve > 0)  {
      setValue(TrendName,"linkCurves", Plots[Track].CurveName, Plots[Plots[Track].ParentCurve].CurveName);
    } else if(Plots[Track].ParentCurve < 0) {
        setValue(TrendName,"unlinkCurves", Plots[Track].CurveName, Plots[-Plots[Track].ParentCurve].CurveName);
        Plots[Track].ParentCurve = 0;
    }
  }

  private setTheChart()
  {
    setMultiValue(TrendName,"backCol",(colBackground==""?"White":colBackground)); //  this doesn't works !,TrendName,"foreCol",colForeground);

    setMultiValue(TrendName,"scaleFont",fonScales,
                  TrendName,"legendFont",fonScales);
    setValue(TrendName,"gridColor",(colGrid==""?"Grey":colGrid));
    setMultiValue(TrendName,"rulerPanel","alidcsUi/Scope/trendRulerPanel.pnl",
                  TrendName,"maxRulerCount",0,5);
    return;
  }

  private bool isTheCurve(string curveName)
  {
    dyn_string dsCurves;
    getValue(TrendName, "curveNames", 0, dsCurves);
    return( dynContains(dsCurves,curveName) > 0 ? true : false);
  }

  public void Rescale() synchronized(semap)
  {
    float Min, Max;
    int i,j;
    for(i=1;i<=plotsCounter;i++){
      if(Plots[i].Autoscale == false){
        j = Plots[i].ParentCurve;
        if(j>0){
          getMultiValue(TrendName,"curveMin",Plots[j].CurveName,Min,TrendName,"curveMax",Plots[j].CurveName,Max);
          setMultiValue(TrendName,"curveMin",Plots[i].CurveName,Min,TrendName,"curveMax",Plots[i].CurveName,Max);
        }
      }
    }
    return;
  }

  public string ExtractTrendsTable(string sFileName, bool noDdAccess = true)
  {
    time tStart,tEnd;
    dyn_dyn_string daValues;
    dyn_time dtTimes;
    dyn_string dsIntesta;
    dyn_dyn_string daValuesFin;
    dyn_time dtTimesFin;
    int count, iNumDp;
    dyn_string  dfAp;
    string sAp;
    dyn_bit64 status;
    string result;
    string delimiter = deCSVdelimiter;

    // Get timerange
    getValue(TrendName, "visibleTimeRange", 0,tStart,tEnd);
    dynClear(dtTimesFin);
    dynClear(daValuesFin);

    result = "Extract ["+formatDumpOut(tStart,DPEL_TIME)+","+formatDumpOut(tEnd,DPEL_TIME)+"] Traks:"+dynlen(Plots)+" =";
    int i,j,k;
    iNumDp = 0;
    for(i= 1;i<=dynlen(Plots);i++) {
      if(!Plots[i].GetVisibility()) {
        continue;
      }
      iNumDp++;
      dynAppend(dsIntesta,Plots[i].Label);
      dynClear(dfAp);
      for(j=1;j<=iNumDp;j++) dynAppend(dfAp,"");// skip the column
      result += queryTheSerie(i,tStart,tEnd,daValues,dtTimes, noDdAccess) +",";

      k = 1;
      count=dynlen(daValues);
      for(j=1;j<=count;j++) {
        if(dtTimes[j] < tStart || dtTimes[j] > tEnd) continue;
        while( k <= dynlen(dtTimesFin) && dtTimesFin[k] < dtTimes[j]) k++;
        if(k>dynlen(dtTimesFin)) {
          dynAppend(dtTimesFin,dtTimes[j]);
          dfAp[iNumDp] = daValues[j];
          dynAppend(daValuesFin,dfAp);
          k++;
        } else {
          if(dtTimesFin[k] == dtTimes[j]) {
            daValuesFin[k][iNumDp] = daValues[j];
          } else {
            dynInsertAt(dtTimesFin,dtTimes[j],k);
            dfAp[iNumDp] = daValues[j];
            dynInsertAt(daValuesFin,"",k);
            daValuesFin[k] = dfAp;
          }
        }
      }
    }
    // Registrazione sullo stream
    string sDpNa, sDes, sUni;
    file nF;
    float fAp;
    nF = fopen(sFileName,"w");
    int err = ferror(nF);
    if(err) {
      return("Error to open the output file !");
    }
    // Crea l'intestazione
    sAp = "TIME STAMP" + delimiter;
    for(i=1;i<=dynlen(Plots);i++) {
      if(!Plots[i].GetVisibility()) {
        continue;
      }
      // prima cerca per l'alias del dpe
      sDpNa = Plots[i].Label;
      sUni = Plots[i].Unit;
      if(Plots[i].Comment != "") sDpNa = sDpNa + " - "+ Plots[i].Comment ;
      // completa con l'unit� di misura
      if(sUni != "") sDpNa = sDpNa + " ("+sUni+")";
      // e aggiunge al precedente
      sAp = sAp + sDpNa+ delimiter;
    }
    fputs(sAp+'\n',nF);

    // Scrive la tabella
    for(i=1;i<=dynlen(dtTimesFin);i++)  {
      sAp = formatTimeStamp(dtTimesFin[i]);
      for(j=1;j<=dynlen(daValuesFin[i]);j++){
        if(daValuesFin[i][j] == ""){
          sAp = sAp + delimiter;
        } else {
          if( Plots[j].isText ) {
            sAp = sAp + delimiter + daValuesFin[i][j];
          } else {
            fAp = daValuesFin[i][j];
            sAp = sAp + delimiter + dpValToString( Plots[j].DPEsName[1],fAp ,FALSE);
          }
        }
      }
      fputs(sAp+'\n',nF);
    }
    // Fine
    fclose(nF);
    return(result);
  }

  private void resizeTheSerie(time tStart, time tEnd, dyn_time &dtTimes, dyn_dyn_anytype &daValues)
  {
    int n = dynlen(dtTimes);
    dyn_time dtTS;
    dyn_dyn_anytype daVal;

    for(int i=1;i<=n;i++) {
      if(dtTimes[i]>=tStart && dtTimes[i]<=tEnd ){
        dynAppend(dtTS, dtTimes[i]);
        dynAppend(daVal,daValues[i]);
      }
    }
    dtTimes = dtTS;
    daValues = daVal;
    return;
  }

  private string queryTheSerie(int plotIndex,
                               time tStart, time tEnd,
                               dyn_dyn_anytype &daValues, dyn_time &dtTimes,
                               bool noDdAccess=true)
  {
    int iType = dpElementType(Plots[plotIndex].DPEsName[1]);
    bool bIsArchived;
    dyn_bit64 status;
    dpGet(Plots[plotIndex].DPEsName[1]+":_archive.._archive", bIsArchived);
    if(bIsArchived && (!noDdAccess || Plots[plotIndex].isText)) {
      int count = dpGetPeriod(tStart,tEnd,0,Plots[plotIndex].DPEsName[1]+":_offline.._value",daValues,dtTimes);
    } else {
      getValue(TrendName, "curveValues", Plots[plotIndex].CurveName, daValues,dtTimes, status);
      resizeTheSerie(tStart, tEnd, dtTimes, daValues);
    }
    return(dynlen(daValues));
  }

  public string ExtractJsonTrends(string sFileName, bool noDdAccess = true)
  {
    dyn_dyn_anytype theFile,theTrend, thePlot, theValue;
    file f;
    f = fopen(sFileName, "w");
    if(f == 0){
      return("ERROR Opening Output file :"+sFileName);
    }
    int root = jsonCreateJSON(theFile);
    jsonAppendItemString(theFile,root,"Name", "ALICE DCS UI : JSON trends data extraction");
    jsonAppendItemString(theFile,root,"Version", "Ver:1.0  AF - INFN Bari - Date: 27/10/2022");

    // Get timerange
    time tStart, tEnd;
    getValue(TrendName, "visibleTimeRange", 0,tStart,tEnd);
    dyn_dyn_string daValues;
    dyn_time dtTimes;
    dyn_bit64 status;
    dynClear(dtTimes);
    dynClear(daValues);

    string result = "Extract ["+formatDumpOut(tStart,DPEL_TIME)+","+formatDumpOut(tEnd,DPEL_TIME)+"] Traks:"+dynlen(Plots)+" =";

    int aTrend = jsonCreateJSON(theTrend);
    jsonAppendItemString(theTrend,aTrend,"Time start",formatDumpOut(tStart,DPEL_TIME));
    jsonAppendItemString(theTrend,aTrend,"Time end",formatDumpOut(tEnd,DPEL_TIME));
    jsonAppendItemInteger(theTrend,aTrend,"Number of tracks",dynlen(Plots));

    int plots = jsonAppendItemArray(theTrend,aTrend,"Plots");
    for (int i=1;i<=dynlen(Plots);i++) {
      int aPlot = jsonCreateJSON(thePlot);
      jsonAppendItemString(thePlot,aPlot,"Label", Plots[i].Label);
      jsonAppendItemString(thePlot,aPlot,"DP name",Plots[i].DPEsName[1]);
      jsonAppendItemString(thePlot,aPlot,"Unit",Plots[i].Unit);
      jsonAppendItemString(thePlot,aPlot,"Comment",Plots[i].Comment);

      int iType = dpElementType(Plots[i].DPEsName[1]);
      result += queryTheSerie(i,tStart,tEnd,daValues,dtTimes, noDdAccess) +",";

      jsonAppendItemInteger(thePlot,aPlot,"Number of samples",dynlen(daValues));

      int values = jsonAppendItemArray(thePlot,aPlot,"Values");
      int num = dynlen(daValues);
      for(int j = 1; j <= num; j++) {
        int aValue = jsonCreateJSON(theValue);
        string sTS = formatTimeStamp(dtTimes[j]);
        switch(iType) {
          case DPEL_INT:
            jsonAppendItemInteger(theValue,aValue,sTS,(int)daValues[j][1]);
            break;
          case DPEL_FLOAT:
            jsonAppendItemFloat(theValue,aValue,sTS,(float)daValues[j][1]);
            break;
          case DPEL_BOOL:
            jsonAppendItemBool(theValue,aValue,sTS,(bool)daValues[j][1]);
            break;
          case DPEL_STRING:
            jsonAppendItemString(theValue,aValue,sTS,(string)daValues[j][1]);
            break;
          case DPEL_TIME:
            jsonAppendItemString(theValue,aValue,sTS,formatTimeStamp((time)daValues[j][1]));
            break;
          default:
            jsonAppendItemString(theValue,aValue,sTS,dynStringToString(daValues[j]));
            break;
        }
        jsonAppendObjectToArray(thePlot,values,j,theValue);
      }
      jsonAppendObjectToArray(theTrend,plots,Plots[i].ID,thePlot);
    }

    jsonAppendItemObject(theFile,root,"Trend",theTrend);
    fputs(jsonStringify(theFile,true),f);
    fclose(f);
    return(result);
  }

  public string DumpTrends(string sFileName, bool noDdAccess = true)
  {
    time t = getCurrentTime();
    // open the file
    file f;
    f = fopen(sFileName, "w");
    if(f == 0){
      return("ERROR Opening Output file :"+sFileName);
    }
    fputs( "# -----------  Dump Scope Trends  -----------\n",f);
    fputs( "#    Tool Author: Antonio FRANCO  INFN-BARI |\n",f);
    fputs( "#    Ver:1.0  AFx  Date: 11/10/2010         |\n",f);
    fputs( "# -------------------------------------------\n",f);

    // Get timerange
    time tStart, tEnd;
    getValue(TrendName, "visibleTimeRange", 0,tStart,tEnd);
    dyn_dyn_string daValues;
    dyn_time dtTimes;
    dyn_bit64 status;
    dynClear(dtTimes);
    dynClear(daValues);

    string result = "Extract ["+formatDumpOut(tStart,DPEL_TIME)+","+formatDumpOut(tEnd,DPEL_TIME)+"] Traks:"+dynlen(Plots)+" =";
    string buf;

    fputs( "% Extracted interval ["+formatDumpOut(tStart,DPEL_TIME)+","+formatDumpOut(tEnd,DPEL_TIME)+"]\n",f);
    fputs( "% Number of tracks "+dynlen(Plots)+"\n",f);
    fputs( "# -------------------------------------------\n",f);
    for(int i = 1; i <= dynlen(Plots); i++) {
      if(!Plots[i].GetVisibility()) {
        continue;
      }
      fputs( "% Trend name = "+Plots[i].Label+"\n",f);
      fputs( "% DP name = "+Plots[i].DPEsName[1]+"\n",f);
      fputs( "% Unit = "+Plots[i].Unit+"\n",f);
      fputs( "% Comment = "+Plots[i].Comment+"\n",f);
      int iType = dpElementType(Plots[i].DPEsName[1]);
      result += queryTheSerie(i,tStart,tEnd,daValues,dtTimes, noDdAccess) +",";
      fputs( "% Number of samples = "+dynlen(daValues)+"\n",f);
      fputs( "% BEGIN: \n",f);
      for(int j = 1; j <= dynlen(daValues); j++) {
        buf = formatTimeStamp(dtTimes[j]) + "\t" + formatDumpOut(daValues[j],iType) ;
        fputs( buf+"\n",f);
      }
      fputs("% END:\n",f);
    }
    fputs( "# ------------- eof -------------\n",f);
    fclose(f);
    return(result);
  }

  private string formatTimeStamp(time TS)
  {
    if(deTimeType == TypeOfTimeStamp::Text)
      return( formatTime( "%Y.%m.%d %H:%M:%S", TS) );
    else
      return( (string)period(TS) );
  }

  private string formatDumpOut(dyn_anytype aValue, int iType)
  {
    string sBuf;
    int i;
    float f;
    bool b;
    time t;
    switch(iType){
      case DPEL_INT:
        sBuf = ((int)(aValue[1]));
        break;
      case DPEL_FLOAT:
        sBuf = ((float)(aValue[1]));
        break;
      case DPEL_BOOL:
        sBuf = ((bool)(aValue[1]));
        break;
      case DPEL_STRING:
        sBuf = ((string)(aValue[1]));
        break;
      case DPEL_TIME:
        sBuf = formatTimeStamp(aValue[1]);
        break;
      default:
        sBuf = dynStringToString(aValue, ",");
        break;
    }
    return(sBuf);
  }

  public SaveConfiguration(string sFileName)
  {
    dyn_dyn_anytype theConfig, theTrend, thePlot;
    file nF;
    int iAp;
    float fAp;
    nF = fopen(sFileName,"w");
    int root = jsonCreateJSON(theConfig);
    jsonCreateJSON(theTrend);
    int trend = jsonAppendItemObject(theConfig,root,"Trend",theTrend);
    jsonAppendItemString(theConfig,trend,"Name", Name);
    jsonAppendItemString(theConfig,trend,"ShapeName", TrendName);
    jsonAppendItemBool(theConfig,trend,"GridVisibe", GridVisible);
    jsonAppendItemString(theConfig,trend,"ForegrColor", colForeground);
    jsonAppendItemString(theConfig,trend,"BackgrColor", colBackground);

    GetTimeBase(TimeStart, TimeEnd);
    jsonAppendItemString(theConfig,trend,"TimeBegin", formatTime("%Y.%m.%d %H:%M:%S",TimeStart));
    jsonAppendItemString(theConfig,trend,"TimeEnd", formatTime("%Y.%m.%d %H:%M:%S",TimeEnd));

    jsonAppendItemBool(theConfig,trend,"ScaleLeftAuto", scaleLeftAuto);
    jsonAppendItemBool(theConfig,trend,"ScaleRightAuto", scaleRightAuto);
    jsonAppendItemInteger(theConfig,trend,"ScaleLeftValue", scaleLeftValue);
    jsonAppendItemInteger(theConfig,trend,"ScaleRightValue", scaleRightValue);

    int plots = jsonAppendItemArray(theConfig,trend,"Plots");
    for (int i=1;i<=dynlen(Plots);i++) {
      int aPlot = jsonCreateJSON(thePlot);
      iAp= Plots[i].ID; jsonAppendItemInteger(thePlot,aPlot,"ID",iAp);
      jsonAppendItemString(thePlot,aPlot,"Label", Plots[i].Label);
      jsonAppendItemString(thePlot,aPlot,"DPEsName", dynStringToString(Plots[i].DPEsName));
      jsonAppendItemString(thePlot,aPlot,"Unit", Plots[i].Unit);
      jsonAppendItemString(thePlot,aPlot,"Comment", Plots[i].Comment);
      jsonAppendItemString(thePlot,aPlot,"Format", Plots[i].Format);
      jsonAppendItemString(thePlot,aPlot,"ValueSources", dynStringToString(Plots[i].ValueSources));
      iAp=Plots[i].NumOfSources; jsonAppendItemInteger(thePlot,aPlot,"NumOfSources", iAp);
      jsonAppendItemString(thePlot,aPlot,"CurveName", Plots[i].CurveName);
      iAp=Plots[i].CurveIndex; jsonAppendItemInteger(thePlot,aPlot,"CurveIndex", iAp);
      jsonAppendItemString(thePlot,aPlot,"CBFunctionName", Plots[i].CBFunctionName);
      iAp= Plots[i].ScalePosition; jsonAppendItemInteger(thePlot,aPlot,"ScalePosition",iAp);
      iAp= Plots[i].ParentCurve; jsonAppendItemInteger(thePlot,aPlot,"ParentCurve", iAp);
      iAp=(int)Plots[i].GetPointType(); jsonAppendItemInteger(thePlot,aPlot,"PointType", iAp);
      jsonAppendItemString(thePlot,aPlot,"LineType", Plots[i].LineType);
      iAp=(int)Plots[i].GetCurveType(); jsonAppendItemInteger(thePlot,aPlot,"CurveType", iAp);
      fAp= Plots[i].Min; jsonAppendItemFloat(thePlot,aPlot,"Min", fAp);
      fAp= Plots[i].Max; jsonAppendItemFloat(thePlot,aPlot,"Max", fAp);
      jsonAppendItemFloat(thePlot,aPlot,"ScaleMin", Plots[i].ReadScaleMin());
      jsonAppendItemFloat(thePlot,aPlot,"ScaleMax", Plots[i].ReadScaleMax());
      jsonAppendItemBool(thePlot,aPlot,"Autoscale", Plots[i].Autoscale);
      jsonAppendItemBool(thePlot,aPlot,"ScaleVisible", Plots[i].ScaleVisible);
      jsonAppendItemBool(thePlot,aPlot,"Logaritmic", Plots[i].Logaritmic);
      jsonAppendItemString(thePlot,aPlot,"Color", Plots[i].Color);
      jsonAppendItemString(thePlot,aPlot,"OriginalFormula", Plots[i].OriginalFormula);
      jsonAppendItemString(thePlot,aPlot,"MathScript", Plots[i].MathScript);
      jsonAppendItemBool(thePlot,aPlot,"isMath", Plots[i].isMath);
      jsonAppendItemBool(thePlot,aPlot,"isDerivative", Plots[i].isDerivative);
      jsonAppendItemBool(thePlot,aPlot,"isIntegral", Plots[i].isIntegral);

      jsonAppendObjectToArray(theConfig,plots,Plots[i].ID,thePlot);
    }
    fputs(jsonStringify(theConfig,true),nF);
    fclose(nF);
  }

  public RestoreConfiguration(string sFileName, string sTrendShape = "")
  {
    dyn_dyn_anytype theConfig, thePlot;
    string buf = "";
    string ap1,ap3, ap5;
    bool ap4;
    dyn_string ap2;
    anytype value;

    fileToString(sFileName,buf);
    jsonParse(buf, theConfig);
    buf = "";
    //jsonDumpObject(theConfig);
    jsonGetItemValue(theConfig, "Trend.Name",value);  Name = value;
    if(sTrendShape != "") {
      TrendName = checkTheTrendName(sTrendShape);
    } else {
      jsonGetItemValue(theConfig, "Trend.ShapeName",value);  TrendName = value;
    }
    jsonGetItemValue(theConfig, "Trend.GridVisibe",value);  ap4 = value;
    SetGridVisible(ap4);
    jsonGetItemValue(theConfig, "Trend.ForegrColor",value);  ap1 = value;
    jsonGetItemValue(theConfig, "Trend.BackgrColor",value);  ap3 = value;
    SetChartColor(ap1,ap3);

    jsonGetItemValue(theConfig, "Trend.TimeBegin",value);  TimeStart = value;
    jsonGetItemValue(theConfig, "Trend.TimeEnd",value);  TimeEnd = value;
    SetTimeBase(TimeStart, TimeEnd);

    jsonGetItemValue(theConfig, "Trend.ScaleLeftAuto",value);  scaleLeftAuto = value;
    jsonGetItemValue(theConfig, "Trend.ScaleRightAuto",value);  scaleRightAuto = value;
    jsonGetItemValue(theConfig, "Trend.ScaleLeftValue",value);  scaleLeftValue = value;
    jsonGetItemValue(theConfig, "Trend.ScaleRightValue",value);  scaleRightValue = value;

    int numOfPlots = jsonGetItemSize(theConfig, "Trend.Plots");
    dynClear(Plots);
    for(int i=1;i<=numOfPlots;i++) {
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].Label",value); ap1= value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].DPEsName",value); ap2= stringToDynString(value);
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].OriginalFormula",value); ap3=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].CurveName",value); ap5=value;
      RestoredMathPlot(ap1, ap2, ap5, ap3);
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].ID",value); Plots[i].ID = value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].Unit",value);Plots[i].Unit=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].Comment",value);Plots[i].Comment=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].Format",value);Plots[i].Format=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].ValueSources",value);Plots[i].ValueSources=stringToDynString(value);
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].NumOfSources",value);Plots[i].NumOfSources=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].CurveIndex",value);Plots[i].CurveIndex=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].CBFunctionName",value);Plots[i].CBFunctionName=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].ScalePosition",value);Plots[i].ScalePosition=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].ParentCurve",value);Plots[i].ParentCurve=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].PointType",value);
      Plots[i].SetPointType( (TypeOfPoint)((int)value < 0 ? 0 : (int)value) );
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].LineType",value);Plots[i].LineType=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].CurveType",value);
      Plots[i].SetCurveType( (TypeOfCurve)((int)value < 0 ? 0 : (int)value) );
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].Min",value);Plots[i].Min=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].Max",value);Plots[i].Max=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].Autoscale",value);Plots[i].Autoscale=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].ScaleMin",value);Plots[i].ScaleMin = value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].ScaleMax",value);Plots[i].ScaleMax = value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].ScaleVisible",value);Plots[i].ScaleVisible=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].Logaritmic",value);Plots[i].Logaritmic=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].Color",value);Plots[i].Color=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].OriginalFormula",value);Plots[i].OriginalFormula=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].MathScript",value);Plots[i].MathScript=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].isMath",value);Plots[i].isMath=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].isDerivative",value);Plots[i].isDerivative=value;
      jsonGetItemValue(theConfig,"Trend.Plots["+i+"].isIntegral",value);Plots[i].isIntegral=value;
      //setTheCurve(Plots[i].ID);
    }
   return;
  }

  public void ClearConfiguration()
  {
    int plotID;
    checkConsistency();
    int nPlot = GetTracksCount();
    for (int i=nPlot; i>0; i--) {
      plotID = GetTracksIDbyIndex(i);
      RemovePlot(plotID);
    }
    return;
  }

  private void checkConsistency() {
    if(plotsCounter != dynlen(Plots) ) {
      DebugTN("Trend::checkConsistency() : Plots Counter Mismatch ! Realigned");
      plotsCounter = dynlen(Plots);
    }
    return;
  }

  private string checkTheTrendName(string trendName)
  {
    if(strpos(trendName, ".") < 0 && strpos(trendName, ":") < 0) {
      trendName = myModuleName() + "." + myPanelName() + ":" + trendName;
    }
    return(trendName);
  }
};

  public dyn_string tbDurationDes = makeDynString("One Minute","10 Minutes","One Hour","Six Hours","12 Hours",
                                   "One Day","2 Days","One Week","2 Weeks","One Month",
                                   "3 Months","1 Year");
  public dyn_long tbDurationSec = makeDynLong(60,600,3600,6*3600,12*3600,
                                   24*3600,48*3600,7*24*3600,14*24*3600,31*24*3600,
                                   31*3*24*3600,365*24*3600);

// ---- Scope Static Class ()
class Scope
{
  // members
  public static int TrendsCount;
  private static dyn_anytype Trends;
  private static string PickTrendDestination;
  private static dyn_string PickedDps;

  public static Scope()
  {
    //the Constructor
    dynClear(Trends);
    TrendsCount = 0;

  }

  public static void Init()
  {
    return;
  }
  private static void __openTheScope()
  {
    if( !isModuleOpen("dcsUiScopeModule") || !isPanelOpen("dcsUiScope","dcsUiScopeModule" )) {
      ModuleOnWithPanel("dcsUiScopeModule", 75, 100, 0, 0, 1, 1,
                        "None","alidcsUi/Scope/OscilloScope.pnl", "dcsUiScope",
                        makeDynString() );
      stayOnTop(true,"dcsUiScopeModule");
      delay(0,500); // waits in order to end the Init phase;
    }
  }
  public static void Open()
  {
    __openTheScope();
    return;
  }

  public static void AddATrack(string theDP, string aLabel = "")
  {
    __openTheScope();
    shape sh = getShape("dcsUiScopeModule.dcsUiScope:");
    sh.setAddplot(theDP+","+aLabel);
    if(sh.getAddplot() == -1)
      DebugTN("alidcsUI Oscilloscope: ERROR to add the track !");
    return;
  }

  public static void SetPick(string TrendName)
  {
    TrendName = CheckTheTrendName(TrendName);
    dynClear(PickedDps);
    PickTrendDestination = TrendName;
    return;
  }
  public static string GetPick()
  {
    return(PickTrendDestination);
  }
  public static void AddPickedDP(string theDP, string aLabel = "")
  {
    dynAppend(PickedDps, theDP+","+aLabel);
    return;
  }
  public static dyn_string GetPickedDPs()
  {
    return(PickedDps);
  }

  public static string CheckTheTrendName(string TrendName)
  {
    if(strpos(TrendName, ".") < 0 && strpos(TrendName, ":") < 0) {
      TrendName = myModuleName() + "." + myPanelName() + ":" + TrendName;
    }
    return(TrendName);
  }

  public static int Add(string TrendName, string Label="")
  {
    TrendName = CheckTheTrendName(TrendName);
    int ptr = seek(TrendName);
    if(ptr > 0) {
      Remove(TrendName);
    }
    shared_ptr<Trend> newTrend = new Trend(TrendName, Label);
    dynAppend(Trends, newTrend);
    TrendsCount++;
    return(TrendsCount);
  }

  public static bool Remove(string TrendName, bool isDpDisconnect=false)
  {
    TrendName = CheckTheTrendName(TrendName);
    shared_ptr<Trend> ptr = GetTrend(TrendName);
    if(ptr == nullptr) return(false);

    for(int i=ptr.GetTracksCount();i>0;i--) {
      ptr.RemovePlot(ptr.GetTracksIDbyIndex(i), isDpDisconnect);
    }
    int ind =  seek(TrendName);
    Trends[ind] = nullptr;
    dynRemove(Trends,ind);
    TrendsCount--;
    return(true);
  }
  private static int seek(string TrendName)
  {
    for(int i=1;i<=TrendsCount;i++) {
      if(Trends[i].GetName() == TrendName) return(i);
    }
    return(-1);
  }
  public static shared_ptr<Trend> GetTrend(string TrendName)
  {
    TrendName = CheckTheTrendName(TrendName);
    int ptr = seek(TrendName);
    if(ptr < 0) return(nullptr);
    return(Trends[ptr]);
  }

  public static shared_ptr<Plot> GetTrack(string TrendName, int PlotID)
  {
    TrendName = CheckTheTrendName(TrendName);
    int ptr = seek(TrendName);
    if(ptr < 0) return(nullptr);
    shared_ptr<Plot> plt = nullptr;
    plt = Trends[ptr].GetTrackPtr(PlotID);
    return(plt);
  }

  // Some utilities functions
  public static string tbGetDurationItems()
  {
    return(dynlen(tbDurationDes));
  }
  public static string tbGetDurationDesc(int idx)
  {
    if(idx > 0 && idx <= dynlen(  tbDurationDes) ) {
      return(tbDurationDes[idx]);
    }
    return(tbDurationDes[1]);
  }
  public static long tbGetDurationSecs(int idx)
  {
    if(idx > 0 && idx <= dynlen(  tbDurationSec) ) {
      return(tbDurationSec[idx]);
    }
    return(tbDurationSec[1]);
  }
  public static long tbGetIndexSecs(long secs)
  {
    int idx = dynlen(tbDurationSec);
    while(idx > 0) {
      if(tbDurationSec[idx] <= secs) {
        return(idx);
      }
      idx--;
    }
    return(1);
  }
  public static time tbGetEndTime(time Start, long duration)
  {
    time appo;
    setPeriod(appo, period(Start) + duration);
    return(appo);
  }
  public static time tbGetStartTime(time End, long duration)
  {
    time appo;
    setPeriod(appo, period(End) - duration);
    return(appo);
  }
  public static long tbGetDuration(time Start, time End)
  {
    return( period(End) - period(Start));
  }
};


int aliDcsUi_addPlot(string sTrend, string sLabel, string sDpEName,
                     int iPosition = -1, int iParent = 0)
{
  // VERIfy the existence
  if(!dpExists(dpSubStr(sDpEName,DPSUB_SYS_DP))) {
    DebugTN("dcsUiScope::aliDcsUi_addPlot() : Error Dp="+sDpEName+" doesn't exists !");
    return(0);
  }
  int plotID = -1;
  shared_ptr<Trend> ptr = nullptr;
  ptr = Scope::GetTrend(sTrend);
  plotID = ptr.AddPlot(sLabel, sDpEName);
  ptr.SetScale(plotID,iPosition,iParent);
  shared_ptr<Plot> plt = nullptr;
  plt = ptr.GetTrackPtr(plotID);
  plt.PauseRtPlot(false);
  return(plotID);
}

bool aliDcsUi_isDpeArchived(string DPEname)
{
  bool isArch = false;
  dpGet(dpSubStr(DPEname, DPSUB_SYS_DP_EL)+":_archive.._archive", isArch);
  return(isArch);
}
