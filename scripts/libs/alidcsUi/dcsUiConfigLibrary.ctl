// $License: NOLICENSE
/* ------------------------------------------------------------

  ALICE DCS - UI CONFIGURATION LIBRARY

  Ver. 0.0  18/11/2019

  Auth: A.Franco
  Inst. : INFN Seza. BARI - ITALY

       * HISTORY *
05/02/2020   Adapt to Dynamic Hierarchy
26/09/2022   Comlete the library with all the Write functions

---------------------------------------------------------------*/
#uses "CtrlXml"
#uses "alidcsUi/dcsUiLibrary.ctl"
//#uses "alidcsUi/dcsUiObjectLibrary.ctl"

// ==============  Global const definitions ==================

const string UICONF_OBJ_VERSION = "1.1"; // Library Version
const bool UICONF_IS_CATCHED = false;

const int MAXNUMTAB = 10;
const int MAXNUMPANBUT = 10;

// ==============  Global var definitions ==================


// ==============  Data type/structures definition ==============

struct UILayout
{
  string windowBackColor;
  int numPanButRows;
  int maxButRows;
  int headerHeigth;
  int footerHeigth;
  string zoneBackColor;
  string buttonBackgColor;
  string buttonForegColor;
  string buttonFont;
  bool buttonTypeIsRise;
  int maxOfTabs;
  int numberOfTabs;
  int defaultTabButton;
  bool windowFullScreen;
  bool windowResizable;
};


// ==============  Class Definition =================


class Configuration
{
  string confFileName = "";
  unsigned ConfId = -1;
  unsigned ConfRootId = -1;
  bool isCached = UICONF_IS_CATCHED;
  //UILayout theLayout;

  public bool Create(string sName="LHC Interface UI",
                     string sVersion="0.0",
                     string sAuthor="",
                     string sDate="18/04/1963"
                     )
  {
    if(ConfId >= 0) xmlCloseDocument(ConfId);

    ConfId = xmlNewDocument();
    if(ConfId < 0) {
      alidcsUi_log("Error to create config. Abort ! ["+sName+"]",LOGCRIT, "Configuration::Create()");
      return(false);
    }
    xmlAppendChild(ConfId, -1, XML_COMMENT_NODE, "ALICE DCS UI configuration file - Produced by UI Script.");
    ConfRootId = xmlAppendChild(ConfId, -1, XML_ELEMENT_NODE, "UI");
    xmlSetElementAttribute(ConfId, ConfRootId, "Author", sAuthor);
    xmlSetElementAttribute(ConfId, ConfRootId, "Date", sDate);
    xmlSetElementAttribute(ConfId, ConfRootId, "Name", sName);
    xmlSetElementAttribute(ConfId, ConfRootId, "Version", sVersion);
    int node = xmlAppendChild(ConfId, ConfRootId, XML_ELEMENT_NODE, "LAYOUT");
    node = xmlAppendChild(ConfId, ConfRootId, XML_ELEMENT_NODE, "FSMHIERARCHY");
    node = xmlAppendChild(ConfId, ConfRootId, XML_ELEMENT_NODE, "TOOLS");
    node = xmlAppendChild(ConfId, ConfRootId, XML_ELEMENT_NODE, "CUSTOMIZATION");
    node = xmlAppendChild(ConfId, ConfRootId, XML_ELEMENT_NODE, "DEFINITION");
    xmlSetElementAttribute(ConfId, node, "NumberOfTabs", 8);
    alidcsUi_log("Create config ! ["+ConfId+"]",LOGINFO, "Configuration::Create()");
    return(true);
  }

  public bool Init(string configurationFileName, bool catched = UICONF_IS_CATCHED)
  {
    if(ConfId >= 0) xmlCloseDocument(ConfId);
    ConfId = -1;
    ConfRootId = -1;

    if(configurationFileName == "" || !isfile(configurationFileName)) {
      alidcsUi_log("Bad Configuration file name. Abort ! ["+configurationFileName+"]",LOGCRIT, "Configuration::Init()");
      return(false);
    }
    confFileName = configurationFileName;
    isCached = catched;

    if(isCached) {
      if(!readFile())
        return(false);
    } else {
      if(!checkFile())
        return(false);
    }
    return(true);
  }

  public bool Save(string configurationFileName, bool overwrite=true)
  {
    if(ConfId == -1) {
      alidcsUi_log("Config not exists. Abort ! ["+configurationFileName+"]",LOGCRIT, "Configuration::Save()");
      return(false);
    }

    if(configurationFileName == "" || (isfile(configurationFileName) && !overwrite ) ){
      alidcsUi_log("File already exists. Abort ! ["+configurationFileName+"]",LOGCRIT, "Configuration::Save()");
      return(false);
    }
    checkAndCorrect();
    confFileName = configurationFileName;
    int res = xmlDocumentToFile(ConfId,configurationFileName);
    alidcsUi_log("Config saved ! ["+configurationFileName+"]",LOGINFO, "Configuration::Save()");
    return(true);
  }

  private bool readFile()
  {
    string errMsg;
    int errLine, errColumn;

    ConfId = xmlDocumentFromFile(confFileName, errMsg, errLine, errColumn);
    if(errLine > 0) {
      alidcsUi_log("Error reading the configuration file ("+confFileName+"):" + errMsg + " in("+errLine+","+errColumn+")", LOGCRIT, "Configuration::openFile()");
      ConfId = -1;
      return(false);
    }
    ConfRootId = -1;// xmlFirstChild(ConfId);
    if(!checkFile()) {
      alidcsUi_log("the File isn't valid !", LOGERRO, "Configuration::ReadToolsDefinition()");
      return(false);
    }
    return(true);
  }

  private bool openFile()
  {
    if(isCached && ConfId != -1) {
      return(true);
    }
    return(readFile());
  }

  private void closeFile()
  {
    if(!isCached) {
      xmlCloseDocument(ConfId);
      ConfId = -1;
      ConfRootId = -1;
    }
    return;
  }

  // Checks if is a valid XML file and reset the pointer to Root
  private bool checkFile()
  {
    if(ConfId < 0) {
      return(false);
    }
    string value;
    resetThePointer();
    xmlGetElementAttribute(ConfId,ConfRootId,"Version",value);
    float v = value;
    alidcsUi_log("The configuration file version is :" +value+" ", LOGINFO, "Configuration::checkFile()");
    if( v > 0 ) {
       return(true);
    }
    alidcsUi_log("Error checking the configuration file. Abort ! ("+confFileName+")", LOGCRIT, "Configuration::checkFile()");
    ConfRootId = -1;
    return(false);
  }

  // make some checks and impose correction in order to avoids strange behaviour of the UI
  private void checkAndCorrect()
  {
    int width, height;
    UILayout layout;

    if(ReadUIDim(width, height)) {
      if(ReadUILayout(layout)) {
        if(layout.windowResizable && layout.windowFullScreen) {
          layout.windowResizable = false;
        }
        if(width == 0 || height == 0) {
          width = 1400;
          height = 1024;
        }
      }
    }
    WriteUIDim(width, height);
    WriteUILayout(layout);

    return;
  }

  private void resetThePointer()
  {
    if(ConfId < 0) {
      return;
    }
    string value;
    int childNode = xmlFirstChild(ConfId);
    while(childNode != -1) {
      if( xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE ) {
        if( xmlNodeName(ConfId, childNode) == "UI" ) {
          ConfRootId = childNode;
            return;
        }
      }
      childNode = xmlNextSibling(ConfId,childNode);
    }
    ConfRootId = -1;
    return;
  }

  private void seekTheSection(string section)
  {
    resetThePointer();
    int childNode = xmlFirstChild(ConfId,ConfRootId);
    while( childNode != -1){
      if(xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, childNode) == section) {
        ConfRootId = childNode;
        return;
      }
      childNode = xmlNextSibling(ConfId,childNode);
    }
    return;
  }

  private int seekTheElement(string sNodeName)
  {
    int childNode = xmlFirstChild(ConfId,ConfRootId);
    while( childNode != -1){
      if(xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, childNode) == sNodeName) {
        return(childNode);
      }
      childNode = xmlNextSibling(ConfId,childNode);
    }
    return(-1);
  }

  public dyn_dyn_anytype ReadToolsDefinition()
  {
    string value;
    dyn_dyn_anytype ddaResult;
    dyn_anytype daAppo;

    if(!openFile()) return(ddaResult);
    seekTheSection("TOOLS");
    int childNode = xmlFirstChild(ConfId,ConfRootId);
    while( childNode != -1){
      if(xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, childNode) == "PANEL") {
        dynClear(daAppo);
        xmlGetElementAttribute(ConfId,childNode,"Label",value); dynAppend(daAppo,value);
        xmlGetElementAttribute(ConfId,childNode,"PanelName",value); dynAppend(daAppo,value);
        xmlGetElementAttribute(ConfId,childNode,"Parameters",value); dynAppend(daAppo,value);
        xmlGetElementAttribute(ConfId,childNode,"Enabled",value); dynAppend(daAppo,value);
        dynAppend(ddaResult, daAppo);
      }
      childNode = xmlNextSibling(ConfId,childNode);
    }

    closeFile();
    return(ddaResult);
  }

  public void WriteToolsDefinition(dyn_dyn_anytype ddaToolsDef)
  {
    if(ConfId == -1) return;
    seekTheSection("TOOLS");

    int theNode = seekTheElement("PANEL");
    while(theNode != -1) {
      xmlRemoveNode(ConfId, theNode);
      theNode = seekTheElement("PANEL");
    }

    for(int i=1; i<=dynlen(ddaToolsDef); i++) {
      theNode = xmlAppendChild(ConfId, ConfRootId, XML_ELEMENT_NODE, "PANEL");
      xmlSetElementAttribute(ConfId,theNode,"Enabled",ddaToolsDef[i][4]);
      xmlSetElementAttribute(ConfId,theNode,"Parameters",ddaToolsDef[i][3]);
      xmlSetElementAttribute(ConfId,theNode,"PanelName",ddaToolsDef[i][2]);
      xmlSetElementAttribute(ConfId,theNode,"Label",ddaToolsDef[i][1]);
    }
    return;
  }

  public dyn_dyn_anytype ReadTabsDefinition()
  {
    string value;
    dyn_dyn_anytype ddaResult;
    dyn_anytype daAppo;

    if(!openFile()) return(ddaResult);
    seekTheSection("DEFINITION");
    int childNode = xmlFirstChild(ConfId,ConfRootId);
    while( childNode != -1){
      if(xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, childNode) == "TAB") {
        dynClear(daAppo);
        xmlGetElementAttribute(ConfId,childNode,"Label",value); dynAppend(daAppo,value);
        xmlGetElementAttribute(ConfId,childNode,"Index",value); dynAppend(daAppo,value);
        xmlGetElementAttribute(ConfId,childNode,"Position",value); dynAppend(daAppo,value);
        xmlGetElementAttribute(ConfId,childNode,"Enabled",value); dynAppend(daAppo,value);
        dynAppend(ddaResult, daAppo);
      }
      childNode = xmlNextSibling(ConfId,childNode);
    }

    closeFile();
    return(ddaResult);
  }

  public void WriteTabDefinition(dyn_dyn_anytype ddaDefinition)
  {
    if(ConfId == -1) return;
    seekTheSection("DEFINITION");

    int theNode = seekTheElement("TAB");
    while(theNode != -1) {
      xmlRemoveNode(ConfId, theNode);
      theNode = seekTheElement("TAB");
    }

    for(int i=1; i<=dynlen(ddaDefinition); i++) {
      theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "TAB");
      xmlSetElementAttribute(ConfId,theNode,"Enabled",ddaDefinition[i][4]);
      xmlSetElementAttribute(ConfId,theNode,"Label",ddaDefinition[i][1]);
      xmlSetElementAttribute(ConfId,theNode,"Position",ddaDefinition[i][3]);
      xmlSetElementAttribute(ConfId,theNode,"Index",i);
    }
    return;
  }

  public bool ReadUIAbout(string &theName, string &theVersion, string &theAuthor, string &theDate)
  {
    if(!openFile()) return(false);
    resetThePointer();
    xmlGetElementAttribute(ConfId,ConfRootId,"Version",theVersion);
    xmlGetElementAttribute(ConfId,ConfRootId,"Name",theName);
    xmlGetElementAttribute(ConfId,ConfRootId,"Date",theDate);
    xmlGetElementAttribute(ConfId,ConfRootId,"Author",theAuthor);
    return(true);
  }

  public bool WriteUIAbout(string theName, string theVersion, string theAuthor, string theDate)
  {
    if(ConfId == -1) return(false);
    resetThePointer();
    xmlSetElementAttribute(ConfId,ConfRootId,"Author",theAuthor);
    xmlSetElementAttribute(ConfId,ConfRootId,"Date",theDate);
    xmlSetElementAttribute(ConfId,ConfRootId,"Name",theName);
    xmlSetElementAttribute(ConfId,ConfRootId,"Version",theVersion);
    return(true);
  }

  // Read the XML configuration file
  public bool ReadUILayout(UILayout &theLayout)
  {
    theLayout.windowBackColor = "{162,204,161}";
    theLayout.numPanButRows = MAXNUMPANBUT;
    theLayout.maxButRows = MAXNUMPANBUT;
    theLayout.headerHeigth = 53;
    theLayout.footerHeigth = 48;
    theLayout.zoneBackColor = "{162,204,161}";
    theLayout.buttonBackgColor = "Grey";
    theLayout.buttonForegColor = "Black";
    theLayout.buttonFont = "Burlingame Pro Light";
    theLayout.buttonTypeIsRise = true;
    theLayout.numberOfTabs = MAXNUMTAB;
    theLayout.maxOfTabs = MAXNUMTAB;
    theLayout.defaultTabButton = 0;
    theLayout.windowFullScreen = false;
    theLayout.windowResizable = false;

    string value;

    if(!openFile()) return(false);
    seekTheSection("LAYOUT");
    int childNode = xmlFirstChild(ConfId,ConfRootId);
    while( childNode != -1){
      if(xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, childNode) == "MAIN") {
        xmlGetElementAttribute(ConfId,childNode,"BackCol",value); theLayout.windowBackColor = value;
        xmlGetElementAttribute(ConfId,childNode,"FrameRows",value); theLayout.numPanButRows = value;
        xmlGetElementAttribute(ConfId,childNode,"HeaderHeigth",value); theLayout.headerHeigth = value;
        xmlGetElementAttribute(ConfId,childNode,"FooterHeigth",value); theLayout.footerHeigth = value;
      }
      if(xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, childNode) == "ZONE") {
        xmlGetElementAttribute(ConfId,childNode,"BackCol",value); theLayout.zoneBackColor = value;
      }
      if(xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, childNode) == "TAB") {
        xmlGetElementAttribute(ConfId,childNode,"BackCol",value); theLayout.buttonBackgColor = value;
        xmlGetElementAttribute(ConfId,childNode,"ForeCol",value); theLayout.buttonForegColor = value;
        xmlGetElementAttribute(ConfId,childNode,"Font",value); theLayout.buttonFont = value;
        xmlGetElementAttribute(ConfId,childNode,"Type",value); theLayout.buttonTypeIsRise = (strtoupper(value) == "RISE") ? true : false;
      }

      if(xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, childNode) == "WINDOW") {
        xmlGetElementAttribute(ConfId,childNode,"Resizable",value); theLayout.windowResizable = (strtoupper(value) == "TRUE") ? true : false;
        xmlGetElementAttribute(ConfId,childNode,"Fullscreen",value); theLayout.windowFullScreen = (strtoupper(value) == "TRUE") ? true : false;
      }
      childNode = xmlNextSibling(ConfId,childNode);
    }

    seekTheSection("DEFINITION");
    xmlGetElementAttribute(ConfId,ConfRootId,"NumberOfTabs",value); theLayout.numberOfTabs = value;
    if(theLayout.numberOfTabs > MAXNUMTAB || theLayout.numberOfTabs <= 0) theLayout.numberOfTabs = 1;
    xmlGetElementAttribute(ConfId,ConfRootId,"Default",value); theLayout.defaultTabButton = value;
    if(theLayout.defaultTabButton > MAXNUMTAB || theLayout.defaultTabButton <= 0) theLayout.defaultTabButton = 0;

    closeFile();
    return(true);
  }

  public void WriteUILayout(UILayout theLayout) {
    if(ConfId == -1) return;
    seekTheSection("LAYOUT");
    int theNode;
    theNode = seekTheElement("MAIN");
    if(theNode == -1) theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "MAIN");
    xmlSetElementAttribute(ConfId,theNode,"HeaderHeigth", theLayout.headerHeigth);
    xmlSetElementAttribute(ConfId,theNode,"FooterHeigth", theLayout.footerHeigth);
    xmlSetElementAttribute(ConfId,theNode,"FrameRows", theLayout.numPanButRows);
    xmlSetElementAttribute(ConfId,theNode,"BackCol", theLayout.windowBackColor);

    theNode = seekTheElement("ZONE");
    if(theNode == -1) theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "ZONE");
    xmlSetElementAttribute(ConfId,theNode,"BackCol", theLayout.windowBackColor);

    theNode = seekTheElement("TAB");
    if(theNode == -1) theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "TAB");
    xmlSetElementAttribute(ConfId,theNode,"Type", theLayout.buttonTypeIsRise);
    xmlSetElementAttribute(ConfId,theNode,"Font", theLayout.buttonFont);
    xmlSetElementAttribute(ConfId,theNode,"BackCol", theLayout.buttonBackgColor);
    xmlSetElementAttribute(ConfId,theNode,"ForeCol", theLayout.buttonForegColor);

    theNode = seekTheElement("WINDOW");
    if(theNode == -1) theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "WINDOW");
    xmlSetElementAttribute(ConfId,theNode,"Resizable", theLayout.windowResizable);
    xmlSetElementAttribute(ConfId,theNode,"Fullscreen", theLayout.windowFullScreen);

    seekTheSection("DEFINITION");
    xmlSetElementAttribute(ConfId,ConfRootId,"Default", theLayout.defaultTabButton);
    xmlSetElementAttribute(ConfId,ConfRootId,"NumberOfTabs", theLayout.numberOfTabs);
    return;
  }

  public void WriteTheLayout(dyn_anytype daLayout) {
    if(ConfId == -1) return;
    seekTheSection("LAYOUT");
    int theNode;
    theNode = seekTheElement("MAIN");
    if(theNode == -1) theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "MAIN");
    xmlSetElementAttribute(ConfId,theNode,"HeaderHeigth", daLayout[3]);
    xmlSetElementAttribute(ConfId,theNode,"FooterHeigth", daLayout[4]);
    xmlSetElementAttribute(ConfId,theNode,"FrameRows", daLayout[2]);
    xmlSetElementAttribute(ConfId,theNode,"BackCol", daLayout[1]);

    theNode = seekTheElement("ZONE");
    if(theNode == -1) theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "ZONE");
    xmlSetElementAttribute(ConfId,theNode,"BackCol", daLayout[5]);

    theNode = seekTheElement("TAB");
    if(theNode == -1) theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "TAB");
    xmlSetElementAttribute(ConfId,theNode,"Type", daLayout[9]);
    xmlSetElementAttribute(ConfId,theNode,"Font", daLayout[8]);
    xmlSetElementAttribute(ConfId,theNode,"BackCol", daLayout[6]);
    xmlSetElementAttribute(ConfId,theNode,"ForeCol", daLayout[7]);

    theNode = seekTheElement("WINDOW");
    if(theNode == -1) theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "WINDOW");
    xmlSetElementAttribute(ConfId,theNode,"Resizable", daLayout[12]);
    xmlSetElementAttribute(ConfId,theNode,"Fullscreen", daLayout[13]);

    seekTheSection("DEFINITION");
    xmlSetElementAttribute(ConfId,ConfRootId,"Default", daLayout[11]);
    xmlSetElementAttribute(ConfId,ConfRootId,"NumberOfTabs", daLayout[10]);
    return;
  }

  public bool ReadUIDim(int &width, int &height)
  {
    string value;

    if(!openFile()) return(false);
    seekTheSection("LAYOUT");
    int childNode = xmlFirstChild(ConfId,ConfRootId);
    while( childNode != -1){
      if(xmlNodeType(ConfId, childNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, childNode) == "DIMENSION") {
        xmlGetElementAttribute(ConfId,childNode,"Width",value); width = value;
        xmlGetElementAttribute(ConfId,childNode,"Height",value); height = value;
      }
      childNode = xmlNextSibling(ConfId,childNode);
    }
    closeFile();
    return(true);
  }

  public void WriteUIDim(int width, int height)
  {
    if(ConfId == -1) return;
    seekTheSection("LAYOUT");
    int theNode = seekTheElement("DIMENSION");
    if(theNode == -1) theNode = xmlAppendChild(ConfId,ConfRootId, XML_ELEMENT_NODE, "DIMENSION");
    xmlSetElementAttribute(ConfId,theNode,"Width",width);
    xmlSetElementAttribute(ConfId,theNode,"Height",height);
    return;
  }

  public dyn_dyn_anytype ReadFrameDefinition(int position)
  {
    string value, butLabel, butPanel, PanelReferences ;
    int TabPosition, butPosition, butIndex, DefaultButton, NumberOfButtons;
    bool butEnabled, butHookable;
    string PanelParameters;
    dyn_dyn_anytype ddaResult;

    if(!openFile()) return(ddaResult);
    seekTheSection("DEFINITION");
    int tabNode = xmlFirstChild(ConfId,ConfRootId);
    while( tabNode != -1) {
      if(xmlNodeType(ConfId, tabNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, tabNode) == "TAB") {
        xmlGetElementAttribute(ConfId,tabNode,"Position",value); TabPosition = value;
        if(TabPosition == position) {
          int fraNode = xmlFirstChild(ConfId,tabNode);
          while( fraNode != -1) {
            if(xmlNodeType(ConfId, fraNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, fraNode) == "FRAME") {
              NumberOfButtons = 0; if (xmlGetElementAttribute(ConfId,fraNode,"NumberOfButtons",value) == 0) NumberOfButtons = value;
              DefaultButton = 0; if (xmlGetElementAttribute(ConfId,fraNode,"Default",value) == 0) DefaultButton = value;
              PanelReferences = ""; if (xmlGetElementAttribute(ConfId,fraNode,"Panelreference",value) == 0) PanelReferences = value;
              PanelParameters = makeDynString(); if (xmlGetElementAttribute(ConfId,fraNode,"Parameters",value) == 0) PanelParameters = stringToDynString(value, "|");
              if(  NumberOfButtons == 0 &&  PanelReferences != "") { // we have a panel
                dynAppend(ddaResult,makeDynString(PanelReferences, PanelParameters));
                return(ddaResult);
//                setupLeftPanel(PanelReferences, stringToDynString(PanelParameters,"|"));
              } else {  // we have buttons
                int butNode = xmlFirstChild(ConfId,fraNode);
                while( butNode != -1) {
                  if(xmlNodeType(ConfId, butNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, butNode) == "BUTTON") {
                    xmlGetElementAttribute(ConfId,butNode,"Index",value); butIndex = value;
                    xmlGetElementAttribute(ConfId,butNode,"Position",value); butPosition = value;
                    xmlGetElementAttribute(ConfId,butNode,"Label",value); butLabel = value;
                    xmlGetElementAttribute(ConfId,butNode,"PanelName",value); butPanel = value;
                    xmlGetElementAttribute(ConfId,butNode,"Parameters",value); PanelParameters = value;
                    xmlGetElementAttribute(ConfId,butNode,"Enabled",value); butEnabled = value;
                    butHookable = (xmlGetElementAttribute(ConfId,butNode,"Hookable",value) == 0)? value : true;
                    // Set up the buttons
                    dyn_anytype daAppo;
                    dynAppend(daAppo,butIndex);
                    dynAppend(daAppo,butPosition);
                    dynAppend(daAppo,butLabel);
                    dynAppend(daAppo,butPanel);
                    dynAppend(daAppo,PanelParameters);
                    dynAppend(daAppo,butEnabled);
                    dynAppend(daAppo,butHookable);
                    dynAppend(daAppo,(DefaultButton > 0 && butIndex == DefaultButton)?true:false);
                    dynAppend(ddaResult,daAppo);
                  }
                  butNode = xmlNextSibling(ConfId,butNode);
                }
              }
              closeFile();
              return(ddaResult);
            }
            fraNode = xmlNextSibling(ConfId,fraNode);
          }
          closeFile();
          return(ddaResult);
        }
      }
      tabNode = xmlNextSibling(ConfId,tabNode);
    }
    closeFile();
    return(ddaResult);
  }

  public void WriteFrameDefinition(int position, dyn_dyn_anytype ddaFrameDef)
  {
    if(ConfId == -1) return;
    seekTheSection("DEFINITION");
    bool flag = false;
    int theTabNode, theNode, thePosition;
    string value;


    theNode = xmlFirstChild(ConfId,ConfRootId);
    while(theNode != -1){
      if(xmlNodeType(ConfId, theNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, theNode) == "TAB") {
        xmlGetElementAttribute(ConfId,theNode,"Position",value); thePosition = value;
        if(thePosition == position) {
          flag = true;
          theTabNode = theNode;
          theNode = -1;
        } else {
          theNode = xmlNextSibling(ConfId,theNode);
        }
      }
    }
    if(!flag) return;
    ConfRootId = theTabNode;

    theNode = seekTheElement("FRAME");
    while(theNode != -1) {
      xmlRemoveNode(ConfId, theNode);
      theNode = seekTheElement( "FRAME");
    }

    int Index = 0;
    int DefaultBut = 0;
    theNode = xmlAppendChild(ConfId, ConfRootId, XML_ELEMENT_NODE, "FRAME");
    for(int i=1;i<=dynlen(ddaFrameDef); i++) {
      if(dynlen(ddaFrameDef[i]) == 2) {
        xmlSetElementAttribute(ConfId,theNode,"Parameters",dynStringToString(ddaFrameDef[i][2], "|") );
        xmlSetElementAttribute(ConfId,theNode,"Panelreference",ddaFrameDef[i][1]);
      } else {
        int theButNode = xmlAppendChild(ConfId, theNode, XML_ELEMENT_NODE, "BUTTON");
        xmlSetElementAttribute(ConfId,theButNode,"Hookable",ddaFrameDef[i][7]);
        xmlSetElementAttribute(ConfId,theButNode,"Enabled",ddaFrameDef[i][6]);
        xmlSetElementAttribute(ConfId,theButNode,"Parameters",ddaFrameDef[i][5]);
        xmlSetElementAttribute(ConfId,theButNode,"PanelName",ddaFrameDef[i][4]);
        xmlSetElementAttribute(ConfId,theButNode,"Label",ddaFrameDef[i][3]);
        xmlSetElementAttribute(ConfId,theButNode,"Position",ddaFrameDef[i][2]);
        xmlSetElementAttribute(ConfId,theButNode,"Index",++Index);
        if(strtoupper(ddaFrameDef[i][8]) == "TRUE") DefaultBut = Index;
      }
    }
    if(Index > 0) {
      xmlSetElementAttribute(ConfId,theNode,"Default",DefaultBut);
      xmlSetElementAttribute(ConfId,theNode,"NumberOfButtons",Index);
    }
    return;
  }

  public dyn_string ReadFSMRootNodes()
  {
    string value = "";
    dyn_string dsResult;
    dynClear(dsResult);

    if(!openFile()) return(dsResult);
    seekTheSection("FSMHIERARCHY");
    int tabNode = xmlFirstChild(ConfId,ConfRootId);
    while( tabNode != -1) {
      if(xmlNodeType(ConfId, tabNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, tabNode) == "ROOT") {
        xmlGetElementAttribute(ConfId,tabNode,"Node",value);
        dynAppend(dsResult,value);
      }
      tabNode = xmlNextSibling(ConfId,tabNode);
    }
    closeFile();
    return(dsResult);
  }

  public void WriteFSMRootNodes(dyn_string dsFSMs)
  {
    if(ConfId == -1) return;
    seekTheSection("FSMHIERARCHY");

    int theNode = seekTheElement("ROOT");
    while(theNode != -1) {
      xmlRemoveNode(ConfId, theNode);
      theNode = seekTheElement("ROOT");
    }
    for(int i=1; i<= dynlen(dsFSMs); i++) {
      theNode = xmlAppendChild(ConfId,ConfRootId,XML_ELEMENT_NODE,"ROOT");
      xmlSetElementAttribute(ConfId,theNode,"Node",dsFSMs[i]);
    }
    return;
  }

  public dyn_dyn_string ReadCustomization()
  {
    string value = "";
    string param = "";
    dyn_dyn_string ddsResult;
    dynClear(ddsResult);

    if(!openFile()) return(ddsResult);
    seekTheSection("CUSTOMIZATION");
    int tabNode = xmlFirstChild(ConfId,ConfRootId);
    while( tabNode != -1) {
      if(xmlNodeType(ConfId, tabNode) == XML_ELEMENT_NODE && xmlNodeName(ConfId, tabNode) == "WIDJET") {
        xmlGetElementAttribute(ConfId,tabNode,"Parameters",param);
        xmlGetElementAttribute(ConfId,tabNode,"PanelName",value);
        dynAppend(ddsResult,makeDynString("WidjetPanel", value, param));
      }
      tabNode = xmlNextSibling(ConfId,tabNode);
    }
    closeFile();
    return(ddsResult);
  }

  public void WriteCustomization(dyn_dyn_string ddsCustom)
  {
    string value = "";
    string param = "";

    if(ConfId == -1) return;
    seekTheSection("CUSTOMIZATION");

    int ptr = 1;
    int tabNode = xmlFirstChild(ConfId,ConfRootId);
    while( tabNode != -1) {
      xmlRemoveNode(ConfId, tabNode);
      tabNode = seekTheElement("WIDJET");
    }
    for(ptr = 1; ptr <= dynlen(ddsCustom); ptr++) {
      tabNode = xmlAppendChild(ConfId, ConfRootId, XML_ELEMENT_NODE, "WIDJET");
      xmlSetElementAttribute(ConfId,tabNode,"Parameters",ddsCustom[ptr][3]);
      xmlSetElementAttribute(ConfId,tabNode,"PanelName",ddsCustom[ptr][2]);
    }
    return;
  }
};
